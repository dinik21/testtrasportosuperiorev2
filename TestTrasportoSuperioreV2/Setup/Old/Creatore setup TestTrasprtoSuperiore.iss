; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

#define MyAppName "Test Trasporto Superiore"
#define MyAppVersion "2.0.3"
#define MyAppPublisher "ARCA Technology"
#define MyAppExeName "TestTrasportoSuperioreV2.exe"

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{FB9E3F27-E835-4ADF-9ECE-07D0EAF9AE4F}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
DefaultDirName=C:\Arca\{#MyAppName}
DisableProgramGroupPage=yes
OutputDir=C:\Users\Utente\Documents\Visual Studio 2015\Projects\TestTrasportoSuperioreV2\TestTrasportoSuperioreV2\Setup
OutputBaseFilename=setupTestTrasportoSuperioreV2.0.3
Compression=lzma
SolidCompression=yes

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"
Name: "italian"; MessagesFile: "compiler:Languages\Italian.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "C:\Users\Utente\Documents\Visual Studio 2015\Projects\TestTrasportoSuperioreV2\TestTrasportoSuperioreV2\bin\Debug\TestTrasportoSuperioreV2.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\Utente\Documents\Visual Studio 2015\Projects\TestTrasportoSuperioreV2\TestTrasportoSuperioreV2\bin\Debug\Checklist-001.txt"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\Utente\Documents\Visual Studio 2015\Projects\TestTrasportoSuperioreV2\TestTrasportoSuperioreV2\bin\Debug\CMCommand.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\Utente\Documents\Visual Studio 2015\Projects\TestTrasportoSuperioreV2\TestTrasportoSuperioreV2\bin\Debug\CMLink.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\Utente\Documents\Visual Studio 2015\Projects\TestTrasportoSuperioreV2\TestTrasportoSuperioreV2\bin\Debug\CMTrace.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\Utente\Documents\Visual Studio 2015\Projects\TestTrasportoSuperioreV2\TestTrasportoSuperioreV2\bin\Debug\dllError.ini"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\Utente\Documents\Visual Studio 2015\Projects\TestTrasportoSuperioreV2\TestTrasportoSuperioreV2\bin\Debug\Italiano.ini"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\Utente\Documents\Visual Studio 2015\Projects\TestTrasportoSuperioreV2\TestTrasportoSuperioreV2\bin\Debug\Setup.ini"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\Utente\Documents\Visual Studio 2015\Projects\TestTrasportoSuperioreV2\TestTrasportoSuperioreV2\bin\Debug\TestValues.ini"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\Utente\Documents\Visual Studio 2015\Projects\TestTrasportoSuperioreV2\TestTrasportoSuperioreV2\bin\Debug\TheRCs.INI"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\Utente\Documents\Visual Studio 2015\Projects\TestTrasportoSuperioreV2\TestTrasportoSuperioreV2\bin\Debug\TheRCsCM14_8.INI"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\Utente\Documents\Visual Studio 2015\Projects\TestTrasportoSuperioreV2\TestTrasportoSuperioreV2\bin\Debug\TheRCsCM18.INI"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\Utente\Documents\Visual Studio 2015\Projects\TestTrasportoSuperioreV2\TestTrasportoSuperioreV2\bin\Debug\TheRCsCM18B.INI"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\Utente\Documents\Visual Studio 2015\Projects\TestTrasportoSuperioreV2\TestTrasportoSuperioreV2\bin\Debug\TheRCsCM18SOLO.INI"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\Utente\Documents\Visual Studio 2015\Projects\TestTrasportoSuperioreV2\TestTrasportoSuperioreV2\bin\Debug\TheRCsCM18T.INI"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\Utente\Documents\Visual Studio 2015\Projects\TestTrasportoSuperioreV2\TestTrasportoSuperioreV2\bin\Debug\TheRCsCM20.INI"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\Utente\Documents\Visual Studio 2015\Projects\TestTrasportoSuperioreV2\TestTrasportoSuperioreV2\bin\Debug\TheRCsCM24_8.INI"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\Utente\Documents\Visual Studio 2015\Projects\TestTrasportoSuperioreV2\TestTrasportoSuperioreV2\bin\Debug\TheRCsCM24B.INI"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\Utente\Documents\Visual Studio 2015\Projects\TestTrasportoSuperioreV2\TestTrasportoSuperioreV2\bin\Debug\TheRCsEMU_T.INI"; DestDir: "{app}"; Flags: ignoreversion
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{commonprograms}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent

