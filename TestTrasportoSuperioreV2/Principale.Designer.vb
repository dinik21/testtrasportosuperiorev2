﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmPrincipale
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cboIdProdotto = New System.Windows.Forms.ComboBox()
        Me.cboCliente = New System.Windows.Forms.ComboBox()
        Me.lblMessaggio = New System.Windows.Forms.Label()
        Me.txtMatricola = New System.Windows.Forms.TextBox()
        Me.cmdPrincipale = New System.Windows.Forms.Button()
        Me.cmdComando1 = New System.Windows.Forms.Button()
        Me.cmdComando2 = New System.Windows.Forms.Button()
        Me.cmdComando3 = New System.Windows.Forms.Button()
        Me.chkOk = New System.Windows.Forms.CheckBox()
        Me.chkFault = New System.Windows.Forms.CheckBox()
        Me.pgbDownload = New System.Windows.Forms.ProgressBar()
        Me.lblShiftCenter = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.mnuConnection = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuConnect = New System.Windows.Forms.ToolStripMenuItem()
        Me.RS232ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.USBToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDisconnect = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuMainCommands = New System.Windows.Forms.ToolStripMenuItem()
        Me.OPENLToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OPENRToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CLOSELToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CLOSERToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TransparentCommandToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SingleCommandToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuModuleTest = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComScopeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AbilitaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DisabilitaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.lstChecklist = New System.Windows.Forms.ListBox()
        Me.cmdComando4 = New System.Windows.Forms.Button()
        Me.myTimer = New System.Windows.Forms.Timer(Me.components)
        Me.tabella = New System.Windows.Forms.DataGridView()
        Me.cmdShiftOk = New System.Windows.Forms.Button()
        Me.cmdShiftKo = New System.Windows.Forms.Button()
        Me.trkShift = New System.Windows.Forms.TrackBar()
        Me.cmdSx = New System.Windows.Forms.Button()
        Me.cmdDx = New System.Windows.Forms.Button()
        Me.lblMacchina = New System.Windows.Forms.Label()
        Me.picLogo = New System.Windows.Forms.PictureBox()
        Me.lstComScope = New System.Windows.Forms.ListBox()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.tabella, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.trkShift, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cboIdProdotto
        '
        Me.cboIdProdotto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboIdProdotto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboIdProdotto.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIdProdotto.FormattingEnabled = True
        Me.cboIdProdotto.Location = New System.Drawing.Point(167, 137)
        Me.cboIdProdotto.Name = "cboIdProdotto"
        Me.cboIdProdotto.Size = New System.Drawing.Size(142, 37)
        Me.cboIdProdotto.TabIndex = 0
        Me.cboIdProdotto.Text = "cboIdProdotto"
        '
        'cboCliente
        '
        Me.cboCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCliente.FormattingEnabled = True
        Me.cboCliente.Location = New System.Drawing.Point(315, 137)
        Me.cboCliente.Name = "cboCliente"
        Me.cboCliente.Size = New System.Drawing.Size(497, 37)
        Me.cboCliente.TabIndex = 1
        Me.cboCliente.Text = "cboCliente"
        '
        'lblMessaggio
        '
        Me.lblMessaggio.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessaggio.ForeColor = System.Drawing.Color.Black
        Me.lblMessaggio.Location = New System.Drawing.Point(12, 34)
        Me.lblMessaggio.Name = "lblMessaggio"
        Me.lblMessaggio.Size = New System.Drawing.Size(1141, 100)
        Me.lblMessaggio.TabIndex = 2
        Me.lblMessaggio.Text = "lblMessaggio"
        '
        'txtMatricola
        '
        Me.txtMatricola.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMatricola.Location = New System.Drawing.Point(259, 198)
        Me.txtMatricola.Name = "txtMatricola"
        Me.txtMatricola.Size = New System.Drawing.Size(310, 35)
        Me.txtMatricola.TabIndex = 3
        Me.txtMatricola.Text = "txtMatricola"
        '
        'cmdPrincipale
        '
        Me.cmdPrincipale.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdPrincipale.Location = New System.Drawing.Point(439, 436)
        Me.cmdPrincipale.Name = "cmdPrincipale"
        Me.cmdPrincipale.Size = New System.Drawing.Size(200, 67)
        Me.cmdPrincipale.TabIndex = 4
        Me.cmdPrincipale.Text = "cmdPrincipale"
        Me.cmdPrincipale.UseVisualStyleBackColor = True
        '
        'cmdComando1
        '
        Me.cmdComando1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdComando1.Location = New System.Drawing.Point(42, 198)
        Me.cmdComando1.Name = "cmdComando1"
        Me.cmdComando1.Size = New System.Drawing.Size(200, 67)
        Me.cmdComando1.TabIndex = 5
        Me.cmdComando1.Text = "cmdComando1"
        Me.cmdComando1.UseVisualStyleBackColor = True
        '
        'cmdComando2
        '
        Me.cmdComando2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdComando2.Location = New System.Drawing.Point(42, 282)
        Me.cmdComando2.Name = "cmdComando2"
        Me.cmdComando2.Size = New System.Drawing.Size(200, 67)
        Me.cmdComando2.TabIndex = 6
        Me.cmdComando2.Text = "cmdComando2"
        Me.cmdComando2.UseVisualStyleBackColor = True
        '
        'cmdComando3
        '
        Me.cmdComando3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdComando3.Location = New System.Drawing.Point(42, 366)
        Me.cmdComando3.Name = "cmdComando3"
        Me.cmdComando3.Size = New System.Drawing.Size(200, 67)
        Me.cmdComando3.TabIndex = 7
        Me.cmdComando3.Text = "cmdComando3"
        Me.cmdComando3.UseVisualStyleBackColor = True
        '
        'chkOk
        '
        Me.chkOk.AutoSize = True
        Me.chkOk.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOk.ForeColor = System.Drawing.Color.LimeGreen
        Me.chkOk.Location = New System.Drawing.Point(315, 308)
        Me.chkOk.Name = "chkOk"
        Me.chkOk.Size = New System.Drawing.Size(127, 41)
        Me.chkOk.TabIndex = 8
        Me.chkOk.Text = "chkOk"
        Me.chkOk.UseVisualStyleBackColor = True
        '
        'chkFault
        '
        Me.chkFault.AutoSize = True
        Me.chkFault.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFault.ForeColor = System.Drawing.Color.Red
        Me.chkFault.Location = New System.Drawing.Point(564, 308)
        Me.chkFault.Name = "chkFault"
        Me.chkFault.Size = New System.Drawing.Size(158, 41)
        Me.chkFault.TabIndex = 9
        Me.chkFault.Text = "chkFault"
        Me.chkFault.UseVisualStyleBackColor = True
        '
        'pgbDownload
        '
        Me.pgbDownload.Location = New System.Drawing.Point(42, 137)
        Me.pgbDownload.Name = "pgbDownload"
        Me.pgbDownload.Size = New System.Drawing.Size(806, 32)
        Me.pgbDownload.TabIndex = 10
        '
        'lblShiftCenter
        '
        Me.lblShiftCenter.AutoSize = True
        Me.lblShiftCenter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblShiftCenter.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShiftCenter.ForeColor = System.Drawing.Color.Black
        Me.lblShiftCenter.Location = New System.Drawing.Point(528, 251)
        Me.lblShiftCenter.Name = "lblShiftCenter"
        Me.lblShiftCenter.Size = New System.Drawing.Size(210, 39)
        Me.lblShiftCenter.TabIndex = 11
        Me.lblShiftCenter.Text = "lblShiftCenter"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuConnection, Me.mnuMainCommands, Me.mnuModuleTest, Me.ComScopeToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1245, 24)
        Me.MenuStrip1.TabIndex = 13
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'mnuConnection
        '
        Me.mnuConnection.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuConnect, Me.mnuDisconnect})
        Me.mnuConnection.Name = "mnuConnection"
        Me.mnuConnection.Size = New System.Drawing.Size(81, 20)
        Me.mnuConnection.Text = "Connection"
        '
        'mnuConnect
        '
        Me.mnuConnect.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RS232ToolStripMenuItem, Me.USBToolStripMenuItem})
        Me.mnuConnect.Name = "mnuConnect"
        Me.mnuConnect.Size = New System.Drawing.Size(133, 22)
        Me.mnuConnect.Text = "Connect"
        '
        'RS232ToolStripMenuItem
        '
        Me.RS232ToolStripMenuItem.Name = "RS232ToolStripMenuItem"
        Me.RS232ToolStripMenuItem.Size = New System.Drawing.Size(105, 22)
        Me.RS232ToolStripMenuItem.Text = "RS232"
        '
        'USBToolStripMenuItem
        '
        Me.USBToolStripMenuItem.Name = "USBToolStripMenuItem"
        Me.USBToolStripMenuItem.Size = New System.Drawing.Size(105, 22)
        Me.USBToolStripMenuItem.Text = "USB"
        '
        'mnuDisconnect
        '
        Me.mnuDisconnect.Name = "mnuDisconnect"
        Me.mnuDisconnect.Size = New System.Drawing.Size(133, 22)
        Me.mnuDisconnect.Text = "Disconnect"
        '
        'mnuMainCommands
        '
        Me.mnuMainCommands.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OPENLToolStripMenuItem, Me.OPENRToolStripMenuItem, Me.CLOSELToolStripMenuItem, Me.CLOSERToolStripMenuItem, Me.TransparentCommandToolStripMenuItem, Me.SingleCommandToolStripMenuItem})
        Me.mnuMainCommands.Enabled = False
        Me.mnuMainCommands.Name = "mnuMainCommands"
        Me.mnuMainCommands.Size = New System.Drawing.Size(111, 20)
        Me.mnuMainCommands.Text = "Main Commands"
        '
        'OPENLToolStripMenuItem
        '
        Me.OPENLToolStripMenuItem.Name = "OPENLToolStripMenuItem"
        Me.OPENLToolStripMenuItem.Size = New System.Drawing.Size(195, 22)
        Me.OPENLToolStripMenuItem.Text = "OPEN L"
        '
        'OPENRToolStripMenuItem
        '
        Me.OPENRToolStripMenuItem.Name = "OPENRToolStripMenuItem"
        Me.OPENRToolStripMenuItem.Size = New System.Drawing.Size(195, 22)
        Me.OPENRToolStripMenuItem.Text = "OPEN R"
        '
        'CLOSELToolStripMenuItem
        '
        Me.CLOSELToolStripMenuItem.Name = "CLOSELToolStripMenuItem"
        Me.CLOSELToolStripMenuItem.Size = New System.Drawing.Size(195, 22)
        Me.CLOSELToolStripMenuItem.Text = "CLOSE L"
        '
        'CLOSERToolStripMenuItem
        '
        Me.CLOSERToolStripMenuItem.Name = "CLOSERToolStripMenuItem"
        Me.CLOSERToolStripMenuItem.Size = New System.Drawing.Size(195, 22)
        Me.CLOSERToolStripMenuItem.Text = "CLOSE R"
        '
        'TransparentCommandToolStripMenuItem
        '
        Me.TransparentCommandToolStripMenuItem.Name = "TransparentCommandToolStripMenuItem"
        Me.TransparentCommandToolStripMenuItem.Size = New System.Drawing.Size(195, 22)
        Me.TransparentCommandToolStripMenuItem.Text = "Transparent Command"
        '
        'SingleCommandToolStripMenuItem
        '
        Me.SingleCommandToolStripMenuItem.Name = "SingleCommandToolStripMenuItem"
        Me.SingleCommandToolStripMenuItem.Size = New System.Drawing.Size(195, 22)
        Me.SingleCommandToolStripMenuItem.Text = "Single Command"
        '
        'mnuModuleTest
        '
        Me.mnuModuleTest.Enabled = False
        Me.mnuModuleTest.Name = "mnuModuleTest"
        Me.mnuModuleTest.Size = New System.Drawing.Size(83, 20)
        Me.mnuModuleTest.Text = "Module Test"
        '
        'ComScopeToolStripMenuItem
        '
        Me.ComScopeToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AbilitaToolStripMenuItem, Me.DisabilitaToolStripMenuItem})
        Me.ComScopeToolStripMenuItem.Name = "ComScopeToolStripMenuItem"
        Me.ComScopeToolStripMenuItem.Size = New System.Drawing.Size(77, 20)
        Me.ComScopeToolStripMenuItem.Text = "ComScope"
        '
        'AbilitaToolStripMenuItem
        '
        Me.AbilitaToolStripMenuItem.Name = "AbilitaToolStripMenuItem"
        Me.AbilitaToolStripMenuItem.Size = New System.Drawing.Size(124, 22)
        Me.AbilitaToolStripMenuItem.Text = "Visualizza"
        '
        'DisabilitaToolStripMenuItem
        '
        Me.DisabilitaToolStripMenuItem.Name = "DisabilitaToolStripMenuItem"
        Me.DisabilitaToolStripMenuItem.Size = New System.Drawing.Size(124, 22)
        Me.DisabilitaToolStripMenuItem.Text = "Nascondi"
        '
        'lstChecklist
        '
        Me.lstChecklist.Enabled = False
        Me.lstChecklist.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstChecklist.FormattingEnabled = True
        Me.lstChecklist.ItemHeight = 20
        Me.lstChecklist.Location = New System.Drawing.Point(854, 138)
        Me.lstChecklist.Name = "lstChecklist"
        Me.lstChecklist.Size = New System.Drawing.Size(255, 264)
        Me.lstChecklist.TabIndex = 14
        '
        'cmdComando4
        '
        Me.cmdComando4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdComando4.Location = New System.Drawing.Point(42, 450)
        Me.cmdComando4.Name = "cmdComando4"
        Me.cmdComando4.Size = New System.Drawing.Size(200, 67)
        Me.cmdComando4.TabIndex = 15
        Me.cmdComando4.Text = "cmdComando4"
        Me.cmdComando4.UseVisualStyleBackColor = True
        '
        'myTimer
        '
        '
        'tabella
        '
        Me.tabella.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.tabella.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.tabella.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.tabella.Enabled = False
        Me.tabella.Location = New System.Drawing.Point(451, 27)
        Me.tabella.Name = "tabella"
        Me.tabella.Size = New System.Drawing.Size(427, 150)
        Me.tabella.TabIndex = 17
        Me.tabella.Visible = False
        '
        'cmdShiftOk
        '
        Me.cmdShiftOk.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdShiftOk.Location = New System.Drawing.Point(315, 348)
        Me.cmdShiftOk.Name = "cmdShiftOk"
        Me.cmdShiftOk.Size = New System.Drawing.Size(200, 67)
        Me.cmdShiftOk.TabIndex = 18
        Me.cmdShiftOk.Text = "OK"
        Me.cmdShiftOk.UseVisualStyleBackColor = True
        '
        'cmdShiftKo
        '
        Me.cmdShiftKo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdShiftKo.Location = New System.Drawing.Point(564, 348)
        Me.cmdShiftKo.Name = "cmdShiftKo"
        Me.cmdShiftKo.Size = New System.Drawing.Size(200, 67)
        Me.cmdShiftKo.TabIndex = 19
        Me.cmdShiftKo.Text = "KO"
        Me.cmdShiftKo.UseVisualStyleBackColor = True
        '
        'trkShift
        '
        Me.trkShift.Location = New System.Drawing.Point(315, 220)
        Me.trkShift.Maximum = 69
        Me.trkShift.Minimum = 55
        Me.trkShift.Name = "trkShift"
        Me.trkShift.Size = New System.Drawing.Size(497, 45)
        Me.trkShift.TabIndex = 20
        Me.trkShift.Value = 55
        '
        'cmdSx
        '
        Me.cmdSx.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSx.Location = New System.Drawing.Point(320, 253)
        Me.cmdSx.Name = "cmdSx"
        Me.cmdSx.Size = New System.Drawing.Size(145, 37)
        Me.cmdSx.TabIndex = 21
        Me.cmdSx.Text = "SX"
        Me.cmdSx.UseVisualStyleBackColor = True
        '
        'cmdDx
        '
        Me.cmdDx.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDx.Location = New System.Drawing.Point(662, 253)
        Me.cmdDx.Name = "cmdDx"
        Me.cmdDx.Size = New System.Drawing.Size(145, 37)
        Me.cmdDx.TabIndex = 22
        Me.cmdDx.Text = "DX"
        Me.cmdDx.UseVisualStyleBackColor = True
        '
        'lblMacchina
        '
        Me.lblMacchina.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMacchina.ForeColor = System.Drawing.Color.Black
        Me.lblMacchina.Location = New System.Drawing.Point(255, 518)
        Me.lblMacchina.Name = "lblMacchina"
        Me.lblMacchina.Size = New System.Drawing.Size(802, 23)
        Me.lblMacchina.TabIndex = 23
        Me.lblMacchina.Text = "Label1"
        '
        'picLogo
        '
        Me.picLogo.Image = Global.TestTrasportoSuperioreV2.My.Resources.Resources.logowhite
        Me.picLogo.Location = New System.Drawing.Point(1176, 34)
        Me.picLogo.Name = "picLogo"
        Me.picLogo.Size = New System.Drawing.Size(52, 59)
        Me.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picLogo.TabIndex = 12
        Me.picLogo.TabStop = False
        '
        'lstComScope
        '
        Me.lstComScope.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstComScope.FormattingEnabled = True
        Me.lstComScope.ItemHeight = 20
        Me.lstComScope.Location = New System.Drawing.Point(854, 308)
        Me.lstComScope.Name = "lstComScope"
        Me.lstComScope.Size = New System.Drawing.Size(288, 224)
        Me.lstComScope.TabIndex = 24
        '
        'frmPrincipale
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(1245, 594)
        Me.Controls.Add(Me.lstComScope)
        Me.Controls.Add(Me.lblMacchina)
        Me.Controls.Add(Me.cmdDx)
        Me.Controls.Add(Me.cmdSx)
        Me.Controls.Add(Me.trkShift)
        Me.Controls.Add(Me.cmdShiftKo)
        Me.Controls.Add(Me.cmdShiftOk)
        Me.Controls.Add(Me.tabella)
        Me.Controls.Add(Me.cmdComando4)
        Me.Controls.Add(Me.lstChecklist)
        Me.Controls.Add(Me.picLogo)
        Me.Controls.Add(Me.lblShiftCenter)
        Me.Controls.Add(Me.chkFault)
        Me.Controls.Add(Me.chkOk)
        Me.Controls.Add(Me.cmdComando3)
        Me.Controls.Add(Me.cmdComando2)
        Me.Controls.Add(Me.cmdComando1)
        Me.Controls.Add(Me.cmdPrincipale)
        Me.Controls.Add(Me.txtMatricola)
        Me.Controls.Add(Me.lblMessaggio)
        Me.Controls.Add(Me.cboCliente)
        Me.Controls.Add(Me.cboIdProdotto)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.pgbDownload)
        Me.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmPrincipale"
        Me.Text = "Form1"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.tabella, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.trkShift, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cboIdProdotto As ComboBox
    Friend WithEvents cboCliente As ComboBox
    Friend WithEvents lblMessaggio As Label
    Friend WithEvents txtMatricola As TextBox
    Friend WithEvents cmdPrincipale As Button
    Friend WithEvents cmdComando1 As Button
    Friend WithEvents cmdComando2 As Button
    Friend WithEvents cmdComando3 As Button
    Friend WithEvents chkOk As CheckBox
    Friend WithEvents chkFault As CheckBox
    Friend WithEvents pgbDownload As ProgressBar
    Friend WithEvents lblShiftCenter As Label
    Friend WithEvents picLogo As PictureBox
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents mnuConnection As ToolStripMenuItem
    Friend WithEvents mnuConnect As ToolStripMenuItem
    Friend WithEvents mnuDisconnect As ToolStripMenuItem
    Friend WithEvents mnuMainCommands As ToolStripMenuItem
    Friend WithEvents mnuModuleTest As ToolStripMenuItem
    Friend WithEvents lstChecklist As ListBox
    Friend WithEvents cmdComando4 As Button
    Friend WithEvents myTimer As Timer
    Friend WithEvents tabella As DataGridView
    Friend WithEvents cmdShiftOk As Button
    Friend WithEvents cmdShiftKo As Button
    Friend WithEvents trkShift As TrackBar
    Friend WithEvents cmdSx As Button
    Friend WithEvents cmdDx As Button
    Friend WithEvents lblMacchina As Label
    Friend WithEvents lstComScope As ListBox
    Friend WithEvents ComScopeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AbilitaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DisabilitaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RS232ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents USBToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TransparentCommandToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SingleCommandToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OPENLToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OPENRToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CLOSELToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CLOSERToolStripMenuItem As ToolStripMenuItem
End Class
