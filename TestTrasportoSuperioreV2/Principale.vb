﻿Imports TestTrasportoSuperioreV2.CMClass
Imports System.Data.OleDb
Imports System
Imports System.IO
Imports System.IO.Ports
Imports System.Collections
Imports System.IO.Compression
Imports Microsoft.VisualBasic.FileIO
Imports System.Text
Imports System.Net.Sockets
Imports System.Text.UTF8Encoding
Imports System.Text.StringBuilder
Imports System.Collections.Generic
Imports System.Drawing.Imaging
Imports System.Drawing.Printing
Imports System.Runtime.InteropServices
Imports XmlDll
Imports System.Security.Cryptography
Imports System.ComponentModel

Public Class frmPrincipale
    Public lastError As Int32
    Public ultimoReplyCode As String
    Public statusCassette(12) As String
    Public statusFeeder, statusController, statusReader, statusSafe, statusEscrowA, statusDepositA, statusEscrowB, statusDepositB As String
    Public serialiVerificati As Integer
    Public serialCassetto(12) As String
    Public comunicationError As Boolean = False
    Public side As String = "L"
    Public formatoSeriale As Boolean = False
    Public stato As String = ""
    Public configIni As New INIFile(localFolder & "\setup.ini")
    Public lingua As String = configIni.ReadValue("option", "language")
    Public tipoConnessione As String = configIni.ReadValue("Connection", "Connection")
    Public comPort As String = configIni.ReadValue("RS232", "Port")
    Public comBaudRate As Integer = configIni.ReadValue("RS232", "Baudrate")
    Public comStopbits As Integer = configIni.ReadValue("RS232", "Stopbit")
    Public comParity As String = configIni.ReadValue("RS232", "Parity")
    Public comDataBits As Integer = configIni.ReadValue("RS232", "Databits")
    Public comProtocol As String = configIni.ReadValue("RS232", "Protocol")
    Public pswUserL As String = configIni.ReadValue("Pwd", "UserL")
    Public pswUserR As String = configIni.ReadValue("Pwd", "UserR")
    Public lanSSL As String = configIni.ReadValue("TCP-IP", "SSL")
    Public lanIPAddress As String = configIni.ReadValue("TCP-IP", "IPAddress")
    Public lanPort As String = configIni.ReadValue("TCP-IP", "Port")
    Public lanProtocol As String = configIni.ReadValue("TCP-IP", "Protocol")
    Public lanDevice As String = configIni.ReadValue("TCP-IP", "Device") & My.Computer.Name
    Public usbSimplified As String = configIni.ReadValue("USB", "Simplified")
    Public usbProtocol As String = configIni.ReadValue("USB", "Protocol")
    Public usbDevice As String = configIni.ReadValue("USB", "Device")
    Public path7Zip As String = configIni.ReadValue("7Zip", "7ZipPath")
    Public myToolTip As New ToolTip
    Public sqlStringa As String = ""
    Public sqlCommand As New OleDbCommand(sqlStringa, objConn)
    Public dbReader As OleDbDataReader
    Public Trasporto As STraspSup = New STraspSup
    Public checkListStep As Schecklist = New Schecklist
    Public chkValore As Integer
    Public testValuesIni As New INIFile(localFolder & "\TestValues.ini")
    Public numeroFileAttuale As Integer = 0
    Public pulsantePremuto As String = ""
    Public customizationConnType As String = "LAN"
    Dim trace As STraceDirective = New STraceDirective()
    Public xmlLog As XMLFile
    Dim controllerType As String = "OLD"
    Const ENGICAM As String = "ENGICAM"
    Const OPAL As String = "OPAL"
    Const PXA As String = "PXA"
    Const FAIL As String = "FAIL"
    Const PASS As String = "PASS"
    Dim fwFileRealTime(4), fwFileFpga(4), fwFileOsc(4), fwFileUsbA(4), fwFileUsbB(4) As String
    Dim fwInstalledRealTime(4), fwInstalledFpga(4), fwInstalledOsc(4), fwInstalledUsbA(4), fwInstalledUsbB(4) As String

    Public Enum EXECUTION_STATE As Integer
        ES_CONTINUOUS = &H80000000
        ES_SYSTEM_REQUIRED = &H1
        ES_DISPLAY_REQUIRED = &H2
        ES_AWAYMODE_REQUIRED = &H40
    End Enum


    Public Declare Function SetThreadExecutionState Lib "kernel32" (ByVal esFlags As Long) As Long
    ''' <summary>
    ''' Impostazione i colodi dell'interfaccia
    ''' </summary>
    ''' <param name="stato">Normale, Errore, Attesa</param>
    Private Sub Interfaccia(Optional ByVal stato As String = "Normale")
        Select Case UCase(stato)
            Case "NORMALE"
                Me.BackColor = ArcaColor.GrigioChiaro
                tabella.BackgroundColor = ArcaColor.GrigioChiaro
                lblShiftCenter.BackColor = ArcaColor.GrigioChiaro
                cmdShiftOk.BackColor = ArcaColor.GrigioChiaro
                cmdShiftKo.BackColor = ArcaColor.GrigioChiaro
                cmdPrincipale.BackColor = ArcaColor.GrigioChiaro

                cmdComando1.BackColor = ArcaColor.Grigio
                cmdComando2.BackColor = ArcaColor.Grigio
                cmdComando3.BackColor = ArcaColor.Grigio
                cmdComando4.BackColor = ArcaColor.Grigio
                cmdDx.BackColor = ArcaColor.Grigio
                cmdSx.BackColor = ArcaColor.Grigio

                cmdShiftKo.ForeColor = ArcaColor.Arancio

                lblMessaggio.ForeColor = ArcaColor.Bianco
                lblMacchina.ForeColor = ArcaColor.Bianco

                tabella.ForeColor = ArcaColor.Nero
                cmdComando1.ForeColor = ArcaColor.Bianco
                cmdComando2.ForeColor = ArcaColor.Bianco
                cmdComando3.ForeColor = ArcaColor.Bianco
                cmdComando4.ForeColor = ArcaColor.Bianco
                cmdDx.ForeColor = ArcaColor.Bianco
                cmdSx.ForeColor = ArcaColor.Bianco
                lblShiftCenter.ForeColor = ArcaColor.Bianco
                cmdShiftOk.ForeColor = ArcaColor.Bianco
                cmdPrincipale.ForeColor = ArcaColor.Bianco
                lstChecklist.BackColor = ArcaColor.Bianco
                lstComScope.BackColor = ArcaColor.Bianco

            Case "ERRORE"
                Me.BackColor = ArcaColor.Arancio
                tabella.BackgroundColor = ArcaColor.Arancio
                lblShiftCenter.BackColor = ArcaColor.Arancio
                cmdShiftOk.BackColor = ArcaColor.Arancio
                cmdShiftKo.BackColor = ArcaColor.Arancio
                cmdPrincipale.BackColor = ArcaColor.Arancio

                cmdComando1.BackColor = ArcaColor.Grigio
                cmdComando2.BackColor = ArcaColor.Grigio
                cmdComando3.BackColor = ArcaColor.Grigio
                cmdComando4.BackColor = ArcaColor.Grigio
                cmdDx.BackColor = ArcaColor.Grigio
                cmdSx.BackColor = ArcaColor.Grigio

                cmdShiftKo.ForeColor = ArcaColor.Viola

                lblMessaggio.ForeColor = ArcaColor.Bianco
                lblMacchina.ForeColor = ArcaColor.Bianco

                tabella.ForeColor = ArcaColor.Nero
                cmdComando1.ForeColor = ArcaColor.Bianco
                cmdComando2.ForeColor = ArcaColor.Bianco
                cmdComando3.ForeColor = ArcaColor.Bianco
                cmdComando4.ForeColor = ArcaColor.Bianco
                cmdDx.ForeColor = ArcaColor.Bianco
                cmdSx.ForeColor = ArcaColor.Bianco
                lblShiftCenter.ForeColor = ArcaColor.Bianco
                cmdShiftOk.ForeColor = ArcaColor.Bianco
                cmdPrincipale.ForeColor = ArcaColor.Bianco
                lstChecklist.BackColor = ArcaColor.Bianco
                lstComScope.BackColor = ArcaColor.Bianco
            Case "ATTESA"
                Me.BackColor = ArcaColor.Verde
                tabella.BackgroundColor = ArcaColor.Verde
                lblShiftCenter.BackColor = ArcaColor.Verde
                cmdShiftOk.BackColor = ArcaColor.Verde
                cmdShiftKo.BackColor = ArcaColor.Verde
                cmdPrincipale.BackColor = ArcaColor.Verde

                cmdComando1.BackColor = ArcaColor.Grigio
                cmdComando2.BackColor = ArcaColor.Grigio
                cmdComando3.BackColor = ArcaColor.Grigio
                cmdComando4.BackColor = ArcaColor.Grigio
                cmdDx.BackColor = ArcaColor.Grigio
                cmdSx.BackColor = ArcaColor.Grigio

                cmdShiftKo.ForeColor = ArcaColor.Arancio

                lblMessaggio.ForeColor = ArcaColor.Nero
                lblMacchina.ForeColor = ArcaColor.Nero

                tabella.ForeColor = ArcaColor.Nero
                cmdComando1.ForeColor = ArcaColor.Bianco
                cmdComando2.ForeColor = ArcaColor.Bianco
                cmdComando3.ForeColor = ArcaColor.Bianco
                cmdComando4.ForeColor = ArcaColor.Bianco
                cmdDx.ForeColor = ArcaColor.Bianco
                cmdSx.ForeColor = ArcaColor.Bianco
                lblShiftCenter.ForeColor = ArcaColor.Bianco
                cmdShiftOk.ForeColor = ArcaColor.Bianco
                cmdPrincipale.ForeColor = ArcaColor.Bianco
                lstChecklist.BackColor = ArcaColor.Bianco
                lstComScope.BackColor = ArcaColor.Bianco
        End Select
    End Sub

    Function AggiornaControllerOpal()
        'Verifica:
        Dim elencoFW As String = ""
        AggiornaControllerOpal = cmdGetVersion("2") 'RTC
        If AggiornaControllerOpal = "OK" Then
            Trasporto.fwRTC = comandoSingoloRisposta(3) & comandoSingoloRisposta(4)
        End If

        AggiornaControllerOpal = cmdGetVersion("5") 'FPGA
        If AggiornaControllerOpal = "OK" Then
            Trasporto.fwFPGA = comandoSingoloRisposta(3) & comandoSingoloRisposta(4)
        End If

        AggiornaControllerOpal = cmdGetVersion("6") 'OSC
        If AggiornaControllerOpal = "OK" Then
            Trasporto.fwOSC = comandoSingoloRisposta(3) & comandoSingoloRisposta(4)
        End If

        objConn.Open()
        sqlStringa = "SELECT tblman.cod_fw FROM tblman where tblman.id_prodotto Like '" & cboIdProdotto.Text & "' and tblman.id_cliente like '" & Trasporto.ClienteID & "' and tblman.id_modulo='fw_prodotto'"
        sqlCommand = New OleDbCommand(sqlStringa, objConn)
        dbReader = sqlCommand.ExecuteReader
        dbReader.Read()
        Trasporto.CodiceFWSuite = dbReader(0)
        objConn.Close()
        lblMacchina.Text = Trasporto.MacchinaCodice & " - " & Trasporto.Categoria & "(" & Trasporto.ClienteNome & ") SUITE: " & Trasporto.CodiceFWSuite
copiafile:
        ScriviMessaggio(Messaggio(34), lblMessaggio) 'ricerca dei file da copiare in corso
        Dim nomeFile = Dir(localFWFolder & Trasporto.CodiceFWSuite & "\*.zip")
        If nomeFile = "" Then
            Interfaccia("errore")
            ScriviMessaggio(Messaggio(219), lblMessaggio) 'Non è stato impostato correttamente il drive di destinazione. Premere un tasto per ripetere, ESC per uscire.
            Select Case AttendiTasto()
                Case "ESC"
                    'CaricaChiavettaFW = "FOLDERKO"
                    Exit Function
                Case Else
                    GoTo copiafile
            End Select
        End If

        SvuotaCartellaDownload()
        ScriviMessaggio(Messaggio(34), lblMessaggio) 'Copia dei file della suite in locale in corso
        unzip(localFWFolder & Trasporto.CodiceFWSuite & "\" & nomeFile, localDLFolder)

        Dim driveID As Integer = 0
start:
        Dim volume As String = configIni.ReadValue("UsbKey", "volume")
        Dim drive As String = ""
        Interfaccia("attesa")
        ScriviMessaggio(String.Format(Messaggio(31), volume), lblMessaggio)
        AttendiTasto()
        Interfaccia("Normale")
        Try
            For i = 0 To My.Computer.FileSystem.Drives.Count - 1
                driveID = i
                If My.Computer.FileSystem.Drives(driveID).DriveType = DriveType.Removable Then
                    drive = My.Computer.FileSystem.Drives(driveID).Name
                    If UCase(My.Computer.FileSystem.Drives(driveID).VolumeLabel) = UCase(volume) Then
                        Exit For
                    Else
                        drive = ""
                    End If
                Else
                    drive = ""
                End If
            Next
driveSelect:
            If drive = "" Then
                Interfaccia("attesa")
                ScriviMessaggio(Messaggio(89), lblMessaggio)
                Dim folderBrower As FolderBrowserDialog = New FolderBrowserDialog
                folderBrower.RootFolder = Environment.SpecialFolder.MyComputer
                folderBrower.SelectedPath = "C:\"
                folderBrower.Description = Messaggio(32)

                If folderBrower.ShowDialog = DialogResult.OK Then
                    drive = folderBrower.SelectedPath
                End If
            End If

            If drive = "" Then
                Interfaccia("errore")
                ScriviMessaggio(Messaggio(219), lblMessaggio)
            End If

            CancellaDrive(drive)

            If Dir(localDLFolder).Length > 0 Then
                ScriviMessaggio(Messaggio(103), lblMessaggio) 'Ricerca e copia sulla chiavetta dei file da installare
                Dim folder As DirectoryInfo = New DirectoryInfo(localDLFolder)
                For Each fileFound As IO.FileInfo In folder.GetFiles
                    If fileFound.Name.StartsWith("FPGA") Then
                        If (fileFound.Name.Substring(5, 4) + fileFound.Name.Substring(10, 4) <> Trasporto.fwFPGA) Then
                            File.Copy(fileFound.FullName, drive & fileFound.Name)
                            elencoFW += " FPGA (" & fileFound.Name & ") "
                        End If

                    End If
                    If fileFound.Name.StartsWith("MC") Then
                        If (fileFound.Name.Substring(5, 4) + fileFound.Name.Substring(10, 4) <> Trasporto.fwRTC) Then
                            File.Copy(fileFound.FullName, drive & fileFound.Name)
                            elencoFW += " RTC (" & fileFound.Name & ") "
                        End If
                    End If
                    If fileFound.Name.StartsWith("xDLL") Then
                        If (fileFound.Name.Substring(5, 4) + fileFound.Name.Substring(10, 4) <> Trasporto.fwOSC) Then
                            elencoFW += " OSC (" & fileFound.Name & ") "
                            For Each fileOsc As FileInfo In folder.GetFiles
                                If fileOsc.Name.StartsWith("xDLL") Or fileOsc.Name.StartsWith("Wce") Or fileOsc.Name.StartsWith("BOOT") Or fileOsc.Name.StartsWith("CMUpgrade") Or fileOsc.Name.StartsWith("FF") Then
                                    File.Copy(fileOsc.FullName, drive & fileOsc.Name)
                                End If
                            Next
                        End If
                    End If


                Next
                'CaricaChiavettaFW = "OK"
            End If
        Catch
            Return "KO"
        End Try

        If elencoFW <> "" Then
            Interfaccia("attesa")
            ScriviMessaggio(String.Format(Messaggio(102), elencoFW), lblMessaggio) 'Estrarre la chiavetta usb dal pc ed inserirla nel trasporto in test, eseguire il download dei FW  {0}. Terminato il download premere INVIO
            AttendiTasto("INVIO")
            ScriviMessaggio(Messaggio(91), lblMessaggio) 'Per confermare la corretta installazione del firmware, premere 'L'.
            xmlLog.AddTest("fwInstall", Log.Result, "KO")
            Select Case AttendiTasto("L")
                Case "OK"
                    xmlLog.AddTestAttribute("fwInstall", Log.Result, "OK", "fwSuite", Trasporto.CodiceFWSuite)
                    Interfaccia("Normale")
                    If Trasporto.Categoria = "CM18SOLO" Or Trasporto.Categoria = "CM18SOLOT" Or Trasporto.Categoria = "OM61" Then
                        ScriviMessaggio(Messaggio(93), lblMessaggio) 'La categoria è CM18SOLO, CM18SOLOT o OM61, collegare il cavo USB alla porta frontale. Premere un tasto per continuare.
                        AttendiTasto()
                        tipoConnessione = "USB"
                        CaricaImpostazioneConnessione()
                        ApriConnessione()
                    End If
                Case Else
                    GoTo copiafile
            End Select
        End If
    End Function

    Function Md5Calculate(ByVal fileName As String) As String
        Dim sb As StringBuilder = New StringBuilder()
        Dim i As Double
        Try
            Using myMD5 As MD5 = MD5.Create()
                'ScriviMessaggio("Calcolo MD5 file " & fileName, lblMessaggio)
                Dim inputBytes() As Byte = File.ReadAllBytes(fileName)
                Dim hashBytes() As Byte = myMD5.ComputeHash(inputBytes)
                For i = 0 To hashBytes.Length - 1
                    sb.Append(hashBytes(i).ToString("X2"))
                Next
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        'ScriviMessaggio("Calcolo MD5 file " & fileName & ": " & sb.ToString(), lblMessaggio)
        Return sb.ToString()
    End Function


    Function PreparaSD()
        'inserire la SD nel PC
        'cercare nel dbfw i codici della customizazione e del FW
        'copiare la customizazione immagini 'IMBxx.ver' + jpg -> root\IMG
        'copiare la customizazione video 'FILMxx.ver' + wmv -> roo\FILM
        'copiare i file FW (tutti) -> root\Backup\FW\FFxx-yy  
        'inserire la SD nel controller, avviare la macchina ed attendere il recovery
        cboCliente.Visible = False
        cboIdProdotto.Visible = False
        PreparaSD = "OK"
        Dim driveSD As String = ""
        Dim suite As String = ""
        Dim custom As String = ""
        Dim cstFile As String = ""
        Dim filmVerFile As String = ""
        Dim imgVerFile As String = ""
        Dim upgFile As String = ""
        Dim fileReader As StreamReader
        Dim folderBrower As FolderBrowserDialog = New FolderBrowserDialog
        Dim md5Source As String = "Source"
        Dim md5Destination As String = "Destination"

        Try

            SvuotaCartellaDownload()

driveSelect:
            Interfaccia("attesa")
            ScriviMessaggio(Messaggio(104), lblMessaggio) 'Inserire la scheda SD nel PC, attendere che venga rilevata e premere un tasto per continuare
            AttendiTasto()
            folderBrower = New FolderBrowserDialog
            folderBrower.RootFolder = Environment.SpecialFolder.MyComputer
            folderBrower.SelectedPath = "C:\"
            folderBrower.Description = Messaggio(105) 'Selezionare il drive corrispondente alla scheda SD
            If folderBrower.ShowDialog = DialogResult.OK Then
                driveSD = folderBrower.SelectedPath
            End If
            If driveSD = "" Or driveSD = "C:\" Then
                Interfaccia("Errore")
                ScriviMessaggio(Messaggio(250), lblMessaggio) 'Non è stato selezionato un drive valido, premere un tasto per ripetere, L per saltare la preparazione della SD, ESC per uscire
                Select Case AttendiTasto()
                    Case "ESC"
                        PreparaSD = "KO"
                        Exit Function
                    Case "L"
                        PreparaSD = "OK"
                        Exit Function
                    Case Else
                        GoTo driveSelect
                End Select
            End If
            'aggiungere una conferma della selezione??

            SvuotaCartellaDownload()
            ScriviMessaggio(Messaggio(119), lblMessaggio) 'Svuotamento scheda SD in corso
            CancellaDrive(driveSD)
            objConn.Open()
            ScriviMessaggio(Messaggio(120), lblMessaggio) 'Raccolta informazioni da DB in corso
            sqlStringa = "Select Cod_fw FROM tblMAN WHERE id_modulo = 'IMMAGINI+VIDEO' AND id_prodotto = '" & cboIdProdotto.Text & "' AND id_cliente = " & Trasporto.ClienteID
            sqlCommand = New OleDbCommand(sqlStringa, objConn)
            dbReader = sqlCommand.ExecuteReader
            Do While Not dbReader.Read() = Nothing
                custom = dbReader(0)
            Loop
            If custom = "" Then
                Interfaccia("Errore")
                ScriviMessaggio(Messaggio(251), lblMessaggio) ' Non è stato possibile trovare il codice della customizzazione nel DBFW, premere un tasto per uscire
                PreparaSD = "KO"
                AttendiTasto()
                objConn.Close()
                Exit Function
            End If
            Trasporto.CustomizationCodice = custom

            sqlStringa = "Select Cod_fw FROM tblMAN WHERE id_modulo = 'FW_PRODOTTO' AND id_prodotto = '" & cboIdProdotto.Text & "' AND id_cliente = " & Trasporto.ClienteID
            sqlCommand = New OleDbCommand(sqlStringa, objConn)
            dbReader = sqlCommand.ExecuteReader
            Do While Not dbReader.Read() = Nothing
                suite = dbReader(0)
            Loop

            If suite = "" Then
                Interfaccia("Errore")
                ScriviMessaggio(Messaggio(252), lblMessaggio) ' Non è stato possibile trovare il codice della suite nel DBFW, premere un tasto per uscire
                PreparaSD = "KO"
                AttendiTasto()
                Exit Function
            End If
            Trasporto.CodiceFWSuite = suite

            Dim endUserCustomization As String = FAIL


            sqlStringa = "Select * FROM tblcustomization WHERE id_prodotto = '" & cboIdProdotto.Text & "' AND id_cliente = " & Trasporto.ClienteID
            sqlCommand = New OleDbCommand(sqlStringa, objConn)
            dbReader = sqlCommand.ExecuteReader
            Do While Not dbReader.Read() = Nothing
                endUserCustomization = PASS
                Trasporto.customCdfCode = dbReader("cdfcode").ToString
                Trasporto.customDisplayMessageCode = dbReader("id_displaymessage").ToString
                Trasporto.customMenuTree = dbReader("id_menutree").ToString
                Trasporto.customSkinCode = dbReader("id_skin").ToString
            Loop
            objConn.Close()

            lblMacchina.Text = Trasporto.MacchinaCodice & " - " & Trasporto.Categoria & "(" & Trasporto.ClienteNome & ") CUSTOMIZATION: " & Trasporto.CustomizationCodice & " - SUITE: " & Trasporto.CodiceFWSuite

            'customizzazione
            ScriviMessaggio(Messaggio(36), lblMessaggio) 'Copia in locale dei file per il download della customization in corso...
            For Each file As String In My.Computer.FileSystem.GetFiles(localFWFolder & Trasporto.CustomizationCodice, FileIO.SearchOption.SearchTopLevelOnly, "*.zip")
                Dim p As New Process
                p.StartInfo.UseShellExecute = True
                p.StartInfo.FileName = path7Zip & "\7z.exe"
                p.StartInfo.Arguments = " x " & """" & file & """" & " -o" & """" & localDLFolder & """" & " -aoa"
                p.StartInfo.RedirectStandardOutput = False
                p.Start()
                p.WaitForExit()
            Next
cercaCST:
            For Each file As String In My.Computer.FileSystem.GetFiles(localDLFolder, FileIO.SearchOption.SearchTopLevelOnly, "*.cst")
                cstFile = file
            Next
            If cstFile = "" Then
                Interfaccia("Errore")
                ScriviMessaggio(Messaggio(253), lblMessaggio) ' Non è stato possibile trovare il file 'CST' nella cartella 'download', premere un tasto per ripetere, ESC per uscire
                PreparaSD = "KO"
                Select Case AttendiTasto()
                    Case "ESC"
                        PreparaSD = "KO"
                        Exit Function
                    Case Else
                        Interfaccia()
                        GoTo cercaCST
                End Select
            End If


            fileReader = New StreamReader(cstFile)
            Dim line As String
            Do
                line = fileReader.ReadLine
                If line = Nothing Then
                    Exit Do
                End If
                If line.Substring(0, 1) = "9" Then
                    filmVerFile = line.Substring(2)
                    ScriviMessaggio(String.Format(Messaggio(106), filmVerFile), lblMessaggio) 'Sto copiando il file {0}
                    If Directory.Exists(driveSD & "FILM") = False Then My.Computer.FileSystem.CreateDirectory(driveSD & "FILM")

copiaFilmVerFile:
                    File.Copy(localDLFolder & filmVerFile, driveSD & "FILM\" & filmVerFile, True)
                    md5Source = Md5Calculate(localDLFolder & filmVerFile)
                    md5Destination = Md5Calculate(driveSD & "FILM\" & filmVerFile)
                    If md5Source <> md5Destination Then
                        Interfaccia("Errore")
                        ScriviMessaggio("Copia del file " & filmVerFile & " non riuscita correttamente, premi un tasto per ripetere o ESC per uscire", lblMessaggio)
                        If AttendiTasto() = "ESC" Then
                            PreparaSD = "KO"
                            Exit Function
                        End If
                        File.Delete(driveSD & "FILM\" & filmVerFile)
                        GoTo copiaFilmVerFile
                    End If
                End If

                If line.Substring(0, 2) = "10" Then
                    imgVerFile = line.Substring(3)
                    ScriviMessaggio(String.Format(Messaggio(106), imgVerFile), lblMessaggio) 'Sto copiando il file {0} 
                    If Directory.Exists(driveSD & "IMG") = False Then My.Computer.FileSystem.CreateDirectory(driveSD & "IMG")
                    md5Source = Md5Calculate(localDLFolder & imgVerFile)
copiaImgVerFile:
                    File.Copy(localDLFolder & imgVerFile, driveSD & "IMG\" & imgVerFile, True)
                    md5Destination = Md5Calculate(driveSD & "IMG\" & imgVerFile)
                    If md5Source <> md5Destination Then
                        Interfaccia("Errore")
                        ScriviMessaggio("Copia del file " & imgVerFile & " non riuscita correttamente, premi un tasto per ripetere o ESC per uscire", lblMessaggio)
                        If AttendiTasto() = "ESC" Then
                            PreparaSD = "KO"
                            Exit Function
                        End If
                        File.Delete(driveSD & "IMG\" & imgVerFile)
                        GoTo copiaImgVerFile
                    End If
                End If
            Loop
            fileReader.Close()

            fileReader = New StreamReader(localDLFolder & filmVerFile)
            Do
                line = fileReader.ReadLine
                If line = Nothing Then
                    Exit Do
                End If
                ScriviMessaggio("Sto copiando il file " & line, lblMessaggio)
                Me.Refresh()
                md5Source = Md5Calculate(localDLFolder & line)
copiaFilmFile:
                File.Copy(localDLFolder & line, driveSD & "FILM\" & line, True)
                md5Destination = Md5Calculate(driveSD & "FILM\" & line)
                If md5Source <> md5Destination Then
                    Interfaccia("Errore")
                    ScriviMessaggio("Copia del file " & line & " non riuscita correttamente, premi un tasto per ripetere o ESC per uscire", lblMessaggio)
                    If AttendiTasto() = "ESC" Then
                        PreparaSD = "KO"
                        Exit Function
                    End If
                    File.Delete(driveSD & "FILM\" & line)
                    GoTo copiaFilmFile
                End If
            Loop
            fileReader.Close()

            fileReader = New StreamReader(localDLFolder & imgVerFile)
            Do
                line = fileReader.ReadLine
                If line = Nothing Then
                    Exit Do
                End If
                ScriviMessaggio("Sto copiando il file " & line, lblMessaggio)
                Me.Refresh()
                md5Source = Md5Calculate(localDLFolder & line)
copiaImgFile:
                File.Copy(localDLFolder & line, driveSD & "IMG\" & line, True)
                md5Destination = Md5Calculate(driveSD & "IMG\" & line)
                If md5Source <> md5Destination Then
                    Interfaccia("Errore")
                    ScriviMessaggio("Copia del file " & line & " non riuscita correttamente, premi un tasto per ripetere o ESC per uscire", lblMessaggio)
                    If AttendiTasto() = "ESC" Then
                        PreparaSD = "KO"
                        Exit Function
                    End If
                    File.Delete(driveSD & "IMG\" & line)
                    GoTo copiaFilmFile
                End If
            Loop
            fileReader.Close()

            SvuotaCartellaDownload()
            'xmlLog.AddTest("customDownload", Log.Result, Trasporto.CustomizationCodice)

            'suite
            ScriviMessaggio(Messaggio(108), lblMessaggio) 'Copia in locale dei file per il download della suite in corso...
            For Each file As String In My.Computer.FileSystem.GetFiles(localFWFolder & Trasporto.CodiceFWSuite, FileIO.SearchOption.SearchTopLevelOnly, "*.zip")
                Dim p As New Process
                p.StartInfo.UseShellExecute = True
                p.StartInfo.FileName = path7Zip & "\7z.exe"
                p.StartInfo.Arguments = " x " & """" & file & """" & " -o" & """" & localDLFolder & """" & " -aoa"
                p.StartInfo.RedirectStandardOutput = False
                p.Start()
                p.WaitForExit()
            Next
            ' cercare file upg per creare la cartella con lo stesso nome

            If (Directory.Exists(localDLFolder + "\files")) Then
                localDLFolder = localFolder + "\Download\files"
            End If

cercaUPG:
            For Each file As String In My.Computer.FileSystem.GetFiles(localDLFolder, FileIO.SearchOption.SearchTopLevelOnly, "*.upg")
                upgFile = Path.GetFileNameWithoutExtension(file)
            Next
            If upgFile = "" Then
                Interfaccia("Errore")
                ScriviMessaggio(Messaggio(252), lblMessaggio) ' Non è stato possibile trovare il file 'UPG' nella cartella 'download', premere un tasto per ripetere, ESC per uscire
                PreparaSD = "KO"
                Select Case AttendiTasto()
                    Case "ESC"
                        PreparaSD = "KO"
                        Exit Function
                    Case Else
                        Interfaccia()
                        GoTo cercaUPG
                End Select
            End If
            Trasporto.SuiteCodiceCompleto = upgFile
            If Directory.Exists(driveSD & "Backup\FW\" & upgFile) = False Then My.Computer.FileSystem.CreateDirectory(driveSD & "Backup\FW\" & upgFile)

            For Each fwFile As String In My.Computer.FileSystem.GetFiles(localDLFolder, FileIO.SearchOption.SearchTopLevelOnly)
fileCopySd:
                md5Source = Md5Calculate(fwFile)
                ScriviMessaggio("Sto copiando il file " & Path.GetFileName(fwFile), lblMessaggio)
                Me.Refresh()
                File.Copy(fwFile, driveSD & "Backup\FW\" & upgFile & "\" & Path.GetFileName(fwFile), True)
                If Not File.Exists(driveSD & "Backup\FW\" & upgFile & "\" & Path.GetFileName(fwFile)) Then
                    MessageBox.Show("File non copiato")
                    GoTo fileCopySd
                End If
                md5Destination = Md5Calculate(driveSD & "Backup\FW\" & upgFile & "\" & Path.GetFileName(fwFile))
                If md5Destination <> md5Source Then
                    MessageBox.Show("MD5 diversi")
                End If
                Select Case Path.GetFileName(fwFile).Substring(0, 4) ' indici 0 e 1 = ENGICAM, 2 e 3 PXA
                    Case "MC1A"
                        fwFileRealTime(0) = Path.GetFileName(fwFile).Substring(5, 4)
                        fwFileRealTime(1) = Path.GetFileName(fwFile).Substring(10, 4)
                        Trasporto.fileEngicamRTC = Path.GetFileName(fwFile)
                    Case "MC19"
                        fwFileRealTime(2) = Path.GetFileName(fwFile).Substring(5, 4)
                        fwFileRealTime(3) = Path.GetFileName(fwFile).Substring(10, 4)
                        Trasporto.filePxaRTC = Path.GetFileName(fwFile)
                    Case "FPJA", "FPJB", "FPKA", "FPJA", "FPKB"
                        fwFileFpga(0) = Path.GetFileName(fwFile).Substring(5, 4)
                        fwFileFpga(1) = Path.GetFileName(fwFile).Substring(10, 4)
                        Trasporto.fileEngicamFPGA = Path.GetFileName(fwFile)
                    Case "FPGA"
                        fwFileFpga(2) = Path.GetFileName(fwFile).Substring(5, 4)
                        fwFileFpga(3) = Path.GetFileName(fwFile).Substring(10, 4)
                        Trasporto.filePxaFPGA = Path.GetFileName(fwFile)
                    'Case "Wce8"
                    '    fwFileOsc(0) = Path.GetFileName(fwFile).Substring(5, 4)
                    '    fwFileOsc(1) = Path.GetFileName(fwFile).Substring(10, 4)
                    '    Trasporto.fileEngicamOSC = Path.GetFileName(fwFile)
                    'Case "Wce5"
                    '    fwFileOsc(2) = Path.GetFileName(fwFile).Substring(5, 4)
                    '    fwFileOsc(3) = Path.GetFileName(fwFile).Substring(10, 4)
                    '    Trasporto.filePxaOSC = Path.GetFileName(fwFile)
                    Case "USB0"
                        fwFileUsbB(0) = Path.GetFileName(fwFile).Substring(5, 4)
                        fwFileUsbB(1) = Path.GetFileName(fwFile).Substring(10, 4)
                        fwFileUsbA(0) = Path.GetFileName(fwFile).Substring(5, 4)
                        fwFileUsbA(1) = Path.GetFileName(fwFile).Substring(10, 4)
                        Trasporto.fileEngicamUSB = Path.GetFileName(fwFile)
                    Case "BOOT"
                        If Path.GetExtension(fwFile) = ".osc" Then
                            If Path.GetFileName(fwFile).Substring(18, 1) = "8" Then
                                Trasporto.fileEngicamBoot = Path.GetFileName(fwFile)
                                fwFileOsc(0) = Path.GetFileName(fwFile).Substring(35, 4)
                                fwFileOsc(1) = Path.GetFileName(fwFile).Substring(40, 4)
                            Else
                                fwFileOsc(2) = Path.GetFileName(fwFile).Substring(35, 4)
                                fwFileOsc(3) = Path.GetFileName(fwFile).Substring(40, 4)
                            End If
                        End If
                End Select
                Me.Refresh()
            Next
            ScriviMessaggio("Copia file del FW terminata", lblMessaggio)
            Me.Refresh()
            SvuotaCartellaDownload()

            ' AGGIUNGERE LA COPIA ED IL CONTROLLO DEI FILE DELLA CUSTOMIZAZIONE
            If endUserCustomization = PASS Then
                If Trasporto.customCdfCode <> "" And Trasporto.customCdfCode <> "SKIP" Then

                End If
                If Trasporto.customDisplayMessageCode <> "" And Trasporto.customDisplayMessageCode <> "SKIP" Then

                End If
                If Trasporto.customMenuTree <> "" And Trasporto.customMenuTree <> "SKIP" Then

                End If
                If Trasporto.customSkinCode <> "" And Trasporto.customSkinCode <> "SKIP" Then

                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Return "FAIL"
        End Try

    End Function

    Function TestCat2Box()
        xmlLog.AddTest("testCRM", Log.Result, "KO")

openCommand:
        ScriviMessaggio("TEST-CRM: Comando OPEN in corso", lblMessaggio)
        TestCat2Box = cmdComandoSingolo("O,1,R,123456", 3)
        If TestCat2Box = "WRONG SIDE" Then
            TestCat2Box = cmdComandoSingolo("C,1,L", 3)
            TestCat2Box = cmdComandoSingolo("O,1,R,123456", 3)
        End If
        If TestCat2Box = "WRONG SIDE" Or TestCat2Box <> "OK" Then
            ScriviMessaggio("comando OPEN non riuscito, premi un tasto per ripetere, ESC per uscire", lblMessaggio, ArcaColor.Arancio)
            If AttendiTasto() = "ESC" Then Exit Function
            GoTo openCommand
        End If

        ScriviMessaggio("Inserisci il kit bn per il test Cat2Box (5x20$ + 3x20€ cat3 + 2x20€ cat2 e premi un tasto per avviare il deposito", lblMessaggio)
        AttendiTasto()
depositCommand:

        ScriviMessaggio("TEST-CRM: Deposito in corso", lblMessaggio)
        TestCat2Box = cmdComandoSingolo("D,1,R,0,0000", 3, 65000)
        Select Case TestCat2Box
            Case "OK"
                If Integer.Parse(comandoSingoloRisposta(6)) < 10 Then
                    ScriviMessaggio("Il cat2box è FULL prima di sfogliare tutto il kit, svuotarlo e inserire il kit nello sfogliatore (i dollari devono essere sfogliati per primi). Premi un tasto per ripetere, ESC per uscire", lblMessaggio, ArcaColor.Arancio)
                    If AttendiTasto() = "ESC" Then Exit Function
                    GoTo depositCommand
                End If
            Case "BUSY", "NO INPUT NOTE", "102", "4"
                ScriviMessaggio("Inserisci il kit bn per il test Cat2Box (5x20$ + 3x20€ cat3 + 2x20€ cat2), i dollari devono essere le prime, e premi un tasto per avviare il deposito", lblMessaggio)
                Aspetta(1000)
                GoTo depositCommand
            Case "CAT.2 BOX FULL", "230", "80"
                'se accettate = 0 e scartate = 10 -> OK
                If comandoSingoloRisposta(4) = "0" And comandoSingoloRisposta(6) = "10" Then
                    GoTo getSuspect
                End If
                If Integer.Parse(comandoSingoloRisposta(6)) < 10 Then
                    ScriviMessaggio("Il cat2box è FULL prima di sfogliare tutto il kit, svuotarlo e inserire il kit nello sfogliatore (i dollari devono essere sfogliati per primi). Premi un tasto per ripetere, ESC per uscire", lblMessaggio, ArcaColor.Arancio)
                    If AttendiTasto() = "ESC" Then Exit Function
                    GoTo depositCommand
                End If
                ScriviMessaggio("TEST-CRM: Banconote presenti nel cat2box. APRI il trasporto, rimuovi le bn ed attendi il reset. Premi un tasto per continuare o ESC per uscire", lblMessaggio)
                If AttendiTasto() = "ESC" Then
                    Exit Function
                End If
                GoTo depositCommand
            Case Else
                ScriviMessaggio("TEST-CRM: Deposito non riuscito. Stato:" & TestCat2Box & ". Premi un tasto per continuare, ESC per uscire", lblMessaggio, ArcaColor.Arancio)
                If AttendiTasto() = "ESC" Then
                    Exit Function
                End If
                GoTo depositCommand
        End Select

getSuspect:
        'T,n,0,86
        TestCat2Box = cmdComandoSingolo("T,1,0,86", 4,)
        Select Case TestCat2Box
            Case "OK"
                If comandoSingoloRisposta(5) <> "CPEA" And comandoSingoloRisposta(6) <> "2" Then
                    ScriviMessaggio("Attese n° 2 bn CPEA sospette (Cat2), ma risultano n°" & comandoSingoloRisposta(6) & " bn " & comandoSingoloRisposta(5) & ". Svuota il cat2box e premi un tasto per ripetere o ESC per uscire", lblMessaggio, ArcaColor.Arancio)
                    If AttendiTasto() = "ESC" Then Exit Function
                    GoTo openCommand
                End If
            Case Else
                ScriviMessaggio("TEST-CRM: Deposito non riuscito. Stato:" & TestCat2Box & ". Premi un tasto per continuare, ESC per uscire", lblMessaggio, ArcaColor.Arancio)
                If AttendiTasto() = "ESC" Then
                    Exit Function
                End If
                GoTo getSuspect
        End Select
getNotAuth:
        'T,n,0,38
        TestCat2Box = cmdComandoSingolo("T,1,0,38", 4,)
        Select Case TestCat2Box
            Case "OK"
                If comandoSingoloRisposta(5) <> "CPEA" And comandoSingoloRisposta(6) <> "3" Then
                    ScriviMessaggio("Attese n° 3 bn CPEA non autenticate (Cat3), ma risultano n° " & comandoSingoloRisposta(6) & " bn " & comandoSingoloRisposta(5) & ". Svuota il cat2box e premi un tasto per ripetere il test o ESC per uscire", lblMessaggio, ArcaColor.Arancio)
                    If AttendiTasto() = "ESC" Then Exit Function
                    GoTo openCommand
                End If
            Case Else
                ScriviMessaggio("TEST-CRM: Deposito non riuscito. Stato:" & TestCat2Box & ". Premi un tasto per continuare, ESC per uscire", lblMessaggio, ArcaColor.Arancio)
                If AttendiTasto() = "ESC" Then
                    Exit Function
                End If
                GoTo getNotAuth
        End Select
getDetail:
        'T,n,0,87(,s)
        TestCat2Box = cmdComandoSingolo("T,1,0,87", 4,)
        Select Case TestCat2Box
            Case "OK"
                If comandoSingoloRisposta(5) <> "5" Then
                    ScriviMessaggio("Attese n°5 bn nel cat2box ma rilevate " & comandoSingoloRisposta(5) & ".Svuota il cat2box e premi un tasto per ripetere il test o ESC per uscire", lblMessaggio, ArcaColor.Arancio)
                    If AttendiTasto() = "ESC" Then Exit Function
                    GoTo openCommand
                End If
                If comandoSingoloRisposta(6) <> "04" Then
                    ScriviMessaggio("Il cat2box non risulta FULL, svuotalo e premi un tasto per ripetere il test o ESC per uscire", lblMessaggio, ArcaColor.Arancio)
                    If AttendiTasto() = "ESC" Then Exit Function
                    GoTo depositCommand
                End If

            Case Else
                ScriviMessaggio("TEST-CRM: Deposito non riuscito. Stato:" & TestCat2Box & ". Premi un tasto per continuare, ESC per uscire", lblMessaggio, ArcaColor.Arancio)
                If AttendiTasto() = "ESC" Then
                    Exit Function
                End If
                GoTo getDetail
        End Select

    End Function

    Function DisplaySelection()
        xmlLog.AddTestAttribute("SetDisplay", Log.Result, "KO")
        'Get T,1,6,30 (T,1,6,30,rc,display)
        'Fill Z,n,6,30,dispID(4-5-6)
verificaDisplay:
        Interfaccia("Normale")
        Dim display As String = "KO"
        Dim displayType As String
        ScriviMessaggio("Verificare che il display si veda correttamente, premere un tasto per continuare, INVIO per cambiare il tipo di display", lblMessaggio)
        If AttendiTasto() = "INVIO" Then
            DisplaySelection = cmdComandoSingolo("T,1,6,30", 4)
            Select Case DisplaySelection
                Case "SYNTAX ERROR", "SYNTAX ERROR OR COMMAND NOT AVAILABLE"
                    ScriviMessaggio(Messaggio(255), lblMessaggio, ArcaColor.Arancio) 'Il firmware non è in grado di cambiare il tipo di display montato. Premere 'R' per continuare ugualmente, ESC per uscire
                    If AttendiTasto() = "R" Then
                        DisplaySelection = "OK"
                    End If
                    Exit Function
                Case "OK"
                Case Else
                    Exit Function
            End Select
            displayType = comandoSingoloRisposta(5)
            Select Case displayType
                Case "4"
                    displayType = "TIANMA"
                Case "5"
                    displayType = "HITACHI"
                Case "6"
                    displayType = "DLC"
            End Select

premiTasto:
            ScriviMessaggio(String.Format("L'attuale display impostato è {0}, premere 4 per TIANMA, 5 per HITACHI, 6 per DLC. ESC per uscire", displayType), lblMessaggio)
            Select Case AttendiTasto()
                Case "4"
                    displayType = "4"
                Case "5"
                    displayType = "5"
                Case "6"
                    displayType = "6"
                Case Else
                    GoTo premiTasto
            End Select
            DisplaySelection = cmdComandoSingolo("Z,1,6,30," & displayType, 4)
            If DisplaySelection <> "OK" Then
                'ripeti / esci
                Select Case DisplaySelection
                    Case "SYNTAX ERROR", "SYNTAX ERROR OR COMMAND NOT AVAILABLE"
                        ScriviMessaggio(Messaggio(255), lblMessaggio, ArcaColor.Arancio) 'Il firmware non è in grado di cambiare il tipo di display montato. Premere 'R' per continuare ugualmente, ESC per uscire
                        If AttendiTasto() = "R" Then
                            DisplaySelection = "OK"
                        End If
                        Exit Function
                End Select
            End If

            ScriviMessaggio("Recovery in corso...", lblMessaggio)
            cmdReset()
            GoTo verificaDisplay
        End If

        display = "OK"
        DisplaySelection = display


    End Function

    Function CercaSuiteProdotto()
        CercaSuiteProdotto = PASS
        Dim driveSD As String = ""
        Dim suite As String = ""
        Dim custom As String = ""
        Dim cstFile As String = ""
        Dim filmVerFile As String = ""
        Dim imgVerFile As String = ""
        Dim upgFile As String = ""
        Dim fileReader As StreamReader
        Dim folderBrower As FolderBrowserDialog = New FolderBrowserDialog
        Dim md5Source As String = "Source"
        Dim md5Destination As String = "Destination"

        Try

            SvuotaCartellaDownload()

            objConn.Open()
            ScriviMessaggio(Messaggio(120), lblMessaggio) 'Raccolta informazioni da DB in corso
            sqlStringa = "Select Cod_fw FROM tblMAN WHERE id_modulo = 'IMMAGINI+VIDEO' AND id_prodotto = '" & cboIdProdotto.Text & "' AND id_cliente = " & Trasporto.ClienteID
            sqlCommand = New OleDbCommand(sqlStringa, objConn)
            dbReader = sqlCommand.ExecuteReader
            Do While Not dbReader.Read() = Nothing
                custom = dbReader(0)
            Loop
            If custom = "" Then
                Interfaccia("Errore")
                ScriviMessaggio(Messaggio(251), lblMessaggio) ' Non è stato possibile trovare il codice della customizzazione nel DBFW, premere un tasto per uscire
                CercaSuiteProdotto = "KO"
                AttendiTasto()
                objConn.Close()
                Exit Function
            End If
            Trasporto.CustomizationCodice = custom

            sqlStringa = "Select Cod_fw FROM tblMAN WHERE id_modulo = 'FW_PRODOTTO' AND id_prodotto = '" & cboIdProdotto.Text & "' AND id_cliente = " & Trasporto.ClienteID
            sqlCommand = New OleDbCommand(sqlStringa, objConn)
            dbReader = sqlCommand.ExecuteReader
            Do While Not dbReader.Read() = Nothing
                suite = dbReader(0)
            Loop

            If custom = "" Then
                Interfaccia("Errore")
                ScriviMessaggio(Messaggio(252), lblMessaggio) ' Non è stato possibile trovare il codice della suite nel DBFW, premere un tasto per uscire
                CercaSuiteProdotto = "KO"
                AttendiTasto()
                Exit Function
            End If
            Trasporto.CodiceFWSuite = suite

            objConn.Close()

            lblMacchina.Text = Trasporto.MacchinaCodice & " - " & Trasporto.Categoria & "(" & Trasporto.ClienteNome & ") CUSTOMIZATION: " & Trasporto.CustomizationCodice & " - SUITE: " & Trasporto.CodiceFWSuite


            'suite
            ScriviMessaggio(Messaggio(108), lblMessaggio) 'Copia in locale dei file per il download della suite in corso...
            For Each file As String In My.Computer.FileSystem.GetFiles(localFWFolder & Trasporto.CodiceFWSuite, FileIO.SearchOption.SearchTopLevelOnly, "*.zip")
                Dim p As New Process
                p.StartInfo.UseShellExecute = True
                p.StartInfo.FileName = path7Zip & "\7z.exe"
                p.StartInfo.Arguments = " x " & """" & file & """" & " -o" & """" & localDLFolder & """" & " -aoa"
                p.StartInfo.RedirectStandardOutput = False
                p.Start()
                p.WaitForExit()
            Next
            ' cercare file upg per creare la cartella con lo stesso nome
cercaUPG:
            For Each file As String In My.Computer.FileSystem.GetFiles(localDLFolder, FileIO.SearchOption.SearchTopLevelOnly, "*.upg")
                upgFile = Path.GetFileNameWithoutExtension(file)
            Next
            If upgFile = "" Then
                Interfaccia("Errore")
                ScriviMessaggio(Messaggio(252), lblMessaggio) ' Non è stato possibile trovare il file 'UPG' nella cartella 'download', premere un tasto per ripetere, ESC per uscire
                CercaSuiteProdotto = "KO"
                Select Case AttendiTasto()
                    Case "ESC"
                        CercaSuiteProdotto = "KO"
                        Exit Function
                    Case Else
                        Interfaccia()
                        GoTo cercaUPG
                End Select
            End If
            Trasporto.SuiteCodiceCompleto = upgFile
            If Directory.Exists(driveSD & "Backup\FW\" & upgFile) = False Then My.Computer.FileSystem.CreateDirectory(driveSD & "Backup\FW\" & upgFile)

            For Each fwFile As String In My.Computer.FileSystem.GetFiles(localDLFolder, FileIO.SearchOption.SearchTopLevelOnly)
fileCopySd:

                Select Case Path.GetFileName(fwFile).Substring(0, 4) ' indici 0 e 1 = ENGICAM, 2 e 3 PXA
                    Case "MC1A"
                        fwFileRealTime(0) = Path.GetFileName(fwFile).Substring(5, 4)
                        fwFileRealTime(1) = Path.GetFileName(fwFile).Substring(10, 4)
                        Trasporto.fileEngicamRTC = Path.GetFileName(fwFile)
                    Case "MC19"
                        fwFileRealTime(2) = Path.GetFileName(fwFile).Substring(5, 4)
                        fwFileRealTime(3) = Path.GetFileName(fwFile).Substring(10, 4)
                        Trasporto.filePxaRTC = Path.GetFileName(fwFile)
                    Case "FPJA"
                        fwFileFpga(0) = Path.GetFileName(fwFile).Substring(5, 4)
                        fwFileFpga(1) = Path.GetFileName(fwFile).Substring(10, 4)
                        Trasporto.fileEngicamFPGA = Path.GetFileName(fwFile)
                    Case "FPGA"
                        fwFileFpga(2) = Path.GetFileName(fwFile).Substring(5, 4)
                        fwFileFpga(3) = Path.GetFileName(fwFile).Substring(10, 4)
                        Trasporto.filePxaFPGA = Path.GetFileName(fwFile)
                    'Case "Wce8"
                    '    fwFileOsc(0) = Path.GetFileName(fwFile).Substring(5, 4)
                    '    fwFileOsc(1) = Path.GetFileName(fwFile).Substring(10, 4)
                    '    Trasporto.fileEngicamOSC = Path.GetFileName(fwFile)
                    'Case "Wce5"
                    '    fwFileOsc(2) = Path.GetFileName(fwFile).Substring(5, 4)
                    '    fwFileOsc(3) = Path.GetFileName(fwFile).Substring(10, 4)
                    '    Trasporto.filePxaOSC = Path.GetFileName(fwFile)
                    Case "USB0"
                        fwFileUsbB(0) = Path.GetFileName(fwFile).Substring(5, 4)
                        fwFileUsbB(1) = Path.GetFileName(fwFile).Substring(10, 4)
                        fwFileUsbA(0) = Path.GetFileName(fwFile).Substring(5, 4)
                        fwFileUsbA(1) = Path.GetFileName(fwFile).Substring(10, 4)
                        Trasporto.fileEngicamUSB = Path.GetFileName(fwFile)
                    Case "BOOT"
                        If Path.GetExtension(fwFile) = ".osc" Then
                            If Path.GetFileName(fwFile).Substring(18, 1) = "8" Then
                                Trasporto.fileEngicamBoot = Path.GetFileName(fwFile)
                                fwFileOsc(0) = Path.GetFileName(fwFile).Substring(35, 4)
                                fwFileOsc(1) = Path.GetFileName(fwFile).Substring(40, 4)
                            Else
                                fwFileOsc(2) = Path.GetFileName(fwFile).Substring(35, 4)
                                fwFileOsc(3) = Path.GetFileName(fwFile).Substring(40, 4)
                            End If
                        End If
                End Select
                Me.Refresh()
            Next
            ScriviMessaggio("Copia file del FW terminata", lblMessaggio)
            Me.Refresh()
            SvuotaCartellaDownload()


        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Return "FAIL"
        End Try

    End Function

    Function TestTrasportoSuperiore() As String

        xmlLog = New XMLFile("logTestTrasportoSuperiore.xml", TestType.TraspSup)
        myMacchina = New SMacchina
        myMacchina.Info.Categoria = Trasporto.Categoria
        If Trasporto.Categoria = "CM18SOLO" Or Trasporto.Categoria = "CM18SOLOT" Or Trasporto.Categoria = "OM61" Then
            ScriviMessaggio(Messaggio(93), lblMessaggio) 'La categoria è CM18SOLO, CM18SOLOT o OM61, collegare il cavo USB alla porta frontale. Premere un tasto per continuare.
            AttendiTasto()
            ConnectionParam.ConnectionMode = DLINKMODEUSB
        End If
        'GoTo codiceTrasporto

preparzioneSD:
        Interfaccia("Normale")
        ScriviMessaggio("Premi un tasto per continuare senza preparare la SD, INVIO per prepararla, ESC per uscire", lblMessaggio)
        cboCliente.Visible = False
        cboIdProdotto.Visible = False
        Select Case AttendiTasto()
            Case "ESC"
                TestTrasportoSuperiore = "SDKO"
                Exit Function
            Case "INVIO"
                TestTrasportoSuperiore = PreparaSD()
                Select Case TestTrasportoSuperiore
                    Case "OK"

                    Case Else
                        Interfaccia("Errore")
                        ScriviMessaggio(Messaggio(247), lblMessaggio, ArcaColor.Arancio) 'FALLITA creazione scheda SD, premere un tasto per ripetere, ESC per uscire
                        Select Case AttendiTasto()
                            Case "ESC"
                                TestTrasportoSuperiore = "SDKO"
                                Exit Function
                            Case Else
                                GoTo preparzioneSD
                        End Select
                End Select
                ScriviMessaggio(Messaggio(107), lblMessaggio) 'Estrarre la scheda SD dal PC, inserirla nel controller, accendere la macchine e premere un tasto per continuare
                AttendiTasto()
            Case Else
                TestTrasportoSuperiore = CercaSuiteProdotto()
                Select Case TestTrasportoSuperiore
                    Case PASS

                    Case Else
                        Interfaccia("Errore")
                        ScriviMessaggio(Messaggio(247), lblMessaggio, ArcaColor.Arancio) 'FALLITA creazione scheda SD, premere un tasto per ripetere, ESC per uscire
                        Select Case AttendiTasto()
                            Case "ESC"
                                TestTrasportoSuperiore = "SDKO"
                                Exit Function
                            Case Else
                                GoTo preparzioneSD
                        End Select
                End Select
        End Select

        'TestTrasportoSuperiore = PreparaSD()
        'Select Case TestTrasportoSuperiore
        '    Case "OK"

        '    Case Else
        '        Interfaccia("Errore")
        '        ScriviMessaggio(Messaggio(247), lblMessaggio, ArcaColor.Arancio) 'FALLITA creazione scheda SD, premere un tasto per ripetere, ESC per uscire
        '        Select Case AttendiTasto()
        '            Case "ESC"
        '                TestTrasportoSuperiore = "SDKO"
        '                Exit Function
        '            Case Else
        '                GoTo preparzioneSD
        '        End Select
        'End Select

        'ScriviMessaggio(Messaggio(107), lblMessaggio) 'Estrarre la scheda SD dal PC, inserirla nel controller, accendere la macchine e premere un tasto per continuare
        'AttendiTasto()
codiceTrasporto:
        Interfaccia("Normale")
        TestTrasportoSuperiore = InserimentoCodiceTrasporto()
        Select Case TestTrasportoSuperiore
            Case "CodiceTrasportoKO"
                Exit Function
            Case "OK"
        End Select

        TestTrasportoSuperiore = InserimentoMatricola()
        Select Case TestTrasportoSuperiore
            Case "MatricolaTrasportoKO"
                Exit Function
            Case "OK"
        End Select

        TestTrasportoSuperiore = InserimentoMatricolaController()
        Select Case TestTrasportoSuperiore
            Case "MatricolaControllerKO"
                Exit Function
            Case "OK"
                Select Case Trasporto.MatricolaController.Substring(1, 5)
                    Case "13396"
                        Trasporto.tipoController = OPAL
                    Case "13307", "13306"
                        Trasporto.tipoController = PXA
                    Case Else
                        Trasporto.tipoController = ENGICAM
                End Select
        End Select
        Trasporto.inizioData = Format(Now, "dd/MM/yy")
        Trasporto.inizioOra = Format(Now, "HH:mm")

        xmlLog.AddProduct(Trasporto.Matricola, Log.Result, "false", Log.DateStart, Trasporto.inizioData, Log.TimeStart, Trasporto.inizioOra, Log.Family, Trasporto.Categoria, Log.ClientName, Trasporto.ClienteNome, Log.ClientId, Trasporto.ClienteID, "code", Trasporto.Codice, "productCode", Trasporto.MacchinaCodice)
        xmlLog.AddModule("Controller", "code", Trasporto.MatricolaController)



Connetti:
        ScriviMessaggio(Messaggio(256), lblMessaggio) 'Premere un tasto per avviare la connessione
        AttendiTasto()
        Interfaccia("Normale")
        If ApriConnessione() = False Then
            Interfaccia("Errore")
            ScriviMessaggio(Messaggio(247), lblMessaggio, ArcaColor.Arancio) 'Connessione non riuscita, premere un tasto per ripetere, ESC per uscire
            Select Case AttendiTasto()
                Case "ESC"
                    TestTrasportoSuperiore = Messaggio(303) 'ConnessioneKO
                    Exit Function
                Case Else
                    GoTo Connetti
            End Select
        End If
        'GoTo cat2box

firmwareControllerOpal:
        'Verificare quali firmware è necessario installare (RTC-FPGA-OSC)
        If Trasporto.tipoController = "OPAL" Then
            TestTrasportoSuperiore = AggiornaControllerOpal()
        End If

selezioneDisplay:
        xmlLog.AddTestAttribute("SetDisplay", Log.Result, "UNNECESSARY")
        Select Case Trasporto.tipoController
            Case OPAL

            Case PXA

            Case ENGICAM
setDLC:
                TestTrasportoSuperiore = cmdComandoSingolo("Z,1,6,30,6", 4)
                If TestTrasportoSuperiore <> "OK" Then
                    Select Case TestTrasportoSuperiore
                        Case <> "OK"
                            xmlLog.AddTestAttribute("SetDisplay", Log.Result, "DLC - " & TestTrasportoSuperiore)
                            ScriviMessaggio(String.Format(Messaggio(255), TestTrasportoSuperiore), lblMessaggio, ArcaColor.Arancio) 'Impostazione display DLC non riuscita. Stato macchina: {0}. Premi un tasto per ripetere, ESC per uscire
                            If AttendiTasto() = "ESC" Then
                                TestTrasportoSuperiore = FAIL
                                Exit Function
                            End If
                            GoTo setDLC
                    End Select
                End If
                xmlLog.AddTestAttribute("SetDisplay", Log.Result, "DLC - " & TestTrasportoSuperiore)
                'RESET
                Interfaccia("Normale")
                ScriviMessaggio(Messaggio(69), lblMessaggio) 'RECOVERY in corso.
                cmdReset()
                Aspetta(1000)
        End Select



open:
        xmlLog.AddTest("openL", Log.Result, "KO")
        ScriviMessaggio(String.Format(Messaggio(50), Chr(lpLEFTSIDE)), lblMessaggio) 'OPEN {0} in corso...
        Interfaccia("Normale")
        'cmdComandoSingolo("O,1,L,123456", 4)
        TestTrasportoSuperiore = cmdOpen(Chr(lpLEFTSIDE))
        Select Case TestTrasportoSuperiore
            Case "OK", "LOW LEVEL CONTROLLER COMMUNICATION ERROR"
            Case Else
                Interfaccia("Errore")
                ScriviMessaggio(String.Format(Messaggio(231), Chr(lpLEFTSIDE), TestTrasportoSuperiore), lblMessaggio) 'Comando OPEN {0} non è stato eseguito correttamente, risposta: {1}. Premere un tasto per ripetere, ESC per uscire.
                Select Case AttendiTasto()
                    Case "ESC"
                        Exit Function
                    Case Else
                        GoTo open
                End Select
        End Select
        xmlLog.AddTestAttribute("openL", Log.Result, TestTrasportoSuperiore)

impostaDataEOra:
        xmlLog.AddTest("setDateTime", Log.Result, "KO")
        Interfaccia("Normale")
        ScriviMessaggio(Messaggio(51), lblMessaggio) 'Impostazione DATA e ORA in corso.
        TestTrasportoSuperiore = ImpostaDataOra()
        Select Case TestTrasportoSuperiore
            Case "OK"
            Case Else
                Interfaccia("errore")
                ScriviMessaggio(String.Format(Messaggio(232), TestTrasportoSuperiore), lblMessaggio) 'Impostazione DATA e ORA non riuscita, risposta: {0}. Premere un tasto per ripetere, ESC per uscire.
                Select Case AttendiTasto()
                    Case "ESC"
                        Exit Function
                    Case Else
                        GoTo impostaDataEOra
                End Select
        End Select
        xmlLog.AddTestAttribute("setDateTime", Log.Result, "OK")
        'Test alimentatore , spostato all'inizio 01/03/2017
alimentatore:

        'DAVIDE - 28/02/2017
        'Interfaccia("Normale")
        'ScriviMessaggio(Messaggio(69), lblMessaggio)
        'cmdReset()
        'Aspetta(1000)
        xmlLog.AddTest("powerSupply", Log.Result, "KO")
        Interfaccia("Normale")
        ScriviMessaggio(Messaggio(53), lblMessaggio) 'Test ALIMENTATORE in corso.
        TestTrasportoSuperiore = TestAlimentatore()
        If TestTrasportoSuperiore <> "OK" Then
            Interfaccia("errore")
            Select Case TestTrasportoSuperiore
                Case "OK"
                'GoTo alimentatore
                Case "AliVonMotOnKO"
                    ScriviMessaggio(Messaggio(211), lblMessaggio) 'Tensione di alimentazione a pieno carico non corretta. Premere un tasto per uscire.
                    Select Case AttendiTasto()
                        Case "ESC"
                            Exit Function
                        Case Else
                            GoTo alimentatore
                    End Select
                Case "AliVonMotOffKO"
                    ScriviMessaggio(Messaggio(210), lblMessaggio) 'Tensione di alimentazione a vuoto non corretta. Premere un tasto per uscire.
                    Select Case AttendiTasto()
                        Case "ESC"
                            Exit Function
                        Case Else
                            GoTo alimentatore
                    End Select
                Case "AliVoff"
                    ScriviMessaggio(Messaggio(209), lblMessaggio) 'Tensione ad alimentazione spenta troppo alta. Premere un tasto per uscire.
                    Select Case AttendiTasto()
                        Case "ESC"
                            Exit Function
                        Case Else
                            GoTo alimentatore
                    End Select
                Case "RelOld" 'firmware vecchio
                Case Else
                    ScriviMessaggio(String.Format(Messaggio(233), TestTrasportoSuperiore), lblMessaggio) 'Test ALIMENTATORE non eseguito correttamente, risposta: {0}. Premere un tasto per ripetere, ESC per uscire.
                    Select Case AttendiTasto()
                        Case "ESC"
                            Exit Function
                        Case Else
                            GoTo alimentatore
                    End Select

            End Select
        End If
        xmlLog.AddTestAttribute("powerSupply", Log.Result, "OK", "Voff", Trasporto.AliVOff, "VonMotOff", Trasporto.AliVonMotOff, "VonMotOn", Trasporto.AliVonMotOn)
        'Trasporto.AliVOff = -1
        'Trasporto.AliVonMotOff = -1
        'Trasporto.AliVonMotOn = -1

        'GoTo devicename

checklist:
        Interfaccia("Normale")
        ScriviMessaggio(Messaggio(52), lblMessaggio) 'Verifica CHECKLIST.
        TestTrasportoSuperiore = EsaminaChecklist()
        Select Case TestTrasportoSuperiore
            Case "OK"
            Case Else
                Interfaccia("errore")
                ScriviMessaggio(String.Format(Messaggio(244), TestTrasportoSuperiore), lblMessaggio)
                Select Case AttendiTasto()
                    Case "ESC"
                        TestTrasportoSuperiore = "ChecklistKO"
                        Exit Function
                    Case Else
                        GoTo checklist
                End Select
        End Select





installazioneFW:
        If Trasporto.tipoController = PXA Then GoTo tipocontroller
        Interfaccia("attesa")
        'ScriviMessaggio(Messaggio(109), lblMessaggio) 'Eseguire il download dei FW RealTime(MB.. o MC..), FPGA(FPGA..) e OSC(Boot..). Durante il download posizionare i tappi su tutte le seriali presenti. Terminato il download premere INVIO
        'ScriviMessaggio(Messaggio(111), lblMessaggio) 'Esegui il download dei firmware RealTime e OSC. Con controller ENGICAM installare MC1A... e ...Wince8..., con PXA installare MC19... e ...Wince5... Terminato il download premere INVIO

        ScriviMessaggio("Passaggio a protocollo semplificato", lblMessaggio)
        TestTrasportoSuperiore = cmdComandoSingolo("u,1,6,1", 4)
        Select Case TestTrasportoSuperiore
            Case "OK"
            Case Else
                ScriviMessaggio("Non è stato possibile passare al protocollo semplificato, premi un tasto per riprovare, ESC per uscire", lblMessaggio, ArcaColor.Arancio)
                If AttendiTasto() <> "ESC" Then GoTo installazioneFW
                Exit Function
        End Select
        ScriviMessaggio("Chiusura connessione in corso", lblMessaggio)
        ChiudiConnessione()
        Aspetta(1000)
        Dim oldLinkMode As Byte = ConnectionParam.ConnectionMode
        ConnectionParam.ConnectionMode = DLINKMODESERIALEASY
        If oldLinkMode = DLINKMODEUSB Then ConnectionParam.ConnectionMode = DLINKMODEUSBEASY
        ScriviMessaggio("Apertura connessione in corso", lblMessaggio)
        ApriConnessione()
        Aspetta(1000)
setDownloadDirectory:
        ScriviMessaggio("Impostazione directory di download", lblMessaggio)
        TestTrasportoSuperiore = cmdComandoSingolo("L,1,6,S,8,,,," & Trasporto.SuiteCodiceCompleto, 2)
        Select Case TestTrasportoSuperiore
            Case "OK"
            Case Else
                ScriviMessaggio("Non è stato possibile impostare correttamente la directory di download, premi un tasto per riprovare, ESC per uscire", lblMessaggio, ArcaColor.Arancio)
                If AttendiTasto() <> "ESC" Then GoTo setDownloadDirectory
                Exit Function
        End Select
tipocontroller:
        Select Case Trasporto.tipoController
            Case PXA
                ScriviMessaggio(Messaggio(116), lblMessaggio) 'Esegui il download dei firmware RealTime (MB.. o MC19..), FPGA(FPGA..) e OSC(Wince5..). Terminato il download premere INVIO
                AttendiTasto("INVIO")
                ScriviMessaggio(Messaggio(91), lblMessaggio) 'Per confermare la corretta installazione del firmware, premere 'L'.
                xmlLog.AddTest("fwInstall", Log.Result, "KO")
                Select Case AttendiTasto("L")
                    Case "OK"
                        xmlLog.AddTestAttribute("fwInstall", Log.Result, "OK")
                        xmlLog.AddTestAttribute("fwInstall", "fwSuite", Trasporto.CodiceFWSuite)
                        Interfaccia("Normale")

                    Case Else
                        GoTo installazioneFW
                End Select
                GoTo devicename
            Case ENGICAM
                'RealTime
getRealTimeFw:
                ScriviMessaggio("Richiesta versione REAL TIME installato in corso", lblMessaggio)
                TestTrasportoSuperiore = cmdComandoSingolo("V,1,2", 2)
                Select Case TestTrasportoSuperiore
                    Case "OK"
                    Case Else
                        ScriviMessaggio("Non è stato possibile reperire la versione di REAL TIME installato, premi un tasto per riprovare, ESC per uscire", lblMessaggio, ArcaColor.Arancio)
                        If AttendiTasto() <> "ESC" Then GoTo getRealTimeFw
                        Exit Function
                End Select
                fwInstalledRealTime(0) = comandoSingoloRisposta(3)
                fwInstalledRealTime(1) = comandoSingoloRisposta(4)
                If (fwInstalledRealTime(0) <> fwFileRealTime(0) Or fwInstalledRealTime(1) <> fwFileRealTime(1)) Then
                    'Installa realtime
rtInstal:
                    ScriviMessaggio("Installazione in corso del file " & Trasporto.fileEngicamRTC, lblMessaggio)
                    TestTrasportoSuperiore = cmdComandoSingolo("L,1,2,F,,,,," & Trasporto.fileEngicamRTC, 2, 900000)
                    Select Case TestTrasportoSuperiore
                        Case "OK"
                        Case Else
                            ScriviMessaggio(comandoSingoloRisposta(2) & ". Installazione REAL TIME non riuscita, premi un tasto per ripetere la verifica, ESC per uscire", lblMessaggio, ArcaColor.Arancio)
                            If AttendiTasto() <> "ESC" Then GoTo getRealTimeFw
                            cmdComandoSingolo("u,1,6,2")
                            Exit Function
                    End Select
                    cmdReset()
                End If

                'FPGA
getFpgaFw:
                ScriviMessaggio("Richiesta versione FPGA installato in corso", lblMessaggio)
                TestTrasportoSuperiore = cmdComandoSingolo("V,1,5", 2)
                Select Case TestTrasportoSuperiore
                    Case "OK"
                    Case Else
                        ScriviMessaggio("Non è stato possibile reperire la versione di FPGA installato, premi un tasto per riprovare, ESC per uscire", lblMessaggio, ArcaColor.Arancio)
                        If AttendiTasto() <> "ESC" Then GoTo getFpgaFw
                        Exit Function
                End Select
                fwInstalledFpga(0) = comandoSingoloRisposta(3)
                fwInstalledFpga(1) = comandoSingoloRisposta(4)
                ' disattivata installazione FPGA - 13/01/22

                'If (fwInstalledFpga(0) <> fwFileFpga(0) Or fwInstalledFpga(1) <> fwFileFpga(1)) Then
                '    'Installa FPGA
                '    ScriviMessaggio("Installazione " & Trasporto.fileEngicamFPGA & " in corso", lblMessaggio)
                '    TestTrasportoSuperiore = cmdComandoSingolo("L,1,5,F,,,,," & Trasporto.fileEngicamFPGA, 2, 2100000)
                '    Select Case TestTrasportoSuperiore
                '        Case "OK"
                '        Case Else
                '            ScriviMessaggio(comandoSingoloRisposta(2) & ". Installazione FPGA non riuscita, premi un tasto per ripetere la verifica, ESC per uscire", lblMessaggio, ArcaColor.Arancio)
                '            If AttendiTasto() <> "ESC" Then GoTo getRealTimeFw
                '            cmdComandoSingolo("u,1,6,2")
                '            Exit Function
                '    End Select
                'End If

                'USBA
getUsbAFw:
                ScriviMessaggio("Richiesta versione USB A installato in corso", lblMessaggio)
                TestTrasportoSuperiore = cmdComandoSingolo("V,1,u", 2)
                Select Case TestTrasportoSuperiore
                    Case "OK"
                    Case Else
                        ScriviMessaggio("Non è stato possibile reperire la versione del chip USB A installato, premi un tasto per riprovare, ESC per uscire", lblMessaggio, ArcaColor.Arancio)
                        If AttendiTasto() <> "ESC" Then GoTo getUsbAFw
                        Exit Function
                End Select
                fwInstalledUsbA(0) = comandoSingoloRisposta(3)
                fwInstalledUsbA(1) = comandoSingoloRisposta(4)
                If (fwInstalledUsbA(0) <> fwFileUsbA(0) Or fwInstalledUsbA(1) <> fwFileUsbA(1)) Then
                    ScriviMessaggio("Versione FW Chip USB A non corretto, è necessario eseguire l'installazione da display. Premi un tasto per ripetere la verifica, ESC per proseguire ugualmente", lblMessaggio)
                    If AttendiTasto() <> "ESC" Then GoTo getUsbAFw

                    'ScriviMessaggio("Installazione " & Trasporto.fileEngicamUSB & " in corso", lblMessaggio)
                    'TestTrasportoSuperiore = cmdComandoSingolo("L,1,u,F,,,,," & Trasporto.fileEngicamUSB, 2, 2100000)
                    'Select Case TestTrasportoSuperiore
                    '    Case "OK"
                    '    Case Else
                    '        ScriviMessaggio("Stato " & comandoSingoloRisposta(2), lblMessaggio)
                    '        'gestione errore
                    'End Select
                End If

                'USBB
getUsbBFw:
                If myMacchina.Info.Categoria <> "CM18SOLO" And myMacchina.Info.Categoria <> "CM18SOLOT" And myMacchina.Info.Categoria <> "OM61" Then
                    ScriviMessaggio("Richiesta versione USB B installato in corso", lblMessaggio)
                    TestTrasportoSuperiore = cmdComandoSingolo("V,1,v", 2)
                    Select Case TestTrasportoSuperiore
                        Case "OK"
                        Case Else
                            ScriviMessaggio("Non è stato possibile reperire la versione del chip USB B installato, premi un tasto per riprovare, ESC per uscire", lblMessaggio, ArcaColor.Arancio)
                            If AttendiTasto() <> "ESC" Then GoTo getUsbBFw
                            Exit Function
                    End Select
                    fwInstalledUsbB(0) = comandoSingoloRisposta(3)
                    fwInstalledUsbB(1) = comandoSingoloRisposta(4)
                    If (fwInstalledUsbB(0) <> fwFileUsbB(0) Or fwInstalledUsbB(1) <> fwFileUsbB(1)) Then
                        ScriviMessaggio("Versione FW Chip USB A non corretto, è necessario eseguire l'installazione da display. Premi un tasto per ripetere la verifica, ESC per proseguire ugualmente", lblMessaggio)
                        If AttendiTasto() <> "ESC" Then GoTo getUsbBFw
                        'ScriviMessaggio("Installazione " & Trasporto.fileEngicamUSB & " in corso", lblMessaggio)
                        'TestTrasportoSuperiore = cmdComandoSingolo("L,1,v,F,,,,," & Trasporto.fileEngicamUSB, 2, 2100000)
                        'Select Case TestTrasportoSuperiore
                        '    Case "OK"
                        '    Case Else
                        '        ScriviMessaggio("Stato " & comandoSingoloRisposta(2), lblMessaggio)
                        '        'gestione errore
                        'End Select
                    End If
                End If
                'OSC
getOscFw:
                Dim installaSempre As Boolean = True
                ScriviMessaggio("Richiesta versione OSC installato in corso", lblMessaggio)
                TestTrasportoSuperiore = cmdComandoSingolo("V,1,6", 2)
                Select Case TestTrasportoSuperiore
                    Case "OK"
                    Case Else
                        ScriviMessaggio("Non è stato possibile reperire la versione di OSC installato, premi un tasto per riprovare, ESC per uscire", lblMessaggio, ArcaColor.Arancio)
                        If AttendiTasto() <> "ESC" Then GoTo getOscFw
                        Exit Function
                End Select
                fwInstalledOsc(0) = comandoSingoloRisposta(3)
                fwInstalledOsc(1) = comandoSingoloRisposta(4)
                If (fwInstalledOsc(0) <> fwFileOsc(0) Or fwInstalledOsc(1) <> fwFileOsc(1) Or installaSempre = True) Then
                    'cmdComandoSingolo("L,1,6,F,,,,," & Trasporto.fileEngicamBoot, 2, 900000)

                    ScriviMessaggio("Installazione " & Trasporto.fileEngicamBoot & " in corso", lblMessaggio)
                    TestTrasportoSuperiore = cmdComandoSingolo("L,6,6,E,0,CMUpdater.exe,,,", 2, 5000)
                    Select Case TestTrasportoSuperiore
                        Case "OK"
                            Aspetta(10000)
connessione:
                            ScriviMessaggio("Attendi la fine dell'installazione e premi un tasto per continuare", lblMessaggio)
                            AttendiTasto()
testConnessione:
                            ApriConnessione()
                            cmdComandoSingolo("E,1", 1, 1000)
                            If comandoSingoloRisposta.Length = 1 Then
                                If ConnectionParam.ConnectionMode = DLINKMODESERIAL Or ConnectionParam.ConnectionMode = DLINKMODEUSB Then
                                    ScriviMessaggio(comandoSingoloRisposta(2) & ". Installazione OSC non riuscita, premi un tasto per ripetere, ESC per uscire", lblMessaggio, ArcaColor.Arancio)
                                    If AttendiTasto() <> "ESC" Then GoTo getOscFw
                                End If
                                If ConnectionParam.ConnectionMode = DLINKMODESERIALEASY Then ConnectionParam.ConnectionMode = DLINKMODESERIAL
                                If ConnectionParam.ConnectionMode = DLINKMODEUSBEASY Then ConnectionParam.ConnectionMode = DLINKMODEUSB
                                GoTo testConnessione
                            End If
                            'cmdReset()
                            GoTo getOscFw
                        Case "SYSTEM ERROR", "UNEXPECTED COMMAND"
                        Case Else
                            If comandoSingoloRisposta.Length > 1 Then
                                If (comandoSingoloRisposta(2) <> "999" And comandoSingoloRisposta(2) <> "221") Then
                                    ScriviMessaggio("Stato " & comandoSingoloRisposta(2), lblMessaggio)
                                    'gestione errore
                                    ScriviMessaggio(comandoSingoloRisposta(2) & ". Installazione OSC non riuscita, premi un tasto per ripetere la verifica, C per continuare ugualmente, ESC per uscire", lblMessaggio, ArcaColor.Arancio)
                                    Select Case AttendiTasto()
                                        Case "ESC"
                                        Case "C"
                                            GoTo continua
                                        Case Else
                                            GoTo getOscFw
                                    End Select
                                    If AttendiTasto() <> "ESC" Then GoTo getOscFw
                                    cmdComandoSingolo("u,1,6,2")
                                    Exit Function
                                End If
                            Else
                                ScriviMessaggio("Nessuna risposta dalla macchina, premi un tasto per ripetere, ESC per uscire", lblMessaggio)
                                Select Case AttendiTasto()
                                    Case "ESC"
                                    Case "C"
                                        GoTo continua
                                    Case Else
                                        GoTo getOscFw
                                End Select
                                If AttendiTasto() <> "ESC" Then GoTo testConnessione
                                Exit Function
                            End If
                    End Select
                End If
continua:
                xmlLog.AddTestAttribute("fwInstall", Log.Result, "OK")
                xmlLog.AddTestAttribute("fwInstall", "fwSuite", Trasporto.CodiceFWSuite)
                Interfaccia("Normale")
                'ScriviMessaggio(Messaggio(117), lblMessaggio) 'Esegui il download dei firmware RealTime (MC1A..) e OSC(Wince8..). Terminato il download premere INVIO
            Case Else
                ScriviMessaggio(Messaggio(111), lblMessaggio) 'Controller non riconosciuto, esegui il download dei firmware. ENGICAM=Realtime(MC1A..), OSC(Wince8..) / PXA=Realtime(MC19..), FPGA(FPGA..) e OSC(Wince5..). Terminato il download premere INVIO
                AttendiTasto("INVIO")
                ScriviMessaggio(Messaggio(91), lblMessaggio) 'Per confermare la corretta installazione del firmware, premere 'L'.
                xmlLog.AddTest("fwInstall", Log.Result, "KO")
                Select Case AttendiTasto("L")
                    Case "OK"
                        xmlLog.AddTestAttribute("fwInstall", Log.Result, "OK")
                        xmlLog.AddTestAttribute("fwInstall", "fwSuite", Trasporto.CodiceFWSuite)
                        Interfaccia("Normale")

                    Case Else
                        GoTo installazioneFW
                End Select
        End Select
        If ConnectionParam.ConnectionMode = DLINKMODEUSBEASY Or ConnectionParam.ConnectionMode = DLINKMODESERIALEASY Then TestTrasportoSuperiore = cmdComandoSingolo("u,1,6,2")
cambiaConnessione:
        ChiudiConnessione()
        Aspetta(1000)
        ConnectionParam.ConnectionMode = oldLinkMode
        Dim connessione As Boolean = ApriConnessione()
        If connessione = False Then
            If oldLinkMode = DLINKMODESERIAL Then ConnectionParam.ConnectionMode = DLINKMODESERIALEASY
            If oldLinkMode = DLINKMODEUSB Then ConnectionParam.ConnectionMode = DLINKMODEUSBEASY
            ChiudiConnessione()
            Aspetta(1000)
            connessione = ApriConnessione()
            If connessione = True Then
                TestTrasportoSuperiore = cmdComandoSingolo("U,1,6,2", 3)
                ChiudiConnessione()
                ConnectionParam.ConnectionMode = oldLinkMode
                Aspetta(1000)
                connessione = ApriConnessione()
                If connessione = True Then GoTo postFw
                ScriviMessaggio(Messaggio(268), lblMessaggio, ArcaColor.Arancio) 'Connessione non riuscita, premi un tasto per riprovare o ESC per uscire
                Select Case AttendiTasto()
                    Case "ESC"
                        Exit Function
                    Case Else
                        GoTo cambiaConnessione

                End Select
            End If
        End If
postFw:
        Aspetta(1000)

        If Trasporto.Categoria = "CM18SOLO" Or Trasporto.Categoria = "CM18SOLOT" Or Trasporto.Categoria = "OM61" Then
            ScriviMessaggio(Messaggio(93), lblMessaggio) 'La categoria è CM18SOLO, CM18SOLOT o OM61, collegare il cavo USB alla porta frontale. Premere un tasto per continuare.
            AttendiTasto()
            tipoConnessione = "USB"
            CaricaImpostazioneConnessione()
            ApriConnessione()
        End If

        'Taratura Foto
foto:
        Interfaccia("Normale")
        ScriviMessaggio(Messaggio(54), lblMessaggio) 'Taratura FOTO in corso.
        TestTrasportoSuperiore = TaraturaFoto()
        Select Case TestTrasportoSuperiore
            Case "OK"
            Case Else
                Interfaccia("errore")
                ScriviMessaggio(String.Format(Messaggio(234), TestTrasportoSuperiore), lblMessaggio)
                Select Case AttendiTasto()
                    Case "ESC"
                        Exit Function
                    Case Else
                        GoTo foto
                End Select
        End Select

        'Test Foto forchetta
forchetta:
        Interfaccia("Normale")
        ScriviMessaggio(Messaggio(55), lblMessaggio)
        TestTrasportoSuperiore = TestFotoForchetta()
        Select Case TestTrasportoSuperiore
            Case "OK"
            Case Else
                Interfaccia("errore")
                ScriviMessaggio(String.Format(Messaggio(235), TestTrasportoSuperiore), lblMessaggio)
                Select Case AttendiTasto()
                    Case "ESC"
                        Exit Function
                    Case Else
                        GoTo forchetta
                End Select
        End Select

macAddress:
        Interfaccia("Normale")
        TestTrasportoSuperiore = ImpostaMacAddress()
        Select Case TestTrasportoSuperiore
            Case "OK"
            Case Else
                Interfaccia("errore")
                ScriviMessaggio(String.Format(Messaggio(236), TestTrasportoSuperiore), lblMessaggio)
                Select Case AttendiTasto()
                    Case "ESC"
                        Exit Function
                    Case Else
                        GoTo macAddress
                End Select
        End Select

devicename:
        customizationConnType = configIni.ReadValue("ForceDownloadType", Trasporto.MacchinaCodice)
        If customizationConnType = "Failed" Then customizationConnType = "LAN"
        If customizationConnType <> "USB" Then
            'Imposta Device Name (LAN)
            Interfaccia("Normale")
            ScriviMessaggio(Messaggio(56), lblMessaggio) 'Impostazione DHCP in corso.
            TestTrasportoSuperiore = ImpostaDHCP(True)
            Select Case TestTrasportoSuperiore
                Case "OK"
                Case Else
                    Interfaccia("errore")
                    ScriviMessaggio(String.Format(Messaggio(236), TestTrasportoSuperiore), lblMessaggio) 'Impostazione DHCP non eseguita correttamete, risposta: {0}. Premere un tasto per ripetere, ESC per uscire.
                    Select Case AttendiTasto()
                        Case "ESC"
                            Exit Function
                        Case Else
                            GoTo devicename
                    End Select
            End Select

            'RESET
            Interfaccia("Normale")
            ScriviMessaggio(Messaggio(69), lblMessaggio) 'RECOVERY in corso.
            cmdReset()
            Aspetta(1000)
        End If
        ApriConnessione()
        ScriviMessaggio(Messaggio(113), lblMessaggio) 'Richiesta indirizzo ip in corso
        TestTrasportoSuperiore = cmdComandoSingolo("T,1,6,29,2", 4)
        If TestTrasportoSuperiore <> "OK" And TestTrasportoSuperiore <> "1" And TestTrasportoSuperiore <> "101" Then
            If TestTrasportoSuperiore = "201" Or TestTrasportoSuperiore = "3" Or TestTrasportoSuperiore = "SYNTAX ERROR OR COMMAND NOT AVAILABLE" Or TestTrasportoSuperiore = "SYNTAX ERROR" Then GoTo selezioneDisplay
            Interfaccia("Errore")
            ScriviMessaggio(String.Format(Messaggio(257), TestTrasportoSuperiore), lblMessaggio) 'Non è stato possibile reperire l'indirizzo IP per effettuare il test. Premere un tasto per ripetere, ESC per uscire.
            Select Case AttendiTasto()
                Case "ESC"
                    Exit Function
                Case Else
                    GoTo devicename
            End Select
        End If
        lanDevice = comandoSingoloRisposta(5)
        Dim oldConnMode As Byte = ConnectionParam.ConnectionMode
connessioneLan:
        ScriviMessaggio(Messaggio(112), lblMessaggio) 'Test LAN in corso
        ChiudiConnessione()
        ConnectionParam.TcpIpPar.clientIpAddr = lanDevice
        ConnectionParam.TcpIpPar.portNumber = 8101
        ConnectionParam.ConnectionMode = DLINKMODETCPIP
        ScriviMessaggio("Connessione LAN in corso", lblMessaggio)
        If ApriConnessione() = False Then
            ScriviMessaggio(String.Format(Messaggio(258), TestTrasportoSuperiore), lblMessaggio) 'Test LAN FALLITO. Premere un tasto per ripetere, ESC per uscire.
            Select Case AttendiTasto()
                Case "ESC"
                    Exit Function
                Case Else
                    GoTo connessioneLan
            End Select
        End If

        ChiudiConnessione()
        ConnectionParam.ConnectionMode = oldConnMode
        ApriConnessione()

        'customdwl:
        '        'Download Customization
        '        Interfaccia("Normale")
        '        ScriviMessaggio(Messaggio(58), lblMessaggio) 'Download CUSTOMIZATION in corso.
        'TestTrasportoSuperiore = DownloadCustomization()
        '        Select Case TestTrasportoSuperiore
        '            Case "OK"
        '            Case Else
        '                Interfaccia("Errore")
        '                ScriviMessaggio(String.Format(Messaggio(238), TestTrasportoSuperiore), lblMessaggio) 'Download CUSTOMIZATION non eseguito correttamete. Premere un tasto per ripetere, ESC per uscire.
        '                Select Case AttendiTasto()
        '                    Case "ESC"
        '                        Exit Function
        '                    Case Else
        '                        GoTo customdwl
        '                End Select
        '        End Select

        '        'faccio cmq un recovery dopo il download del fw DAVIDE
        '        Interfaccia("Normale")
        '        ScriviMessaggio(Messaggio(69), lblMessaggio) 'RECOVERY in corso.
        '        cmdReset()
        '        Aspetta(1000)

tipomacchina:
        'DAVIDE
        'Impostazione tipo macchina
        Interfaccia("Normale")
        ScriviMessaggio(Messaggio(59), lblMessaggio) 'Impostazione TIPO MACCHINA in corso.
        TestTrasportoSuperiore = ImpostaTipoMacchina()

        Select Case TestTrasportoSuperiore
            Case "OK"
            Case "SYNTAX Error"
                Interfaccia("Errore")
                ScriviMessaggio(String.Format(Messaggio(249), TestTrasportoSuperiore), lblMessaggio) 'Risposta: {0}. Verificare che le immagini siano state caricate correttamente. Premere un tasto per ripetere, ESC per uscire
                Select Case AttendiTasto()
                    Case "ESC"
                        Exit Function
                    Case Else
                        GoTo tipomacchina
                End Select
            Case Else
                Interfaccia("errore")
                ScriviMessaggio(String.Format(Messaggio(239), TestTrasportoSuperiore), lblMessaggio) 'Impostazione TIPO MACCHINA non eseguita correttamete, risposta: {0}. Premere un tasto per ripetere, ESC per uscire.
                Select Case AttendiTasto()
                    Case "ESC"
                        Exit Function
                    Case Else
                        GoTo tipomacchina
                End Select
        End Select

        'NON SERVE DAVIDE
        'reset
        'ScriviMessaggio(Messaggio(61), lblMessaggio)
        'cmdReset()
        'Serve una sleep qui ? 

cassettenumber:
        'Impostazione Cassette Number
        Interfaccia("Normale")
        ScriviMessaggio(Messaggio(60), lblMessaggio) 'Impostazione CASSETTE NUMBER per muletto in corso.
        TestTrasportoSuperiore = ImpostaCassetteNumber(True)
        Select Case TestTrasportoSuperiore
            Case "OK"
            Case Else
                Interfaccia("Errore")
                ScriviMessaggio(String.Format(Messaggio(240), TestTrasportoSuperiore), lblMessaggio)
                Select Case AttendiTasto()
                    Case "ESC"
                        Exit Function
                    Case Else
                        GoTo cassettenumber
                End Select
        End Select

        'reset - attivato 07*03*2017
        Interfaccia("Normale")
        ScriviMessaggio(Messaggio(61), lblMessaggio)
        cmdReset()
        Aspetta(1000)

velocità: 'AGGIUNGERE LOG XML -- Aggiunto log, da verificare (11/10/21)
        'Test Velocità MSO
        Interfaccia("Normale")
        ScriviMessaggio(Messaggio(62), lblMessaggio)
        TestTrasportoSuperiore = TestVelocitàMSO()
        Select Case TestTrasportoSuperiore
            Case "OK"
            Case Else
                Interfaccia("Errore")
                ScriviMessaggio(Messaggio(241), lblMessaggio)
                Select Case AttendiTasto()
                    Case "ESC"
                        Exit Function
                    Case Else
                        GoTo velocità
                End Select
        End Select

shiftSet:
        'Shift Center set
        Interfaccia("Normale")
        ScriviMessaggio(Messaggio(63), lblMessaggio)
        TestTrasportoSuperiore = SetShiftCenterPcloseDelay()
        Select Case TestTrasportoSuperiore
            Case "OK"
            Case Else
                Interfaccia("errore")
                ScriviMessaggio(Messaggio(242), lblMessaggio)
                Select Case AttendiTasto()
                    Case "ESC"
                        Exit Function
                    Case Else
                        GoTo shiftSet
                End Select
        End Select

sfogliatura:
        'Sfoglia 200bn
        Interfaccia("Normale")
        ScriviMessaggio(Messaggio(64), lblMessaggio)
        TestTrasportoSuperiore = SfogliaBN()
        Select Case TestTrasportoSuperiore
            Case "OK"
            Case Else
                Interfaccia("Errore")
                ScriviMessaggio(String.Format(Messaggio(243), TestTrasportoSuperiore), lblMessaggio)
                Select Case AttendiTasto()
                    Case "ESC"
                        Exit Function
                    Case Else
                        GoTo sfogliatura
                End Select
        End Select

shiftChange:
        'Shift center change
        Interfaccia("Normale")
        ScriviMessaggio(Messaggio(65), lblMessaggio)
        TestTrasportoSuperiore = ShiftCenterChange()
        Select Case TestTrasportoSuperiore
            Case "OK"
            Case "SHIFTCHANGED"
                GoTo sfogliatura
            Case Else
                Interfaccia("errore")
                ScriviMessaggio(String.Format(Messaggio(242), TestTrasportoSuperiore), lblMessaggio)
                Select Case AttendiTasto()
                    Case "ESC"
                        Exit Function
                    Case Else
                        GoTo shiftChange
                End Select
        End Select

cat2box:
        If myMacchina.Info.CRM = 1 Then
            Interfaccia("Normale")
            ScriviMessaggio(Messaggio(121), lblMessaggio) 'Test cassettino dei falsi/sospetti (CAT2BOX)
            TestTrasportoSuperiore = TestCat2Box()
            Select Case TestTrasportoSuperiore
                Case "OK"

                Case Else
                    Interfaccia("errore")
                    ScriviMessaggio("Test cat2Box fallito. Premi un tasto per ripetere o ESC per uscire", lblMessaggio)
                    Select Case AttendiTasto()
                        Case "ESC"
                            Exit Function
                        Case Else
                            GoTo cat2box
                    End Select
            End Select
        End If

cassette:
        'Cassette number
        Interfaccia("Normale")
        ScriviMessaggio(Messaggio(66), lblMessaggio)
        TestTrasportoSuperiore = ImpostaCassetteNumber(False)
        Select Case TestTrasportoSuperiore
            Case "OK"
            Case Else
                Interfaccia("errore")
                ScriviMessaggio(String.Format(Messaggio(240), TestTrasportoSuperiore), lblMessaggio)
                Select Case AttendiTasto()
                    Case "ESC"
                        Exit Function
                    Case Else
                        GoTo cassette
                End Select
        End Select

impdeviceName:
        'rimuovi device name (LAN)
        Interfaccia("Normale")
        ScriviMessaggio(Messaggio(67), lblMessaggio) 'Ripristino impostazione DHCP in corso.
        TestTrasportoSuperiore = ImpostaDHCP(False)
        Select Case TestTrasportoSuperiore
            Case "OK"
                'davide 27/02
                Interfaccia("Normale")
                ScriviMessaggio(Messaggio(69), lblMessaggio) 'RECOVERY in corso.
                cmdReset()
                Aspetta(1000)
            Case Else
                Interfaccia("errore")
                ScriviMessaggio(String.Format(Messaggio(236), TestTrasportoSuperiore), lblMessaggio) 'Impostazione DHCP non eseguita correttamete, risposta: {0}. Premere un tasto per ripetere, ESC per uscire.
                Select Case AttendiTasto()
                    Case "ESC"
                        Exit Function
                    Case Else
                        GoTo impdeviceName
                End Select
        End Select

initLog:
        Interfaccia()
        TestTrasportoSuperiore = InitLog()
        Select Case TestTrasportoSuperiore
            Case "OK"
                Aspetta(100)
            Case Else
                Interfaccia("errore")
                ScriviMessaggio(String.Format(Messaggio(236), TestTrasportoSuperiore), lblMessaggio) 'Impostazione DHCP non eseguita correttamete, risposta: {0}. Premere un tasto per ripetere, ESC per uscire.
                Select Case AttendiTasto()
                    Case "ESC"
                        Exit Function
                    Case Else
                        GoTo initLog
                End Select
        End Select
    End Function

    Function InitLog()
inizio:
        'ScriviMessaggio(Messaggio(101), lblMessaggio) 'Inizializzazione error log in corso
        'InitLog = cmdComandoSingolo("T,1,0,17", 5, 5000)
        'If InitLog <> "OK" And InitLog <> "NO ELECTRONIC JOURNAL" Then
        '    Exit Function
        'End If
        ScriviMessaggio(Messaggio(101), lblMessaggio) 'Inizializzazione error log in corso
        InitLog = cmdComandoSingolo("H,1,IIII", 2, 15000)
    End Function

    Function SalvaLog() As Boolean
        SalvaLog = True
        Dim infoMacchina, infoAlimentatore, infoFoto, infoVelocità As String
        Dim rigaLog As String
        Dim sep As String = " | "
        Dim intestazione As String = ""
        With Trasporto
            infoMacchina = .MacchinaCodice & sep & .ClienteID & sep & .Codice & sep & .Matricola & sep & .MatricolaController & sep & .inizioData & sep & .inizioOra &
                sep & .TestResult & sep & .checklist & sep & Application.ProductVersion.ToString & sep & System.Windows.Forms.SystemInformation.ComputerName
            infoAlimentatore = .AliVOff & sep & .AliVonMotOff & sep & .AliVonMotOn & sep & .TestResult

            infoFoto = .fotoRealTime.Count.Valore & sep & .fotoRealTime.Feed.Valore & sep & .fotoRealTime.C1.Valore & sep & .fotoRealTime.HMax.Valore &
                 sep & .fotoRealTime.InCenter.Valore & sep & .fotoRealTime.InBox.Valore & sep & .fotoRealTime.InLeft.Valore & sep & .fotoRealTime.InQ.Valore &
                 sep & .fotoRealTime.Out.Valore & sep & .fotoRealTime.C3.Valore & sep & .fotoRealTime.C4A.Valore & sep & .fotoRealTime.C4B.Valore & sep & .fotoRealTime.Rej.Valore &
                 sep & .fotoRealTime.Shift.Valore & sep & .fotoSafe.CashLeft.Valore & sep & .fotoSafe.CashRight.Valore & sep & .fotoForchetta

            infoVelocità = .VelTrasp.Minima & sep & .VelTrasp.Media & sep & .VelTrasp.Massima

        End With

        intestazione = "ProductCode|ClientID|TransportCode|TransportSN|ControllerSN|DateStart|TimeStart|TestResult|ChecklistResult|SWVersion|ComputerName|" _
            & "AliVOff|AliVOnMotOff|AliVOnMotOn|Count|Feed|C1|Hmax|InCenter|InBox|InLeft|InQ|Out|C3|C4A|C4B|Rej|Shift|CashLeft|CashRight|Forchetta|" _
            & "VelMin|VelAvg|VelMax|DateEnd|TimeEnd|TestResult"
        rigaLog = infoMacchina & sep & infoAlimentatore & sep & infoFoto & sep & infoVelocità & Trasporto.fineData & sep & Trasporto.fineOra

        If Not File.Exists(localFolder & "\LogTrasportoSuperiore.txt") Then
            Using fileLog As StreamWriter = File.CreateText(localFolder & "\LogTrasportoSuperiore.txt")
                fileLog.WriteLine(intestazione)
                fileLog.Close()
            End Using
        End If
        Using fileLog As StreamWriter = File.AppendText(localFolder & "\LogTrasportoSuperiore.txt")
            fileLog.WriteLine(rigaLog)
            fileLog.Close()
        End Using

        SalvaLogServer(localFolder & "\LogTrasportoSuperiore.txt")
    End Function

    Sub SalvaLogServer(nomeFile As String)
        Dim tracciamento As String = ""
        Dim iRiga As Integer = 1
        Try
            tracciamento += "apro il file origine"
            Dim fileOrigine As StreamReader = New StreamReader(nomeFile)
            tracciamento += "_Ho aperto il file origine"
            tracciamento += "_Apro il file destinazione"
            Using fileDestinazione As StreamWriter = File.AppendText(serverLogFolder & "LogTrasportoSuperiore.txt")
                tracciamento += "_Leggo il file origine"
                Do While fileOrigine.Peek() >= 0
                    tracciamento += "_Ho letto una riga del file origine"
                    If iRiga > 1 Then
                        tracciamento += "_Scrivo sul file destinazione"
                        fileDestinazione.WriteLine(fileOrigine.ReadLine())
                        tracciamento += "_Ho scritto sul file destinazione"
                    End If
                    iRiga += 1
                Loop
                tracciamento += "_Ripeto"
            End Using
            tracciamento += "_Chiudo il file origine"
            fileOrigine.Close()
            tracciamento += "_Ho chiuso il file origine_Cancello il file origine"
            File.Delete(nomeFile)
            tracciamento += "_Ho cancellato il file origine_Creo il file di tracciamento"
        Catch ex As Exception
            tracciamento += "_" & ex.Message
        End Try
        Dim fileTracciamento As StreamWriter = New StreamWriter(localFolder & "\TracciamentoLog.txt")
        fileTracciamento.WriteLine(tracciamento)
        fileTracciamento.Close()
    End Sub

    Function ShiftCenterChange()
        ShiftCenterChange = ""
        pulsantePremuto = ""
        ScriviMessaggio(Messaggio(48), lblMessaggio)
        cmdShiftOk.Visible = True
        cmdShiftKo.Visible = True
        Do While Not pulsantePremuto.Length > 0
            Application.DoEvents()
        Loop
        cmdShiftOk.Visible = False
        cmdShiftKo.Visible = False

        Select Case pulsantePremuto
            Case "OK"
                ShiftCenterChange = pulsantePremuto
            Case "KO"
                lblShiftCenter.Visible = True
                lblShiftCenter.Text = testValuesIni.ReadValue("Varie", "shiftCenterValue")
                trkShift.Value = lblShiftCenter.Text
                trkShift.Visible = True
                cmdDx.Visible = True
                cmdSx.Visible = True
                ScriviMessaggio(Messaggio(49), lblMessaggio)
                AttendiTasto()
                trkShift.Visible = False
                cmdDx.Visible = False
                cmdSx.Visible = False
                lblShiftCenter.Visible = False
                CmdTransparentIn()
                Dim valore As String = "3" & lblShiftCenter.Text.Substring(0, 1) & "3" & lblShiftCenter.Text.Substring(1, 1)
                Trasporto.ShiftCenterValue = lblShiftCenter.Text
                CmdTransparentCommand("920D3030" & valore)
                CmdTransparentOut()
                ShiftCenterChange = "SHIFTCHANGED"
        End Select
    End Function

    Function SfogliaBN() As String
inizio:
        SfogliaBN = ""

        Interfaccia("attesa")
        ScriviMessaggio(Messaggio(47), lblMessaggio)
        AttendiTasto()
        Interfaccia("Normale")
        ScriviMessaggio(Messaggio(95), lblMessaggio)
        cmdOpen()
        SfogliaBN = cmdCounting()
        Select Case SfogliaBN
            Case "OK"

            Case "NOTE ON FRONT OUTPUT"
                Interfaccia("attesa")
                ScriviMessaggio(Messaggio(246), lblMessaggio)
                AttendiTasto()
                GoTo inizio
            Case "NOTE ON LEFT OUTPUT"
                Interfaccia("Normale")
                ScriviMessaggio(Messaggio(245), lblMessaggio)
                AttendiTasto()
                GoTo inizio
            Case Else
                Interfaccia("errore")
                ScriviMessaggio(Messaggio(229), lblMessaggio)
                Select Case AttendiTasto()
                    Case "ESC"
                        GoTo fine
                    Case Else
                        GoTo inizio
                End Select
        End Select

        Trasporto.Deposito.Totale = myDepositReply.NotesToSafe
        Trasporto.Deposito.Unfit = myDepositReply.NotesToOut

        If Trasporto.Deposito.Unfit >= 15 And Trasporto.Deposito.Totale >= 100 Then
            ScriviMessaggio(String.Format(Messaggio(94), Trasporto.Deposito.Totale, Trasporto.Deposito.Unfit), lblMessaggio) 'Banconote depositate correttamente, riconosciute {0} e Unfit {1}.
            Aspetta(1000)
            SfogliaBN = "OK"
        Else
            Interfaccia("errore")
            SfogliaBN = "DEPOSITKO"
            ScriviMessaggio(String.Format(Messaggio(230), Trasporto.Deposito.Totale, Trasporto.Deposito.Unfit), lblMessaggio) 'Banconote depositate non sufficienti, riconosciute {0} invece di 100 e Unfit {1} invece di 15. Premere un tasto per continuare, ESC per uscire.
            Select Case AttendiTasto()
                Case "ESC"
                    GoTo fine
                Case Else
                    SfogliaBN = ""
                    GoTo inizio
            End Select
        End If
fine:
        Interfaccia("Normale")
    End Function

    Function SetShiftCenterPcloseDelay() As String
        Dim shiftCenter, pcClose As String
        Dim contShift, contClose As Integer
        shiftCenter = testValuesIni.ReadValue("varie", "shiftCenterValue")
        pcClose = testValuesIni.ReadValue("varie", "PcCloseDelay")
        shiftCenter = Dec2Char(shiftCenter)
        pcClose = Dec2Char(pcClose)
        contShift = 1
        contClose = 1

        SetShiftCenterPcloseDelay = ""
        ScriviMessaggio(Messaggio(14), lblMessaggio)
        CmdTransparentIn()
        Aspetta(100)
        ScriviMessaggio(Messaggio(45), lblMessaggio)
SetShiftCenter:
        CmdTransparentCommand("920D" & shiftCenter, 8500)
        CmdTransparentCommand("E22C", 1000)

        If TransparentAnswer <> shiftCenter Then
            ScriviMessaggio(String.Format(Messaggio(99), AsciCode2AsciChar(shiftCenter), AsciCode2AsciChar(TransparentAnswer)), lblMessaggio)
            If contShift <= 3 Then
                contShift += 1
                Aspetta(1000)
                GoTo SetShiftCenter
            Else
                SetShiftCenterPcloseDelay = "SHIFTCENTERKO"
                GoTo fine
            End If
        End If

        ScriviMessaggio(Messaggio(46), lblMessaggio)
        Aspetta(100)

SetPcClose:
        CmdTransparentCommand("9211" & pcClose, 8500)
        CmdTransparentCommand("E22F", 1000)
        If TransparentAnswer <> pcClose Then
            ScriviMessaggio(String.Format(Messaggio(100), AsciCode2AsciChar(shiftCenter), AsciCode2AsciChar(TransparentAnswer)), lblMessaggio)
            If contClose <= 3 Then
                contClose += 1
                Aspetta(1000)
                GoTo SetPcClose
            Else
                SetShiftCenterPcloseDelay = "PCCLOSEKO"
                GoTo fine
            End If
        End If
        ScriviMessaggio(Messaggio(20), lblMessaggio)
        Aspetta(100)
        SetShiftCenterPcloseDelay = "OK"

fine:
        CmdTransparentOut()
        Aspetta(100)

    End Function

    Function TestVelocitàMSO() As String
inizio:
        xmlLog.AddTest("testSpeedMSO", Log.Result, "KO")
        TestVelocitàMSO = ""
        ScriviMessaggio(Messaggio(41), lblMessaggio) 'Test della velocità del motore MSO in corso...
        Dim velocità As String = ""
        CmdTransparentIn()
        Aspetta(500)

        'tcPusherRejectOn() 'modifica consigliata da Martinallo 05/08/2020
        tcSetDepositMode()
        'tcAlignHome() 'modifica consigliata da Martinallo 05/08/2020
        'tcPressorClose() 'modifica consigliata da Martinallo 05/08/2020
        'tcFeedStart() 'modifica consigliata da Martinallo 05/08/2020

        Aspetta(1000)
        CmdTransparentCommand("83", 8000)
        Aspetta(100)
        CmdTransparentCommand("D31A01", 21000)
        velocità = LeggiSecondeCifre(TransparentAnswer)

        tabella.Visible = True

        With Trasporto.VelTrasp
            .Massima = Hex2Dec(Mid(velocità, 1, 4))
            .Media = Hex2Dec(Mid(velocità, 5, 4))
            .Minima = Hex2Dec(Mid(velocità, 9, 4))

            .Limiti.Minima = testValuesIni.ReadValue("Velocità", "Minima")
            .Limiti.MediaMinima = testValuesIni.ReadValue("Velocità", "MediaMinima")
            .Limiti.MediaMassima = testValuesIni.ReadValue("Velocità", "MediaMassima")
            .Limiti.Massima = testValuesIni.ReadValue("Velocità", "Massima")

            tabella.Rows.Clear()
            tabella.ColumnCount = 3
            tabella.Columns(0).Name = "TEST"
            tabella.Columns(1).Name = "VALUE"
            tabella.Columns(2).Name = "RANGE"

            tabella.Rows.Add(Messaggio(42), .Minima, "> " & .Limiti.Minima) 'Velocità MINIMA
            If .Minima < .Limiti.Minima Then
                tabella.Item(1, 0).Style.BackColor = ArcaColor.Arancio
                TestVelocitàMSO = "VelKO"
            Else
                tabella.Item(1, 0).Style.BackColor = Color.White
            End If

            tabella.Rows.Add(Messaggio(43), .Media, .Limiti.MediaMinima & " < X < " & .Limiti.MediaMassima) 'Velocità MEDIA
            If .Media < .Limiti.MediaMinima Or .Media > .Limiti.MediaMassima Then
                tabella.Item(1, 1).Style.BackColor = ArcaColor.Arancio
                TestVelocitàMSO = "VelKO"
            Else
                tabella.Item(1, 1).Style.BackColor = Color.White
            End If

            tabella.Rows.Add(Messaggio(44), .Massima, "< " & .Limiti.Massima) 'Velocità MASSIMA
            If .Massima > .Limiti.Massima Then
                tabella.Item(1, 2).Style.BackColor = ArcaColor.Arancio
                TestVelocitàMSO = "VelKO"
            Else
                tabella.Item(1, 2).Style.BackColor = Color.White
            End If
        End With

        'tcFeedStop() 'modifica consigliata da Martinallo 05/08/2020
        'tcPusherHome() 'modifica consigliata da Martinallo 05/08/2020
        tcSetStopMode()
        'tcPusherRejectOff() 'modifica consigliata da Martinallo 05/08/2020
        tabella.AutoSize = True

        If TestVelocitàMSO = "VelKO" Then
            Interfaccia("errore")
            ScriviMessaggio(Messaggio(228), lblMessaggio)
            Select Case AttendiTasto()
                Case "ESC"
                Case Else
                    GoTo inizio
            End Select
        Else
            TestVelocitàMSO = "OK"
        End If

        Aspetta(1000)
        CmdTransparentOut()
        tabella.Visible = False
        xmlLog.AddTest("testSpeedMSO", Log.Result, TestVelocitàMSO, "SpeedMin", Trasporto.VelTrasp.Minima, "SpeedAvg", Trasporto.VelTrasp.Media, "SpeedMax", Trasporto.VelTrasp.Massima)
    End Function

    Function ImpostaCassetteNumber(Optional ByVal muletto As Boolean = False) As String
inizio:
        xmlLog.AddTest("setCassetteNumber", Log.Result, "KO")
        ImpostaCassetteNumber = "OK"
        Interfaccia("Normale")
        ScriviMessaggio(Messaggio(40), lblMessaggio)
        Dim nCassetti As Byte = 0
        If muletto = False Then
            Select Case Trasporto.Categoria
                Case "CM18B" ' , "CM18SOLO"
                    nCassetti = 6
                Case "CM18", "CM20S", "CM18EVO", "CM18SOLOT", "CM18SOLO", "CM18HC" '21/02/17 davide
                    nCassetti = 8
                Case "CM20"
                    nCassetti = 10
                Case "CM18T", "CM20T", "CM18EVOT"
                    nCassetti = 12
                Case Else
                    nCassetti = 6
            End Select
        Else
            nCassetti = 6
        End If

        'davide 21/02/17
        If Trasporto.Categoria = "CM18SOLO" Or Trasporto.Categoria = "CM18SOLOT" Then
            tipoConnessione = "USB"
            CaricaImpostazioneConnessione()
            'ApriConnessione()
            'Aspetta(500)
        End If

        ApriConnessione()
        If Trasporto.Categoria <> "CM18HC" Then
            ImpostaCassetteNumber = cmdComandoSingolo("F,1,11," & Trim(Str(nCassetti)), 3, 5000)
            If ImpostaCassetteNumber <> "OK" Then
                Interfaccia("errore")
                ScriviMessaggio(String.Format(Messaggio(225), ImpostaCassetteNumber), lblMessaggio)
                Select Case AttendiTasto()
                    Case "ESC"
                        Exit Function
                    Case Else
                        GoTo inizio
                End Select
            End If
        End If

        If Trasporto.CD80 > 0 Then
            'ImpostaCassetteNumber = cmdComandoSingolo("F,1,30," & Trasporto.CD80, 4, 5000)

            ImpostaCassetteNumber = cmdComandoSingolo("F,1,30,0", 3, 5000)
            xmlLog.AddTest("setTo0cd80", Log.Result, ImpostaCassetteNumber)
            If ImpostaCassetteNumber <> "OK" Then
                Interfaccia("errore")
                ScriviMessaggio(String.Format(Messaggio(226), ImpostaCassetteNumber), lblMessaggio)
                Select Case AttendiTasto()
                    Case "ESC"
                        Exit Function
                    Case Else
                        GoTo inizio
                End Select
            End If
        End If
        xmlLog.AddTest("setCassetteNumber", Log.Result, "OK", "cassetteNumber", nCassetti)
    End Function

    Function ImpostaTipoMacchina() As String
inizio:
        Interfaccia("Normale")
        xmlLog.AddTest("setMachineType", Log.Result, "KO")
        ImpostaTipoMacchina = cmdComandoSingolo("T,1,0,77", 4, 5000)
        If comandoSingoloRisposta.Length = 1 Then
            Interfaccia("errore")
            ScriviMessaggio(String.Format(Messaggio(224), "NO ANSWER"), lblMessaggio) 'Tipo macchina non impostato correttamente. {0}. Premere un tasto per ripetere, ESC per uscire.
            Select Case AttendiTasto()
                Case "ESC"
                    Exit Function
                Case Else
                    GoTo inizio
            End Select
        End If
        Dim tipoMacchinaLetto As String = comandoSingoloRisposta(5)
        Dim tipoMacchina As String = ""
        Select Case Trasporto.Categoria
            Case "CM18", "CM18EVO", "CM18SOLO"
                tipoMacchina = "CM18"
            Case "CM20S"
                tipoMacchina = "CM20S"
            Case "CM18T", "CM18EVOT", "CM18SOLOT", "CM18HC"  'davide 21/02/17
                tipoMacchina = "CM18T"
            Case "CM20T" ', "CM18SOLOT"
                tipoMacchina = "CM20T"
            Case "CM20"
                tipoMacchina = "CM20"
            Case "CM18B", "OM61"
                tipoMacchina = "CM18B"
            Case Else
                Interfaccia("errore")
                ScriviMessaggio(String.Format(Messaggio(223), Trasporto.Categoria), lblMessaggio)
                AttendiTasto()
                ImpostaTipoMacchina = "TIPOMACCHINAKO"
                Exit Function
        End Select
        If tipoMacchinaLetto <> tipoMacchina Then
            'davide 21/02/17
            If Trasporto.Categoria = "CM18SOLO" Or Trasporto.Categoria = "CM18SOLOT" Or Trasporto.Categoria = "OM61" Then
                tipoConnessione = "USB"
                CaricaImpostazioneConnessione()
                ApriConnessione()
                Aspetta(500)
            End If

            ImpostaTipoMacchina = cmdComandoSingolo("F,1,10," & tipoMacchina, 3, 5000)
        End If

        If ImpostaTipoMacchina <> "OK" Then
            Interfaccia("errore")
            ScriviMessaggio(String.Format(Messaggio(224), ImpostaTipoMacchina), lblMessaggio)
            Select Case AttendiTasto()
                Case "ESC"
                    Exit Function
                Case Else
                    GoTo inizio
            End Select
        End If
        xmlLog.AddTest("setMachineType", Log.Result, "OK", "machineType", tipoMacchina)
    End Function

    ''' <summary>
    ''' Eliminazione dei file presenti nella cartella del download
    ''' </summary>
    Private Sub SvuotaCartellaDownload()
        ScriviMessaggio(Messaggio(35), lblMessaggio) 'Svuotamento cartella di appoggio locale in corso...
        Me.Refresh()

        Try
            Directory.Delete(localDLFolder, True)
            Threading.Thread.Sleep(100)
        Catch ex As DirectoryNotFoundException
        Finally
            Directory.CreateDirectory(localDLFolder)
        End Try
    End Sub

    Function DownloadCustomization() As String
        DownloadCustomization = "OK"
        Dim cartella As String = ""
        Dim firmware As String = ""
        Dim numeroFile As Integer = 0
        SvuotaCartellaDownload()
        objConn.Open()
        sqlStringa = "Select Cod_fw FROM tblMAN WHERE id_modulo = 'IMMAGINI+VIDEO' AND id_prodotto = '" & cboIdProdotto.Text & "' AND id_cliente = " & Trasporto.ClienteID
        sqlCommand = New OleDbCommand(sqlStringa, objConn)
        dbReader = sqlCommand.ExecuteReader
        Do While Not dbReader.Read() = Nothing
            firmware = dbReader(0)
        Loop
        objConn.Close()
        numeroFileAttuale = 1
        numeroFile = 100
        Trasporto.CustomizationCodice = firmware
        lblMacchina.Text = Trasporto.MacchinaCodice & " - " & Trasporto.Categoria & "(" & Trasporto.ClienteNome & ") CUSTOMIZATION: " & Trasporto.CustomizationCodice

        ScriviMessaggio(Messaggio(36), lblMessaggio) 'Copia in locale dei file per il download della customization in corso...
        For Each file As String In My.Computer.FileSystem.GetFiles(localFWFolder & firmware, FileIO.SearchOption.SearchTopLevelOnly, "*.zip")
            Dim p As New Process
            p.StartInfo.UseShellExecute = True
            p.StartInfo.FileName = path7Zip & "\7z.exe"
            p.StartInfo.Arguments = " x " & """" & file & """" & " -o" & """" & localDLFolder & """" & " -aoa"
            p.StartInfo.RedirectStandardOutput = False
            p.Start()
            p.WaitForExit()
        Next

        CmdTransparentOut()


        'disinstallazione file video ed immagini
        DownloadCustomization = cmdComandoSingolo("L,1,6,U,9", 2)
        If DownloadCustomization <> "OK" Then

        End If
        DownloadCustomization = cmdComandoSingolo("L,1,6,U,10", 2)
        If DownloadCustomization <> "OK" Then

        End If
        'ChiudiConnessione()
        'customizationConnType = "USB"
        'If ImpostaLAN(8101) = False Then
        '    customizationConnType = "USB"
        '    tipoConnessione = "USB"
        'End If


        'CaricaImpostazioneConnessione(tipoConnessione)
        'ApriConnessione()


        DownloadCustomization = cmdComandoSingolo("T,1,6,29,2", 4)
        Select Case DownloadCustomization
            Case "RECORD NOT PRESENT", "219"
                customizationConnType = "USB"
            Case "OK", "101", "1"
                lanDevice = comandoSingoloRisposta(5)
                customizationConnType = "LAN"
        End Select
        'If DownloadCustomization = "OK" Or DownloadCustomization = "101" Or DownloadCustomization = "1" Then
        '    lanDevice = cmdComandoSingolo(5)
        '    customizationConnType = "LAN"
        'End If

        Aspetta(500)
        ChiudiConnessione()
        If customizationConnType = "USB" Then
usbStd:
            CaricaImpostazioneConnessione("USB")
            ApriConnessione()
            'passaggio a protocollo semplificato
            DownloadCustomization = cmdComandoSingolo("u,1,6,1", 4)
            xmlLog.AddTest("switchToEasy", Log.Result, DownloadCustomization)
            If DownloadCustomization <> "OK" Then Exit Function
            DownloadCustomization = cmdComandoSingolo("T,1,0,7", 4)
            If DownloadCustomization = "" Then
                ChiudiConnessione()
                CaricaImpostazioneConnessione("USBEASY")
                If ApriConnessione() = True Then DownloadCustomization = "OK"
            End If
            DownloadCustomization = cmdReset()

            If DownloadCustomization <> "OK" Then Exit Function
usbSim:
            ChiudiConnessione()
            CaricaImpostazioneConnessione("USBEASY")
            ApriConnessione()
        End If
        ScriviMessaggio(Messaggio(37), lblMessaggio) 'Download Customization in corso...
        pgbDownload.Visible = True
        numeroFile = My.Computer.FileSystem.GetFiles(localDLFolder).Count
        Dim fileReader As StreamReader
        For Each customizationFile As String In My.Computer.FileSystem.GetFiles(localDLFolder, FileIO.SearchOption.SearchTopLevelOnly, "*.cst")
            fileReader = New StreamReader(customizationFile)
            Dim line As String
            Do
                line = fileReader.ReadLine()
                If Mid(line, 1, 1) = "9" Then
                    Aspetta(150)
                    If customizationConnType = "USB" Then
                        DownloadCustomization = CopiaFileSuSDUSB(Mid(line, 3, Len(line) - 2), 9, numeroFile)
                    Else
                        DownloadCustomization = CopiaFileSuSD(Mid(line, 3, Len(line) - 2), 9, numeroFile)
                    End If
                    If DownloadCustomization <> "OK" Then Exit Do
                End If
                If Mid(line, 1, 2) = "10" Then
                    Aspetta(150)
                    If customizationConnType = "USB" Then
                        DownloadCustomization = CopiaFileSuSDUSB(Mid(line, 4, Len(line) - 3), 10, numeroFile)
                    Else
                        DownloadCustomization = CopiaFileSuSD(Mid(line, 4, Len(line) - 3), 10, numeroFile)
                    End If
                    If DownloadCustomization <> "OK" Then Exit Do
                End If

            Loop Until line Is Nothing
            fileReader.Close()
            xmlLog.AddTest("customDownload", Log.Result, DownloadCustomization)
            If DownloadCustomization <> "OK" Then Exit Function
        Next

        pgbDownload.Visible = False
        If Trasporto.Categoria = "CM18SOLO" Or Trasporto.Categoria = "CM18SOLOT" Or Trasporto.Categoria = "OM61" Then
            tipoConnessione = "USB"
        Else
            tipoConnessione = configIni.ReadValue("Connection", "Connection")
        End If
        If customizationConnType = "USB" Then
            'passaggio a protocollo standard
            DownloadCustomization = cmdComandoSingolo("u,1,6,2", 4)
            xmlLog.AddTest("switchToStd", Log.Result, DownloadCustomization)
            If DownloadCustomization <> "OK" Then Exit Function
            Aspetta(1000)
            DownloadCustomization = cmdComandoSingolo("T,1,0,7", 4)
            If DownloadCustomization = "" Then
                ChiudiConnessione()
                CaricaImpostazioneConnessione("USB")
                If ApriConnessione() = True Then DownloadCustomization = "OK"
            End If
            DownloadCustomization = cmdReset()
            If DownloadCustomization <> "OK" Then

            End If
            ChiudiConnessione()
        End If
        CaricaImpostazioneConnessione(tipoConnessione)
        ApriConnessione()
        Aspetta(500)
        lblMacchina.Text = Trasporto.MacchinaCodice & " - " & Trasporto.Categoria & "(" & Trasporto.ClienteNome & ")"
    End Function

    Function ImpostaLAN(ByVal port As String)

        tipoConnessione = "LAN"
        With ConnectionParam
            .ConnectionMode = DLINKMODETCPIP
            .TcpIpPar.clientIpAddr = lanDevice
            .TcpIpPar.portNumber = port
        End With
        Return ApriConnessione()

    End Function

    ''' <summary>
    ''' Copia i file dalla cartella di download nella SD della macchina.
    ''' </summary>
    ''' <param name="nomeFile">Nome del file da copiare</param>
    ''' <param name="tipoFile">Tipo del file da copiare: 8=Init Suite Fw, 9=Check Film version, 10=Check Img version</param>
    ''' <param name="numeroFile">Numero dei file da copiare</param>
    ''' <returns></returns>
    Function CopiaFileSuSD(ByVal nomeFile As String, ByVal tipoFile As String, Optional ByVal numeroFile As Integer = 0) As String
        Dim dimensioneFile As Double
        Dim iFreeFile As Integer = FreeFile()
        Dim daInviare As String = ""
        ImpostaLAN(8101)
        pgbDownload.Minimum = 1
        pgbDownload.Maximum = numeroFile
        pgbDownload.Value = numeroFileAttuale
        Aspetta(150)
        CopiaFileSuSD = cmdComandoSingolo("L,1,6,I," & tipoFile & ",,,," & nomeFile, 2, 5000)

        Select Case CopiaFileSuSD
            Case "OK", "WRONG FIRMWARE"

            Case Else
                Exit Function
        End Select
        For Each customizationFile As String In My.Computer.FileSystem.GetFiles(localDLFolder, FileIO.SearchOption.SearchTopLevelOnly, nomeFile)
            Dim fileReader As StreamReader = New StreamReader(localDLFolder & nomeFile)
            Dim line As String
            Do
                line = fileReader.ReadLine()
                If Len(line) > 0 Then
                    iFreeFile = FreeFile()
                    dimensioneFile = FileLen(localDLFolder & line)
                    ScriviMessaggio(String.Format(Messaggio(39), line), lblMessaggio)
                    ImpostaLAN(8101)
preparaFile:
                    stato = "OK"
                    CopiaFileSuSD = cmdComandoSingolo("L,1,6,W," & tipoFile & "," & line & "," & dimensioneFile & ",0", 2, 5000)
                    Select Case CopiaFileSuSD
                        Case "OK"
                        Case Else
                            stato = "ERRORE"
                            ScriviMessaggio(String.Format(Messaggio(222), line), lblMessaggio) 'Copia del file {0} non riuscita, premere un tasto per riprovare, ESC per uscire.
                            Select Case AttendiTasto()
                                Case "ESC"
                                    CopiaFileSuSD = "SDNOTREADY"
                                    Exit Do
                                Case Else
                                    GoTo preparaFile
                            End Select
                    End Select
                    If CopiaFileSuSD <> "OK" Then
                        Exit Do
                    End If
                    ImpostaLAN(8100)
                    FileOpen(iFreeFile, localDLFolder & line, OpenMode.Binary)
                    daInviare = Space(dimensioneFile)
                    FileGet(iFreeFile, daInviare)
copiaFile:
                    stato = "OK"
                    CopiaFileSuSD = cmdComandoSingolo(daInviare, 0, 5000)
                    Select Case CopiaFileSuSD
                        Case "OK"
                        Case Else
                            stato = "ERRORE"
                            ScriviMessaggio(String.Format(Messaggio(222), line), lblMessaggio)
                            Select Case AttendiTasto()
                                Case "ESC"
                                    CopiaFileSuSD = "SDERROR"
                                    Exit Do
                                Case Else
                                    GoTo copiaFile
                            End Select
                    End Select
                    FileClose(iFreeFile)
                End If
                numeroFileAttuale += 1
                If numeroFileAttuale > pgbDownload.Maximum Then numeroFileAttuale = pgbDownload.Maximum
                pgbDownload.Value = numeroFileAttuale

            Loop Until line Is Nothing
            fileReader.Close()
            If CopiaFileSuSD <> "OK" Then
                Exit For
            End If
        Next
        iFreeFile = FreeFile()
        ImpostaLAN(8101)
        dimensioneFile = FileLen(localDLFolder & nomeFile)
        CopiaFileSuSD = cmdComandoSingolo("L,1,6,W," & tipoFile & "," & nomeFile & "," & dimensioneFile & ",0", 2, 5000)
        ImpostaLAN(8100)
        FileOpen(iFreeFile, localDLFolder & nomeFile, OpenMode.Binary)
        daInviare = Space(dimensioneFile)
        FileGet(iFreeFile, daInviare)
        stato = "OK"
        CopiaFileSuSD = cmdComandoSingolo(daInviare, 0, 5000)
        FileClose(iFreeFile)
    End Function

    Function CopiaFileSuSDUSB(ByVal nomeFile As String, ByVal tipoFile As String, Optional ByVal numeroFile As Integer = 0) As String
        Dim dimensioneFile As Double
        Dim iFreeFile As Integer = FreeFile()
        Dim daInviare As String = ""


        pgbDownload.Minimum = 1
        pgbDownload.Maximum = numeroFile
        pgbDownload.Value = numeroFileAttuale
        Aspetta(150)
        CopiaFileSuSDUSB = cmdComandoSingolo("L,1,6,I," & tipoFile & ",,,," & nomeFile, 2, 5000)

        Select Case CopiaFileSuSDUSB
            Case "OK", "WRONG FIRMWARE"

            Case Else
                Exit Function
        End Select
        For Each customizationFile As String In My.Computer.FileSystem.GetFiles(localDLFolder, FileIO.SearchOption.SearchTopLevelOnly, nomeFile)
            Dim fileReader As StreamReader = New StreamReader(localDLFolder & nomeFile)
            Dim line As String
            Do
                line = fileReader.ReadLine()
                If Len(line) > 0 Then
                    iFreeFile = FreeFile()
                    dimensioneFile = FileLen(localDLFolder & line)
                    ScriviMessaggio(String.Format(Messaggio(39), line), lblMessaggio)
preparaFile:
                    stato = "OK"
                    CopiaFileSuSDUSB = cmdComandoSingolo("L,1,6,W," & tipoFile & "," & line & "," & dimensioneFile & ",16384", 2, 5000)
                    Select Case CopiaFileSuSDUSB
                        Case "OK"
                        Case Else
                            stato = "ERRORE"
                            ScriviMessaggio(String.Format(Messaggio(222), line), lblMessaggio) 'Copia del file {0} non riuscita, premere un tasto per riprovare, ESC per uscire.
                            Select Case AttendiTasto()
                                Case "ESC"
                                    CopiaFileSuSDUSB = "SDNOTREADY"
                                    Exit Do
                                Case Else
                                    GoTo preparaFile
                            End Select
                    End Select
                    If CopiaFileSuSDUSB <> "OK" Then
                        Exit Do
                    End If
                    FileOpen(iFreeFile, localDLFolder & line, OpenMode.Binary)
                    Dim position As Integer = 1
                    Dim blocco As Integer = 0
                    Do While position < dimensioneFile
                        If position + 16384 > dimensioneFile Then
                            blocco = dimensioneFile - position + 1
                        Else
                            blocco = 16384
                        End If
                        daInviare = Space(dimensioneFile)
                        FileGet(iFreeFile, daInviare, position)
inviaFile:
                        CopiaFileSuSDUSB = cmdComandoSingolo("L,1,6,W,7,,," & Mid(daInviare, 1, blocco), 2)
                        position += 16384
                    Loop

                    Select Case CopiaFileSuSDUSB
                        Case "OK"
                        Case Else
                            stato = "ERRORE"
                            ScriviMessaggio(String.Format(Messaggio(222), line), lblMessaggio)
                            Select Case AttendiTasto()
                                Case "ESC"
                                    CopiaFileSuSDUSB = "SDERROR"
                                    Exit Do
                                Case Else
                                    GoTo preparaFile
                            End Select
                    End Select
                    FileClose(iFreeFile)
                End If
                numeroFileAttuale += 1
                If numeroFileAttuale > pgbDownload.Maximum Then numeroFileAttuale = pgbDownload.Maximum
                pgbDownload.Value = numeroFileAttuale

            Loop Until line Is Nothing
            fileReader.Close()
            If CopiaFileSuSDUSB <> "OK" Then
                Exit For
            End If
        Next
        iFreeFile = FreeFile()
        dimensioneFile = FileLen(localDLFolder & nomeFile)
        CopiaFileSuSDUSB = cmdComandoSingolo("L,1,6,W," & tipoFile & "," & nomeFile & "," & dimensioneFile & ",16384", 2, 5000)
        FileOpen(iFreeFile, localDLFolder & nomeFile, OpenMode.Binary)
        Dim position2 As Integer = 1
        Dim blocco2 As Integer = 0
        Do While position2 < dimensioneFile
            If position2 + 16384 > dimensioneFile Then
                blocco2 = dimensioneFile - position2 + 1
            Else
                blocco2 = 16384
            End If
            daInviare = Space(dimensioneFile)
            FileGet(iFreeFile, daInviare, position2)
            CopiaFileSuSDUSB = cmdComandoSingolo("L,1,6,W,7,,," & Mid(daInviare, 1, blocco2), 2)
            position2 += 16384
        Loop


        stato = "OK"
        FileClose(iFreeFile)
    End Function

    Function CaricaChiavettaUSB() As String
        CaricaChiavettaUSB = ""
        Dim driveID As Integer = 0
start:
        Dim volume As String = configIni.ReadValue("UsbKey", "volume")
        Dim drive As String = ""
        Interfaccia("attesa")
        ScriviMessaggio(String.Format(Messaggio(31), volume), lblMessaggio)
        AttendiTasto()
        Interfaccia("Normale")
        Try
            For i = 0 To My.Computer.FileSystem.Drives.Count - 1
                driveID = i
                If My.Computer.FileSystem.Drives(driveID).DriveType = DriveType.Removable Then
                    drive = My.Computer.FileSystem.Drives(driveID).Name
                    If UCase(My.Computer.FileSystem.Drives(driveID).VolumeLabel) = UCase(volume) Then
                        Exit For
                    Else
                        drive = ""
                    End If
                Else
                    drive = ""
                End If
            Next
driveSelect:
            If drive = "" Then
                Interfaccia("attesa")
                ScriviMessaggio(Messaggio(89), lblMessaggio)
                Dim folderBrower As FolderBrowserDialog = New FolderBrowserDialog
                folderBrower.RootFolder = Environment.SpecialFolder.MyComputer
                folderBrower.SelectedPath = "C:\"
                folderBrower.Description = Messaggio(32)

                If folderBrower.ShowDialog = DialogResult.OK Then
                    drive = folderBrower.SelectedPath
                End If
            End If

            If drive = "" Then
                Interfaccia("errore")
                ScriviMessaggio(Messaggio(219), lblMessaggio)
                CaricaChiavettaUSB = "DRIVEKO"
                GoTo fine
            End If

            CaricaChiavettaUSB = CancellaDrive(drive)
            Select Case CaricaChiavettaUSB
                Case "KO"
                    GoTo fine
            End Select

            CaricaChiavettaUSB = CaricaChiavettaFW(drive)
            Select Case CaricaChiavettaUSB
                Case "KO"
                    GoTo fine
            End Select

installazione:
            Interfaccia("attesa")
            ScriviMessaggio(Messaggio(88), lblMessaggio) 'Estrarre la chiavetta usb dal pc ed inserirla nel trasporto in test, eseguire il download dei FW RealTime(MB.. o MC..), FPGA(FPGA..) e OSC(Boot..). Durante il download posizionare i tappi su tutte le seriali presenti. Terminato il download premere INVIO
            AttendiTasto("INVIO")
            ScriviMessaggio(Messaggio(91), lblMessaggio) 'Per confermare la corretta installazione del firmware, premere 'L'.
            xmlLog.AddTest("fwInstall", Log.Result, "KO")
            Select Case AttendiTasto("L")
                Case "OK"
                    xmlLog.AddTestAttribute("fwInstall", Log.Result, "OK", "fwSuite", Trasporto.CodiceFWSuite)
                    Interfaccia("Normale")
                    If Trasporto.Categoria = "CM18SOLO" Or Trasporto.Categoria = "CM18SOLOT" Or Trasporto.Categoria = "OM61" Then
                        ScriviMessaggio(Messaggio(93), lblMessaggio) 'La categoria è CM18SOLO, CM18SOLOT o OM61, collegare il cavo USB alla porta frontale. Premere un tasto per continuare.
                        AttendiTasto()
                        tipoConnessione = "USB"
                        CaricaImpostazioneConnessione()
                        ApriConnessione()
                    End If
                Case Else
                    GoTo installazione
            End Select

        Catch ex As Exception
            CaricaChiavettaUSB = "KO"
        End Try
fine:
        Interfaccia("Normale")
        lblMacchina.Text = Trasporto.MacchinaCodice & " - " & Trasporto.Categoria & "(" & Trasporto.ClienteNome & ")"
    End Function

    Function CaricaChiavettaFW(ByVal cartella As String) As String
        CaricaChiavettaFW = ""
        objConn.Open()
        sqlStringa = "SELECT tblman.cod_fw FROM tblman where tblman.id_prodotto Like '" & cboIdProdotto.Text & "' and tblman.id_cliente like '" & Trasporto.ClienteID & "' and tblman.id_modulo='fw_prodotto'"
        sqlCommand = New OleDbCommand(sqlStringa, objConn)
        dbReader = sqlCommand.ExecuteReader
        dbReader.Read()
        Trasporto.CodiceFWSuite = dbReader(0)
        objConn.Close()
        lblMacchina.Text = Trasporto.MacchinaCodice & " - " & Trasporto.Categoria & "(" & Trasporto.ClienteNome & ") SUITE: " & Trasporto.CodiceFWSuite
copiafile:
        ScriviMessaggio(Messaggio(34), lblMessaggio) 'ricerca dei file da copiare in corso
        Dim nomeFile = Dir(localFWFolder & Trasporto.CodiceFWSuite & "\*.zip")
        If nomeFile = "" Then
            Interfaccia("errore")
            ScriviMessaggio(Messaggio(219), lblMessaggio) 'Non è stato impostato correttamente il drive di destinazione. Premere un tasto per ripetere, ESC per uscire.
            Select Case AttendiTasto()
                Case "ESC"
                    CaricaChiavettaFW = "FOLDERKO"
                    Exit Function
                Case Else
                    GoTo copiafile
            End Select
        End If

        ScriviMessaggio(Messaggio(34), lblMessaggio) 'Copia dei file della suite in corso
        unzip(localFWFolder & Trasporto.CodiceFWSuite & "\" & nomeFile, cartella & Trasporto.CodiceFWSuite & "\")
        If Dir(cartella & Trasporto.CodiceFWSuite & "\").Length > 0 Then
            Dim folder As DirectoryInfo = New DirectoryInfo(cartella & Trasporto.CodiceFWSuite & "\")
            For Each fileFound As IO.FileInfo In folder.GetFiles
                If Not fileFound.Name.StartsWith("FPGA") And Not fileFound.Name.StartsWith("BOOT") And Not fileFound.Name.StartsWith("FF") _
                        And Not fileFound.Name.StartsWith("Wce") And Not fileFound.Name.StartsWith("xDLL") And Not fileFound.Name.StartsWith("Init") _
                        And Not fileFound.Name.StartsWith("MC") And Not fileFound.Name.StartsWith("MB") And Not fileFound.Name.StartsWith("MBAG") _
                        And Not fileFound.Name.StartsWith("CMUpgrade") Then
                    fileFound.Delete()
                End If
            Next
            CaricaChiavettaFW = "OK"
        End If

    End Function

    Public Sub unzip(ByVal nomefile As String, ByVal nomedir As String)

        Dim p As New Process
        p.StartInfo.UseShellExecute = False
        p.StartInfo.FileName = path7Zip & "7z.exe"
        p.StartInfo.Arguments = " x " & """" & nomefile & """" & " -o" & """" & nomedir & """" & " -aoa"
        p.StartInfo.RedirectStandardOutput = False
        p.Start()
        p.WaitForExit()
        'str = p.StandardOutput.ReadToEnd()
        'If InStr(str, "Ok") = 0 Then
        '    Using logfile As StreamWriter = File.AppendText(Application.StartupPath & "\errori.txt")
        '        logfile.WriteLine(Format(Now, "dd-MM-yy HH:mm") & " - " & str)
        '        logfile.Close()
        '    End Using
        'End If

    End Sub

    Function CancellaDrive(ByVal drive As String) As String
        ScriviMessaggio(String.Format(Messaggio(90), drive), lblMessaggio) 'Svuotamento drive {0} in corso.
        Dim folder As DirectoryInfo = New DirectoryInfo(drive)
        Try
            For Each Cartella As IO.DirectoryInfo In folder.GetDirectories
                Cartella.Delete(True)
            Next
            For Each File As IO.FileInfo In folder.GetFiles
                File.Delete()
            Next
        Catch ex As Exception
        End Try
        CancellaDrive = "OK"
    End Function

    Function ImpostaDHCP(Optional ByVal abilita As Boolean = True) As String
        If abilita = True Then
            ImpostaDHCP = cmdComandoSingolo("Z,1,6,15,1,1", 4, 5000)
            If ImpostaDHCP <> "OK" Then
                Exit Function
            End If
            Trasporto.DeviceName = lanDevice
            ImpostaDHCP = cmdComandoSingolo("Z,1,6,15,5," & Trasporto.DeviceName, 4, 5000)
            If ImpostaDHCP <> "OK" Then
                Exit Function
            End If
            ImpostaDHCP = cmdComandoSingolo("Z,1,6,15,11,1", 4, 5000)
            'If ImpostaDHCP <> "OK" And ImpostaDHCP <> "SYNTAX ERROR" Then
            '    Exit Function
            'Else
            ImpostaDHCP = "OK"
            'End If
            ScriviMessaggio(Messaggio(69), lblMessaggio)

        Else
            ImpostaDHCP = cmdComandoSingolo("Z,1,6,15,1,0", 4, 5000)
            If ImpostaDHCP <> "OK" Then
                Exit Function
            End If
            ImpostaDHCP = cmdComandoSingolo("Z,1,6,15,5,CMDevice", 4, 5000)
            If ImpostaDHCP <> "OK" Then
                Exit Function
            End If
            ImpostaDHCP = cmdComandoSingolo("Z,1,6,15,11,0", 4, 5000)
            'If ImpostaDHCP <> "OK" And ImpostaDHCP <> "SYNTAX ERROR" Then
            '    Exit Function
            'Else
            ImpostaDHCP = "OK"
            'End If
        End If
    End Function

    Function TestFotoForchetta() As String
test:
        Interfaccia("Normale")
        TestFotoForchetta = ""
        CmdTransparentIn()
        tcPowerOn()
        tcPressorOpen()
        Aspetta(1000)

        StatoFoto()
        xmlLog.AddTest("fHpressClose", Log.Result, photoStatus.Hpres)
        If photoStatus.Hpres = False Then
            Interfaccia("errore")
            ScriviMessaggio(Messaggio(213), lblMessaggio) 'Test foto HPRES fallito, premere un tasto per ripetere, ESC per uscire.
            Select Case AttendiTasto()
                Case "ESC"
                    TestFotoForchetta = "HPRESSKO"
                    GoTo fine
                Case Else
                    GoTo test
            End Select
        End If

        Interfaccia("attesa")
        Me.Focus()
        ScriviMessaggio(Messaggio(22), lblMessaggio) 'Portare il pressore a metà della sua corsa e premere un tasto.
        AttendiTasto()
        Interfaccia("Normale")
        ScriviMessaggio(Messaggio(23), lblMessaggio) 'Verifica foto PRESSOR in corso...
        Me.Focus()
        StatoFoto()
        xmlLog.AddTest("fHpressOpen", Log.Result, Not photoStatus.Hpres)
        If photoStatus.Hpres = True Then
            Interfaccia("errore")
            ScriviMessaggio(Messaggio(213), lblMessaggio) 'Test foto HPRES fallito, premere un tasto per ripetere, ESC per uscire.
            Select Case AttendiTasto()
                Case "ESC"
                    TestFotoForchetta = "HPRESSKO"
                    GoTo fine
                Case Else
                    GoTo test
            End Select
        End If
        xmlLog.AddTest("fPressorOpen", Log.Result, photoStatus.pressor)
        If photoStatus.pressor = False Then
            Interfaccia("errore")
            ScriviMessaggio(Messaggio(214), lblMessaggio) 'Test foto HPRES fallito, premere un tasto per ripetere, ESC per uscire.
            Select Case AttendiTasto()
                Case "ESC"
                    TestFotoForchetta = "PRESSORKO"
                    GoTo fine
                Case Else
                    GoTo test
            End Select
        End If

        tcPressorClose()
        Aspetta(1000)
        StatoFoto()
        xmlLog.AddTest("fHpressOpen2", Log.Result, Not photoStatus.Hpres)
        If photoStatus.Hpres = True Then
            Interfaccia("errore")
            ScriviMessaggio(Messaggio(213), lblMessaggio) 'Test foto HPRES fallito, premere un tasto per ripetere, ESC per uscire.
            Select Case AttendiTasto()
                Case "ESC"
                    TestFotoForchetta = "HPRESSKO"
                    GoTo fine
                Case Else
                    GoTo test
            End Select
        End If

        tcPressorHome()
        Aspetta(1000)
        StatoFoto()
        xmlLog.AddTest("fHpressClose2", Log.Result, photoStatus.Hpres)
        If photoStatus.Hpres = False Then
            Interfaccia("errore")
            ScriviMessaggio(Messaggio(213), lblMessaggio) 'Test foto HPRES fallito, premere un tasto per ripetere, ESC per uscire.
            Select Case AttendiTasto()
                Case "ESC"
                    TestFotoForchetta = "HPRESSKO"
                    GoTo fine
                Case Else
                    GoTo test
            End Select
        End If

align:
        ScriviMessaggio(Messaggio(25), lblMessaggio) 'Verifica foto ALIGN in corso...
        tcAlignHome()
        Aspetta(1000)
        StatoFoto()
        xmlLog.AddTest("fAlignOpen", Log.Result, Not photoStatus.align)
        If photoStatus.align = True Then
            Interfaccia("errore")
            ScriviMessaggio(Messaggio(215), lblMessaggio) 'Test foto ALIGN fallito, premere un tasto per ripetere, ESC per uscire.
            Select Case AttendiTasto()
                Case "ESC"
                    TestFotoForchetta = "ALIGNSKO"
                    GoTo fine
                Case Else
                    GoTo align
            End Select
        End If

        Interfaccia("attesa")
        ScriviMessaggio(Messaggio(24), lblMessaggio) 'Ruotare di circa 90 gradi l'allineatore e premere un tasto.
        AttendiTasto()
        Interfaccia("Normale")
        ScriviMessaggio(Messaggio(25), lblMessaggio) 'Verifica foto ALIGN in corso...
        StatoFoto()
        xmlLog.AddTest("fAlignClose", Log.Result, photoStatus.align)
        If photoStatus.align = False Then
            Interfaccia("errore")
            ScriviMessaggio(Messaggio(215), lblMessaggio) 'Test foto ALIGN fallito, premere un tasto per ripetere, ESC per uscire.
            Select Case AttendiTasto()
                Case "ESC"
                    TestFotoForchetta = "ALIGNSKO"
                    GoTo fine
                Case Else
                    GoTo align
            End Select
        End If

        tcAlignHome()
        Aspetta(1000)
        StatoFoto()
        xmlLog.AddTest("fAlignOpen2", Log.Result, Not photoStatus.align)
        If photoStatus.align = True Then
            Interfaccia("errore")
            ScriviMessaggio(Messaggio(215), lblMessaggio) 'Test foto ALIGN fallito, premere un tasto per ripetere, ESC per uscire.
            Select Case AttendiTasto()
                Case "ESC"
                    TestFotoForchetta = "ALIGNSKO"
                    GoTo fine
                Case Else
                    GoTo align
            End Select
        End If

push:
        ScriviMessaggio(Messaggio(26), lblMessaggio) 'Verifica foto PUSHER in corso...
        tcPusherHome()
        Aspetta(500)
        StatoFoto()
        xmlLog.AddTest("fPusherClose", Log.Result, photoStatus.pusher)
        If photoStatus.pusher = False Then
            Interfaccia("errore")
            ScriviMessaggio(Messaggio(216), lblMessaggio) 'Test foto PUSHER fallito, premere un tasto per ripetere, ESC per uscire.
            Select Case AttendiTasto()
                Case "ESC"
                    TestFotoForchetta = "PUSHERKO"
                    GoTo fine
                Case Else
                    GoTo push
            End Select
        End If

        tcPusherClose()
        Aspetta(500)
        StatoFoto()
        xmlLog.AddTest("fPusherOpen", Log.Result, Not photoStatus.align)
        If photoStatus.pusher = True Then
            Interfaccia("errore")
            ScriviMessaggio(Messaggio(216), lblMessaggio) 'Test foto PUSHER fallito, premere un tasto per ripetere, ESC per uscire.
            Select Case AttendiTasto()
                Case "ESC"
                    TestFotoForchetta = "PUSHERKO"
                    GoTo fine
                Case Else
                    GoTo push
            End Select
        End If

        tcPusherHome()
        Aspetta(500)
        StatoFoto()
        xmlLog.AddTest("fPusherClose2", Log.Result, photoStatus.pusher)
        If photoStatus.pusher = False Then
            Interfaccia("errore")
            ScriviMessaggio(Messaggio(216), lblMessaggio) 'Test foto PUSHER fallito, premere un tasto per ripetere, ESC per uscire.
            Select Case AttendiTasto()
                Case "ESC"
                    TestFotoForchetta = "PUSHERKO"
                    GoTo fine
                Case Else
                    GoTo push
            End Select
        End If

testFeeder:
        StatoFoto()
        Dim inizioTimer As Date = Now
        xmlLog.AddTest("fFeederOpen", Log.Result, "False")
        If photoStatus.feederOpen = False Then
            Interfaccia("attesa")
            ScriviMessaggio(Messaggio(27), lblMessaggio) 'Scoprire il foto FEEDER OPEN
            Do While Not photoStatus.feederOpen = True
                StatoFoto()
                Aspetta(200)
                Dim secondiPassati As Long = DateDiff(DateInterval.Second, inizioTimer, Now)
                If secondiPassati > CInt(testValuesIni.ReadValue("Fotosensori", "Attesa")) Then
                    Interfaccia("errore")
                    ScriviMessaggio(Messaggio(217), lblMessaggio) 'Test foto FEEDER OPEN fallito, premere un tasto per ripetere, ESC per uscire.
                    Select Case AttendiTasto()
                        Case "ESC"
                            TestFotoForchetta = "FEEDEROPENKO"
                            GoTo fine
                        Case Else
                            GoTo testFeeder
                    End Select
                End If
            Loop
        End If
        xmlLog.AddTest("fFeederOpen", Log.Result, "True")

        inizioTimer = Now
        Interfaccia("attesa")
        ScriviMessaggio(Messaggio(28), lblMessaggio) 'Coprire il foto FEEDER OPEN
        xmlLog.AddTest("fFeederClose", Log.Result, "False")
        Do While Not photoStatus.feederOpen = False
            StatoFoto()
            Aspetta(200)
            Dim secondiPassati As Long = DateDiff(DateInterval.Second, inizioTimer, Now)
            If secondiPassati > CInt(testValuesIni.ReadValue("Fotosensori", "Attesa")) Then
                Interfaccia("errore")
                ScriviMessaggio(Messaggio(217), lblMessaggio) 'Test foto FEEDER OPEN fallito, premere un tasto per ripetere, ESC per uscire.
                Select Case AttendiTasto()
                    Case "ESC"
                        TestFotoForchetta = "FEEDEROPENKO"
                        GoTo fine
                    Case Else
                        GoTo testFeeder
                End Select
            End If
        Loop
        xmlLog.AddTest("fFeederClose", Log.Result, "True")

testCover:
        StatoFoto()
        Interfaccia("attesa")
        inizioTimer = Now
        xmlLog.AddTest("fCoverOpen", Log.Result, "False")
        If photoStatus.coverOpen = False Then
            ScriviMessaggio(Messaggio(29), lblMessaggio) 'Estrarre il TRASPORTO SUPERIORE
            Do While Not photoStatus.coverOpen = True
                StatoFoto()
                Aspetta(200)
                Dim secondiPassati As Long = DateDiff(DateInterval.Second, inizioTimer, Now)
                If secondiPassati > CInt(testValuesIni.ReadValue("Fotosensori", "Attesa")) Then
                    Interfaccia("errore")
                    ScriviMessaggio(Messaggio(218), lblMessaggio) 'Test foto COVER OPEN fallito, premere un tasto per ripetere, ESC per uscire.
                    Select Case AttendiTasto()
                        Case "ESC"
                            TestFotoForchetta = "COVEROPENKO"
                            GoTo fine
                        Case Else
                            GoTo testCover
                    End Select
                End If
            Loop
        End If
        xmlLog.AddTest("fCoverOpen", Log.Result, "True")

        Interfaccia("attesa")
        inizioTimer = Now
        ScriviMessaggio(Messaggio(30), lblMessaggio) 'Chiuder il TRASPORTO SUPERIORE
        xmlLog.AddTest("fCoverClose", Log.Result, "False")
        Do While Not photoStatus.coverOpen = False
            StatoFoto()
            Aspetta(200)
            Dim secondiPassati As Long = DateDiff(DateInterval.Second, inizioTimer, Now)
            If secondiPassati > CInt(testValuesIni.ReadValue("Fotosensori", "Attesa")) Then
                Interfaccia("errore")
                ScriviMessaggio(Messaggio(218), lblMessaggio) 'Test foto COVER OPEN fallito, premere un tasto per ripetere, ESC per uscire.
                Select Case AttendiTasto()
                    Case "ESC"
                        TestFotoForchetta = "COVEROPENKO"
                        GoTo fine
                    Case Else
                        GoTo testCover
                End Select
            End If
        Loop
        xmlLog.AddTest("fCoverClose", Log.Result, "True")

fine:
        Interfaccia("Normale")
        tcPowerOff()
        CmdTransparentOut()
        If TestFotoForchetta.Length = 0 Then
            TestFotoForchetta = "OK"
        End If

    End Function

    Function StatoFoto() As String
        StatoFoto = ""
        photoStatus = New SPhotoStatus
        If statoConnessione <> ConnectionState.TransparentMode Then
            CmdTransparentIn()
        End If
        Dim foto(4) As String
        CmdTransparentCommand("E201", 1000)
        IIf(TransparentAnswer.Length > 0, StatoFoto = "OK", StatoFoto = "KO")
        foto(1) = Hex2Bin(Mid(TransparentAnswer, 1, 2))
        foto(2) = Hex2Bin(Mid(TransparentAnswer, 3, 2))
        foto(3) = Hex2Bin(Mid(TransparentAnswer, 5, 2))
        foto(4) = Hex2Bin(Mid(TransparentAnswer, 7, 2))

        With photoStatus
            .coverOpen = IIf(Mid(foto(2), 1, 1) = 1, True, False)
            .feederOpen = IIf(Mid(foto(2), 2, 1) = 1, True, False)
            .reject = IIf(Mid(foto(2), 3, 1) = 1, True, False)
            '.fpbksus = IIf(Mid(foto(2), 4, 1) = 1, True, False)
            '.fpelsus = IIf(Mid(foto(2), 5, 1) = 1, True, False)
            '.fppock = IIf(Mid(foto(2), 6, 1) = 1, True, False)
            .feed = IIf(Mid(foto(3), 1, 1) = 1, True, False)
            .pressor = IIf(Mid(foto(3), 2, 1) = 1, True, False)
            .inLeft = IIf(Mid(foto(3), 3, 1) = 1, True, False)
            .inCenter = IIf(Mid(foto(3), 4, 1) = 1, True, False)
            .curve1 = IIf(Mid(foto(3), 5, 1) = 1, True, False)
            .curve3 = IIf(Mid(foto(3), 6, 1) = 1, True, False)
            .curve4A = IIf(Mid(foto(3), 7, 1) = 1, True, False)
            .curve4B = IIf(Mid(foto(3), 8, 1) = 1, True, False)
            .pusher = IIf(Mid(foto(4), 1, 1) = 1, True, False)
            .align = IIf(Mid(foto(4), 2, 1) = 1, True, False)
            .Hpres = IIf(Mid(foto(4), 3, 1) = 1, True, False)
            .shift = IIf(Mid(foto(4), 4, 1) = 1, True, False)
            .inq = IIf(Mid(foto(4), 5, 1) = 1, True, False)
            .count = IIf(Mid(foto(4), 6, 1) = 1, True, False)
            .out = IIf(Mid(foto(4), 7, 1) = 1, True, False)
            .Hmax = IIf(Mid(foto(4), 8, 1) = 1, True, False)
        End With
        'CmdTransparentOut()
    End Function

    Function TaraturaFoto() As String
taraFoto:
        Interfaccia("Normale")
        tabella.Visible = True
        tabella.Rows.Clear()
        Dim valoreTaratura(30) As Integer

        'Dim strdebug As String
        ScriviMessaggio(Messaggio(21), lblMessaggio) 'Taratura fotosensori in corso...

        TaraturaFoto = "OK"
        valoreTaratura(0) = 1
        Dim valMaxFotoPos As Integer
        Dim valMinFotoNeg As Integer
leggifileini:
        Try
            valMaxFotoPos = testValuesIni.ReadValue("Fotosensori", "valMaxFotoPos")
            valMinFotoNeg = testValuesIni.ReadValue("Fotosensori", "valMinFotoNeg")
            With Trasporto.fotoRealTime
                .InCenter.Min = testValuesIni.ReadValue("Fotosensori", "fInMin")
                .InLeft.Min = testValuesIni.ReadValue("Fotosensori", "fInlMin")
                .HMax.Min = testValuesIni.ReadValue("Fotosensori", "fHmaxMin")
                .Feed.Min = testValuesIni.ReadValue("Fotosensori", "fFeederMin")
                .C1.Min = testValuesIni.ReadValue("Fotosensori", "fFeeder2Min")
                .Shift.Min = testValuesIni.ReadValue("Fotosensori", "fShiftMin")
                .InQ.Min = testValuesIni.ReadValue("Fotosensori", "fInqMin")
                .Count.Min = testValuesIni.ReadValue("Fotosensori", "fCountMin")
                .Out.Min = testValuesIni.ReadValue("Fotosensori", "fOutMin")
                .C3.Min = testValuesIni.ReadValue("Fotosensori", "fOut3Min")
                .C4A.Min = testValuesIni.ReadValue("Fotosensori", "fOut4AMin")
                .C4B.Min = testValuesIni.ReadValue("Fotosensori", "fOut4BMin")
                .Rej.Min = testValuesIni.ReadValue("Fotosensori", "fRejectMin")
                .InBox.Min = testValuesIni.ReadValue("Fotosensori", "fInBoxMin")

                .InCenter.SogliaMin = testValuesIni.ReadValue("Fotosensori", "thInMin")
                .InLeft.SogliaMin = testValuesIni.ReadValue("Fotosensori", "thInlMin")
                .HMax.SogliaMin = testValuesIni.ReadValue("Fotosensori", "thHmaxMin")
                .Feed.SogliaMin = testValuesIni.ReadValue("Fotosensori", "thFeederMin")
                .C1.SogliaMin = testValuesIni.ReadValue("Fotosensori", "thFeeder2Min")
                .Shift.SogliaMin = testValuesIni.ReadValue("Fotosensori", "thShiftMin")
                .InQ.SogliaMin = testValuesIni.ReadValue("Fotosensori", "thInqMin")
                .Count.SogliaMin = testValuesIni.ReadValue("Fotosensori", "thCountMin")
                .Out.SogliaMin = testValuesIni.ReadValue("Fotosensori", "thOutMin")
                .C3.SogliaMin = testValuesIni.ReadValue("Fotosensori", "thOut3Min")
                .C4A.SogliaMin = testValuesIni.ReadValue("Fotosensori", "thOut4AMin")
                .C4B.SogliaMin = testValuesIni.ReadValue("Fotosensori", "thOut4BMin")
                .Rej.SogliaMin = testValuesIni.ReadValue("Fotosensori", "thRejectMin")
                .InBox.SogliaMin = testValuesIni.ReadValue("Fotosensori", "thInBoxMin")


                .InCenter.Max = testValuesIni.ReadValue("Fotosensori", "fInMax")
                .InLeft.Max = testValuesIni.ReadValue("Fotosensori", "fInlMax")
                .HMax.Max = testValuesIni.ReadValue("Fotosensori", "fHmaxMax")
                .Feed.Max = testValuesIni.ReadValue("Fotosensori", "fFeederMax")
                .C1.Max = testValuesIni.ReadValue("Fotosensori", "fFeeder2Max")
                .Shift.Max = testValuesIni.ReadValue("Fotosensori", "fShiftMax")
                .InQ.Max = testValuesIni.ReadValue("Fotosensori", "fInqMax")
                .Count.Max = testValuesIni.ReadValue("Fotosensori", "fCountMax")
                .Out.Max = testValuesIni.ReadValue("Fotosensori", "fOutMax")
                .C3.Max = testValuesIni.ReadValue("Fotosensori", "fOut3Max")
                .C4A.Max = testValuesIni.ReadValue("Fotosensori", "fOut4AMax")
                .C4B.Max = testValuesIni.ReadValue("Fotosensori", "fOut4BMax")
                .Rej.Max = testValuesIni.ReadValue("Fotosensori", "fRejectMax")
                .InBox.Max = testValuesIni.ReadValue("Fotosensori", "fInBoxMax")

                .InCenter.SogliaMax = testValuesIni.ReadValue("Fotosensori", "thInMax")
                .InLeft.SogliaMax = testValuesIni.ReadValue("Fotosensori", "thInlMax")
                .HMax.SogliaMax = testValuesIni.ReadValue("Fotosensori", "thHmaxMax")
                .Feed.SogliaMax = testValuesIni.ReadValue("Fotosensori", "thFeederMax")
                .C1.SogliaMax = testValuesIni.ReadValue("Fotosensori", "thFeeder2Max")
                .Shift.SogliaMax = testValuesIni.ReadValue("Fotosensori", "thShiftMax")
                .InQ.SogliaMax = testValuesIni.ReadValue("Fotosensori", "thInqMax")
                .Count.SogliaMax = testValuesIni.ReadValue("Fotosensori", "thCountMax")
                .Out.SogliaMax = testValuesIni.ReadValue("Fotosensori", "thOutMax")
                .C3.SogliaMax = testValuesIni.ReadValue("Fotosensori", "thOut3Max")
                .C4A.SogliaMax = testValuesIni.ReadValue("Fotosensori", "thOut4AMax")
                .C4B.SogliaMax = testValuesIni.ReadValue("Fotosensori", "thOut4BMax")
                .Rej.SogliaMax = testValuesIni.ReadValue("Fotosensori", "thRejectMax")
                .InBox.SogliaMax = testValuesIni.ReadValue("Fotosensori", "thInBoxMax")
            End With
        Catch ex As Exception
            Interfaccia("errore")
            ScriviMessaggio(Messaggio(263), lblMessaggio) ' Non è stato possibile leggere tutti i valori dal file ini (thevalue.ini). Non è possibile proseguire, premi un tasto per uscire
            TaraturaFoto = "KO"
            GoTo fineTaratura
        End Try

        tabella.Visible = True
        tabella.ColumnCount = 7
        tabella.Columns(0).Name = "PHOTO NAME"
        tabella.Columns(1).Name = "CUR VALUE"
        tabella.Columns(2).Name = "CUR RANGE"
        tabella.Columns(3).Name = "TH VALUE"
        tabella.Columns(4).Name = "TH RANGE"
        tabella.Columns(5).Name = "AMB VALUE"
        tabella.Columns(6).Name = "AMB RANGE"
        tabella.RowHeadersVisible = False
        tabella.Rows.Add("INCENTER", "-", Trasporto.fotoRealTime.InCenter.Min & "-" & Trasporto.fotoRealTime.InCenter.Max, "-", Trasporto.fotoRealTime.InCenter.SogliaMin & "-" & Trasporto.fotoRealTime.InCenter.SogliaMax, "-", ">" & valMinFotoNeg)
        tabella.Rows.Add("INLEFT", "-", Trasporto.fotoRealTime.InLeft.Min & "-" & Trasporto.fotoRealTime.InLeft.Max, "-", Trasporto.fotoRealTime.InLeft.SogliaMin & "-" & Trasporto.fotoRealTime.InLeft.SogliaMax, "-", ">" & valMinFotoNeg)
        tabella.Rows.Add("HMAX", "-", Trasporto.fotoRealTime.HMax.Min & "-" & Trasporto.fotoRealTime.HMax.Max, "-", Trasporto.fotoRealTime.HMax.SogliaMin & "-" & Trasporto.fotoRealTime.HMax.SogliaMax, "-", ">" & valMinFotoNeg)
        tabella.Rows.Add("FEEDER", "-", Trasporto.fotoRealTime.Feed.Min & "-" & Trasporto.fotoRealTime.Feed.Max, "-", Trasporto.fotoRealTime.Feed.SogliaMin & "-" & Trasporto.fotoRealTime.Feed.SogliaMax, "-", "<" & valMaxFotoPos)
        tabella.Rows.Add("FEEDER2", "-", Trasporto.fotoRealTime.C1.Min & "-" & Trasporto.fotoRealTime.C1.Max, "-", Trasporto.fotoRealTime.C1.SogliaMin & "-" & Trasporto.fotoRealTime.C1.SogliaMax, "-", "<" & valMaxFotoPos)
        tabella.Rows.Add("SHIFT", "-", Trasporto.fotoRealTime.Shift.Min & "-" & Trasporto.fotoRealTime.Shift.Max, "-", Trasporto.fotoRealTime.Shift.SogliaMin & "-" & Trasporto.fotoRealTime.Shift.SogliaMax, "-", "<" & valMaxFotoPos)
        tabella.Rows.Add("INQ", "-", Trasporto.fotoRealTime.InQ.Min & "-" & Trasporto.fotoRealTime.InQ.Max, "-", Trasporto.fotoRealTime.InQ.SogliaMin & "-" & Trasporto.fotoRealTime.InQ.SogliaMax, "-", ">" & valMinFotoNeg)
        tabella.Rows.Add("COUNT", "-", Trasporto.fotoRealTime.Count.Min & "-" & Trasporto.fotoRealTime.Count.Max, "-", Trasporto.fotoRealTime.Count.SogliaMin & "-" & Trasporto.fotoRealTime.Count.SogliaMax, "-", "<" & valMaxFotoPos)
        tabella.Rows.Add("OUT", "-", Trasporto.fotoRealTime.Out.Min & "-" & Trasporto.fotoRealTime.Out.Max, "-", Trasporto.fotoRealTime.Out.SogliaMin & "-" & Trasporto.fotoRealTime.Out.SogliaMax, "-", ">" & valMinFotoNeg)
        tabella.Rows.Add("C3", "-", Trasporto.fotoRealTime.C3.Min & "-" & Trasporto.fotoRealTime.C3.Max, "-", Trasporto.fotoRealTime.C3.SogliaMin & "-" & Trasporto.fotoRealTime.C3.SogliaMax, "-", "<" & valMaxFotoPos)
        tabella.Rows.Add("C4A", "-", Trasporto.fotoRealTime.C4A.Min & "-" & Trasporto.fotoRealTime.C4A.Max, "-", Trasporto.fotoRealTime.C4A.SogliaMin & "-" & Trasporto.fotoRealTime.C4A.SogliaMax, "-", "<" & valMaxFotoPos)
        tabella.Rows.Add("C4B", "-", Trasporto.fotoRealTime.C4B.Min & "-" & Trasporto.fotoRealTime.C4B.Max, "-", Trasporto.fotoRealTime.C4B.SogliaMin & "-" & Trasporto.fotoRealTime.C4B.SogliaMax, "-", "<" & valMaxFotoPos)
        tabella.Rows.Add("REJ", "-", Trasporto.fotoRealTime.Rej.Min & "-" & Trasporto.fotoRealTime.Rej.Max, "-", Trasporto.fotoRealTime.Rej.SogliaMin & "-" & Trasporto.fotoRealTime.Rej.SogliaMax, "-", ">" & valMinFotoNeg)
        tabella.Rows.Add("INBOX", "-", Trasporto.fotoRealTime.InBox.Min & "-" & Trasporto.fotoRealTime.InBox.Max, "-", Trasporto.fotoRealTime.InBox.SogliaMin & "-" & Trasporto.fotoRealTime.InBox.SogliaMax, "-", ">" & valMinFotoNeg)

        ' TARATURA FOTO
        ' T,n,0,60,0,ph_id (0=all, 1=upper, 2=lower)
        ' T,n,0,60,0,rc
        ' LETTURA VALORI AMBIENTE
        ' T,n,0,60,3,type (0=ambient)
        ' T,n,0,60,3,rc, ph_incenter,ph_inleft,ph_hmax,ph_feed,ph_c1,ph_shift, ph_inq, ph_count, ph_out, ph_c3, ph_c4a, ph_c4b, ph_rej,
        '   ph_in_box, ph_res1, ph_res2, ph_res3, ph_res4, ….., ph_res26
        ' LETTURA TARATURA CORRENTI
        ' T,n,0,60,1
        ' T,n,0,60,1,rc, ph_incenter,ph_inleft,ph_hmax,ph_feed,ph_c1,ph_shift, ph_inq, ph_count, ph_out, ph_c3, ph_c4a, ph_c4b, ph_rej,ph_in_box, ph_res1, ph_res2, ph_res3, ph_res4, ph_res5, ph_res6,
        '   ph_cashleft, ph_cashright, ph_res7,….ph_res24
        ' LETTURA SOGLIE
        ' T,n,0,60,2
        ' T,n,0,60,2,rc, ph_incenter,ph_inleft,ph_hmax,ph_feed,ph_c1,ph_shift, ph_inq, ph_count, ph_out, ph_c3, ph_c4a, ph_c4b, ph_rej,
        '   ph_in_box, ph_res1, ph_res2, ph_res3, ph_res4, ….., ph_res26

taratura:
        'CmdTransparentIn()
        'ScriviMessaggio(Messaggio(87), lblMessaggio)
        'CmdTransparentCommand("D271", 15000)
        'CmdTransparentOut()
        Select Case cmdComandoSingolo("T,1,0,60,0,0", 5, 15000)
            Case "OK"
            Case Else
                Interfaccia("errore")
                ScriviMessaggio(Messaggio(261), lblMessaggio) 'Taratura foto non riuscita. Premi un tasto per ripetere, ESC per uscire
                Select Case AttendiTasto()
                    Case "ESC"
                        tabella.Visible = False
                        Exit Function
                    Case Else
                        GoTo taraFoto
                End Select

        End Select

        ' Lettura valori luci ambiente
luceAmbiente:
        ScriviMessaggio(Messaggio(87), lblMessaggio) 'Lettura valori luci ambiente in corso
        Select Case cmdComandoSingolo("T,1,0,60,3,0", 5)
            Case "OK"
            Case Else
                Interfaccia("errore")
                ScriviMessaggio(Messaggio(264), lblMessaggio) 'Non è stato possibile verificare la luce ambiente. Premi un tasto per ripetere, ESC per uscire
                Select Case AttendiTasto()
                    Case "ESC"
                        tabella.Visible = False
                        Exit Function
                    Case Else
                        GoTo luceAmbiente
                End Select
        End Select
        With Trasporto.fotoRealTime
            If Int32.TryParse(comandoSingoloRisposta(6), .InCenter.AmbienteValore) Then
                tabella.Item(5, 0).Value = .InCenter.AmbienteValore
            End If
            If Int32.TryParse(comandoSingoloRisposta(7), .InLeft.AmbienteValore) Then
                tabella.Item(5, 1).Value = .InLeft.AmbienteValore
            End If
            If Int32.TryParse(comandoSingoloRisposta(8), .HMax.AmbienteValore) Then
                tabella.Item(5, 2).Value = .HMax.AmbienteValore
            End If
            If Int32.TryParse(comandoSingoloRisposta(9), .Feed.AmbienteValore) Then
                tabella.Item(5, 3).Value = .Feed.AmbienteValore
            End If
            If Int32.TryParse(comandoSingoloRisposta(10), .C1.AmbienteValore) Then
                tabella.Item(5, 4).Value = .C1.AmbienteValore
            End If
            If Int32.TryParse(comandoSingoloRisposta(11), .Shift.AmbienteValore) Then
                tabella.Item(5, 5).Value = .Shift.AmbienteValore
            End If
            If Int32.TryParse(comandoSingoloRisposta(12), .InQ.AmbienteValore) Then
                tabella.Item(5, 6).Value = .InQ.AmbienteValore
            End If
            If Int32.TryParse(comandoSingoloRisposta(13), .Count.AmbienteValore) Then
                tabella.Item(5, 7).Value = .Count.AmbienteValore
            End If
            If Int32.TryParse(comandoSingoloRisposta(14), .Out.AmbienteValore) Then
                tabella.Item(5, 8).Value = .Out.AmbienteValore
            End If
            If Int32.TryParse(comandoSingoloRisposta(15), .C3.AmbienteValore) Then
                tabella.Item(5, 9).Value = .C3.AmbienteValore
            End If
            If Int32.TryParse(comandoSingoloRisposta(16), .C4A.AmbienteValore) Then
                tabella.Item(5, 10).Value = .C4A.AmbienteValore
            End If
            If Int32.TryParse(comandoSingoloRisposta(17), .C4B.AmbienteValore) Then
                tabella.Item(5, 11).Value = .C4B.AmbienteValore
            End If
            If Int32.TryParse(comandoSingoloRisposta(18), .Rej.AmbienteValore) Then
                tabella.Item(5, 12).Value = .Rej.AmbienteValore
            End If
            If Trasporto.CRM = True Then
                Int32.TryParse(comandoSingoloRisposta(19), .InBox.AmbienteValore)
                tabella.Item(5, 13).Value = .InBox.AmbienteValore
            End If

            ' Foto positivi 
            If .Feed.AmbienteValore > valMaxFotoPos Then
                TaraturaFoto = "KO"
                tabella.Item(5, 3).Style.BackColor = ArcaColor.Arancio
            End If
            If .C1.AmbienteValore > valMaxFotoPos Then
                TaraturaFoto = "KO"
                tabella.Item(5, 4).Style.BackColor = ArcaColor.Arancio
            End If
            If .Shift.AmbienteValore > valMaxFotoPos Then
                TaraturaFoto = "KO"
                tabella.Item(5, 5).Style.BackColor = ArcaColor.Arancio
            End If
            If .Count.AmbienteValore > valMaxFotoPos Then
                TaraturaFoto = "KO"
                tabella.Item(5, 7).Style.BackColor = ArcaColor.Arancio
            End If
            If .C3.AmbienteValore > valMaxFotoPos Then
                TaraturaFoto = "KO"
                tabella.Item(5, 9).Style.BackColor = ArcaColor.Arancio
            End If
            If .C4A.AmbienteValore > valMaxFotoPos Then
                TaraturaFoto = "KO"
                tabella.Item(5, 10).Style.BackColor = ArcaColor.Arancio
            End If
            If .C4B.AmbienteValore > valMaxFotoPos Then
                TaraturaFoto = "KO"
                tabella.Item(5, 11).Style.BackColor = ArcaColor.Arancio
            End If

            ' Foto negativi
            If .InCenter.AmbienteValore < valMinFotoNeg Then
                TaraturaFoto = "KO"
                tabella.Item(5, 0).Style.BackColor = ArcaColor.Arancio
            End If
            If .InLeft.AmbienteValore < valMinFotoNeg Then
                TaraturaFoto = "KO"
                tabella.Item(5, 1).Style.BackColor = ArcaColor.Arancio
            End If
            If .HMax.AmbienteValore < valMinFotoNeg Then
                TaraturaFoto = "KO"
                tabella.Item(5, 2).Style.BackColor = ArcaColor.Arancio
            End If
            If .InQ.AmbienteValore < valMinFotoNeg Then
                TaraturaFoto = "KO"
                tabella.Item(5, 6).Style.BackColor = ArcaColor.Arancio
            End If
            If .Out.AmbienteValore < valMinFotoNeg Then
                TaraturaFoto = "KO"
                tabella.Item(5, 8).Style.BackColor = ArcaColor.Arancio
            End If
            If .Rej.AmbienteValore < valMinFotoNeg Then
                TaraturaFoto = "KO"
                tabella.Item(5, 12).Style.BackColor = ArcaColor.Arancio
            End If

            If .InBox.AmbienteValore < valMinFotoNeg And Trasporto.CRM = True Then
                TaraturaFoto = "KO"
                tabella.Item(5, 13).Style.BackColor = ArcaColor.Arancio
            End If
        End With

        If TaraturaFoto = "KO" Then
            ScriviMessaggio(Messaggio(260), lblMessaggio) 'Luce ambiente elevata, applicare la copertura o sostituire il foto se copertura già applicata. Premi un tasto per ripetere, ESC per uscire
            If AttendiTasto() = "ESC" Then
                GoTo fineTaratura
            End If
            TaraturaFoto = "OK"
            GoTo taratura
        End If


        ScriviMessaggio(Messaggio(265), lblMessaggio) 'Lettura valori di taratura corrente in corso
        Select Case cmdComandoSingolo("T,1,0,60,1", 5)
            Case "OK"
            Case Else
                Interfaccia("errore")
                ScriviMessaggio(Messaggio(266), lblMessaggio) 'Non è stato possibile verificare la taratura dei foto. Premi un tasto per ripetere, ESC per uscire
                Select Case AttendiTasto()
                    Case "ESC"
                        tabella.Visible = False
                        Exit Function
                    Case Else
                        GoTo luceAmbiente
                End Select
        End Select

        Trasporto.fotoRealTime.InCenter.Valore = comandoSingoloRisposta(6)
        tabella.Item(1, 0).Value = Trasporto.fotoRealTime.InCenter.Valore
        Trasporto.fotoRealTime.InLeft.Valore = comandoSingoloRisposta(7)
        tabella.Item(1, 1).Value = Trasporto.fotoRealTime.InLeft.Valore
        Trasporto.fotoRealTime.HMax.Valore = comandoSingoloRisposta(8)
        tabella.Item(1, 2).Value = Trasporto.fotoRealTime.HMax.Valore
        Trasporto.fotoRealTime.Feed.Valore = comandoSingoloRisposta(9)
        tabella.Item(1, 3).Value = Trasporto.fotoRealTime.Feed.Valore
        Trasporto.fotoRealTime.C1.Valore = comandoSingoloRisposta(10)
        tabella.Item(1, 4).Value = Trasporto.fotoRealTime.C1.Valore
        Trasporto.fotoRealTime.Shift.Valore = comandoSingoloRisposta(11)
        tabella.Item(1, 5).Value = Trasporto.fotoRealTime.Shift.Valore
        Trasporto.fotoRealTime.InQ.Valore = comandoSingoloRisposta(12)
        tabella.Item(1, 6).Value = Trasporto.fotoRealTime.InQ.Valore
        Trasporto.fotoRealTime.Count.Valore = comandoSingoloRisposta(13)
        tabella.Item(1, 7).Value = Trasporto.fotoRealTime.Count.Valore
        Trasporto.fotoRealTime.Out.Valore = comandoSingoloRisposta(14)
        tabella.Item(1, 8).Value = Trasporto.fotoRealTime.Out.Valore
        Trasporto.fotoRealTime.C3.Valore = comandoSingoloRisposta(15)
        tabella.Item(1, 9).Value = Trasporto.fotoRealTime.C3.Valore
        Trasporto.fotoRealTime.C4A.Valore = comandoSingoloRisposta(16)
        tabella.Item(1, 10).Value = Trasporto.fotoRealTime.C4A.Valore
        Trasporto.fotoRealTime.C4B.Valore = comandoSingoloRisposta(17)
        tabella.Item(1, 11).Value = Trasporto.fotoRealTime.C4B.Valore
        Trasporto.fotoRealTime.Rej.Valore = comandoSingoloRisposta(18)
        tabella.Item(1, 12).Value = Trasporto.fotoRealTime.Rej.Valore
        If Trasporto.CRM = True Then
            Trasporto.fotoRealTime.InBox.Valore = comandoSingoloRisposta(19)
            tabella.Item(1, 13).Value = Trasporto.fotoRealTime.InBox.AmbienteValore
        End If

        'CmdTransparentIn()
        'ScriviMessaggio(Messaggio(71), lblMessaggio)
        'CmdTransparentCommand("E226", 1000) 'Lettura Taratura foto INCENTER
        'valoreTaratura(7) = AsciCode2AsciChar(TransparentAnswer)
        'ScriviMessaggio(Messaggio(72), lblMessaggio) 'Lettura Taratura foto INLEFT
        'CmdTransparentCommand("E229", 1000)
        'valoreTaratura(8) = AsciCode2AsciChar(TransparentAnswer)
        'ScriviMessaggio(Messaggio(73), lblMessaggio) 'Lettura Taratura foto HMAX
        'CmdTransparentCommand("E22B", 1000)
        'valoreTaratura(9) = AsciCode2AsciChar(TransparentAnswer)
        'ScriviMessaggio(Messaggio(74), lblMessaggio) 'Lettura Taratura foto FEEDER
        'CmdTransparentCommand("E227", 1000)
        'valoreTaratura(10) = AsciCode2AsciChar(TransparentAnswer)
        'ScriviMessaggio(Messaggio(75), lblMessaggio) 'Lettura Taratura foto FEEDER2
        'CmdTransparentCommand("E228", 1000)
        'valoreTaratura(11) = AsciCode2AsciChar(TransparentAnswer)
        'ScriviMessaggio(Messaggio(76), lblMessaggio) 'Lettura Taratura foto SHIFT
        'CmdTransparentCommand("E220", 1000)
        'valoreTaratura(12) = AsciCode2AsciChar(TransparentAnswer)
        'ScriviMessaggio(Messaggio(77), lblMessaggio) 'Lettura Taratura foto INQ
        'CmdTransparentCommand("E221", 1000)
        'valoreTaratura(13) = AsciCode2AsciChar(TransparentAnswer)
        'ScriviMessaggio(Messaggio(78), lblMessaggio) 'Lettura Taratura foto COUNT
        'CmdTransparentCommand("E222", 1000)
        'valoreTaratura(14) = AsciCode2AsciChar(TransparentAnswer)
        'ScriviMessaggio(Messaggio(79), lblMessaggio) 'Lettura Taratura foto OUT
        'CmdTransparentCommand("E22A", 1000)
        'valoreTaratura(15) = AsciCode2AsciChar(TransparentAnswer)
        'ScriviMessaggio(Messaggio(80), lblMessaggio) 'Lettura Taratura foto C3
        'CmdTransparentCommand("E224", 1000)
        'valoreTaratura(16) = AsciCode2AsciChar(TransparentAnswer)
        'ScriviMessaggio(Messaggio(81), lblMessaggio) 'Lettura Taratura foto C4A
        'CmdTransparentCommand("E223", 1000)
        'valoreTaratura(17) = AsciCode2AsciChar(TransparentAnswer)
        'ScriviMessaggio(Messaggio(82), lblMessaggio) 'Lettura Taratura foto C4B
        'CmdTransparentCommand("E230", 1000)
        'valoreTaratura(18) = AsciCode2AsciChar(TransparentAnswer)
        'ScriviMessaggio(Messaggio(83), lblMessaggio) 'Lettura Taratura foto REJ
        'CmdTransparentCommand("E225", 1000)
        'valoreTaratura(19) = AsciCode2AsciChar(TransparentAnswer)
        'If Trasporto.CRM = True Then
        '    CmdTransparentCommand("D24A", 14000)
        '    ScriviMessaggio(Messaggio(84), lblMessaggio) 'Lettura Taratura foto INBOX
        '    CmdTransparentCommand("E231", 1000)
        '    valoreTaratura(20) = AsciCode2AsciChar(TransparentAnswer)
        'End If

        'CmdTransparentOut()
        'Trasporto.fotoRealTime.InCenter.Valore = valoreTaratura(7)
        'Trasporto.fotoRealTime.InLeft.Valore = valoreTaratura(8)
        'Trasporto.fotoRealTime.HMax.Valore = valoreTaratura(9)
        'Trasporto.fotoRealTime.Feed.Valore = valoreTaratura(10)
        'Trasporto.fotoRealTime.C1.Valore = valoreTaratura(11)
        'Trasporto.fotoRealTime.Shift.Valore = valoreTaratura(12)
        'Trasporto.fotoRealTime.InQ.Valore = valoreTaratura(13)
        'Trasporto.fotoRealTime.Count.Valore = valoreTaratura(14)
        'Trasporto.fotoRealTime.Out.Valore = valoreTaratura(15)
        'Trasporto.fotoRealTime.C3.Valore = valoreTaratura(16)
        'Trasporto.fotoRealTime.C4A.Valore = valoreTaratura(17)
        'Trasporto.fotoRealTime.C4B.Valore = valoreTaratura(18)
        'Trasporto.fotoRealTime.Rej.Valore = valoreTaratura(19)
        'Trasporto.fotoRealTime.InBox.Valore = valoreTaratura(20)

        'tabella.Item(1, 0).Value = Trasporto.fotoRealTime.InCenter.Valore
        'tabella.Item(1, 1).Value = Trasporto.fotoRealTime.InLeft.Valore
        'tabella.Item(1, 2).Value = Trasporto.fotoRealTime.HMax.Valore
        'tabella.Item(1, 3).Value = Trasporto.fotoRealTime.Feed.Valore
        'tabella.Item(1, 4).Value = Trasporto.fotoRealTime.C1.Valore
        'tabella.Item(1, 5).Value = Trasporto.fotoRealTime.Shift.Valore
        'tabella.Item(1, 6).Value = Trasporto.fotoRealTime.InQ.Valore
        'tabella.Item(1, 7).Value = Trasporto.fotoRealTime.Count.Valore
        'tabella.Item(1, 8).Value = Trasporto.fotoRealTime.Out.Valore
        'tabella.Item(1, 9).Value = Trasporto.fotoRealTime.C3.Valore
        'tabella.Item(1, 10).Value = Trasporto.fotoRealTime.C4A.Valore
        'tabella.Item(1, 11).Value = Trasporto.fotoRealTime.C4B.Valore
        'tabella.Item(1, 12).Value = Trasporto.fotoRealTime.Rej.Valore
        'If Trasporto.CRM = True Then
        '    tabella.Item(1, 13).Value = Trasporto.fotoRealTime.InBox.AmbienteValore
        'End If


        'LEGGI SOGLIE
soglie:
        ScriviMessaggio(Messaggio(267), lblMessaggio) 'Lettura valori di soglia in corso
        If Trasporto.tipoController <> ENGICAM Then GoTo verificaCorrenti
        Select Case cmdComandoSingolo("T,1,0,60,2", 5)
            Case "OK"

            Case Else
                ScriviMessaggio(Messaggio(262), lblMessaggio, Color.Red) ' Non è stato possibile leggere i valori di soglie dei foto. Premi un tasto per ripetere, ESC per uscire
                Select Case AttendiTasto()
                    Case "ESC"
                        tabella.Visible = False
                        Exit Function
                    Case Else
                        GoTo soglie
                End Select

                'Lettura comparatori
        End Select
        'T,n,0,60,2,rc, ph_incenter,ph_inleft,ph_hmax,ph_feed,ph_c1, ph_shift,ph_inq,ph_count,ph_out,ph_c3,ph_c4a,ph_c4b,
        'ph_rej, ph_in_box, ph_res1, ph_res2, ph_res3, ph_res4, ….., ph_res26
        With Trasporto.fotoRealTime
            Int32.TryParse(comandoSingoloRisposta(6), .InCenter.SogliaValore)
            Int32.TryParse(comandoSingoloRisposta(7), .InLeft.SogliaValore)
            Int32.TryParse(comandoSingoloRisposta(8), .HMax.SogliaValore)
            Int32.TryParse(comandoSingoloRisposta(9), .Feed.SogliaValore)
            Int32.TryParse(comandoSingoloRisposta(10), .C1.SogliaValore)
            Int32.TryParse(comandoSingoloRisposta(11), .Shift.SogliaValore)
            Int32.TryParse(comandoSingoloRisposta(12), .InQ.SogliaValore)
            Int32.TryParse(comandoSingoloRisposta(13), .Count.SogliaValore)
            Int32.TryParse(comandoSingoloRisposta(14), .Out.SogliaValore)
            Int32.TryParse(comandoSingoloRisposta(15), .C3.SogliaValore)
            Int32.TryParse(comandoSingoloRisposta(16), .C4A.SogliaValore)
            Int32.TryParse(comandoSingoloRisposta(17), .C4B.SogliaValore)
            Int32.TryParse(comandoSingoloRisposta(18), .Rej.SogliaValore)
            Int32.TryParse(comandoSingoloRisposta(19), .InBox.SogliaValore)
            tabella.Item(3, 0).Value = .InCenter.SogliaValore
            tabella.Item(3, 1).Value = .InLeft.SogliaValore
            tabella.Item(3, 2).Value = .HMax.SogliaValore
            tabella.Item(3, 3).Value = .Feed.SogliaValore
            tabella.Item(3, 4).Value = .C1.SogliaValore
            tabella.Item(3, 5).Value = .Shift.SogliaValore
            tabella.Item(3, 6).Value = .InQ.SogliaValore
            tabella.Item(3, 7).Value = .Count.SogliaValore
            tabella.Item(3, 8).Value = .Out.SogliaValore
            tabella.Item(3, 9).Value = .C3.SogliaValore
            tabella.Item(3, 10).Value = .C4A.SogliaValore
            tabella.Item(3, 11).Value = .C4B.SogliaValore
            tabella.Item(3, 12).Value = .Rej.SogliaValore
        End With


verificaCorrenti:
        With Trasporto.fotoRealTime
            'tabella.Rows.Add("INCENTER", .InCenter.Valore, .InCenter.Min & " - " & .InCenter.Max, .InCenter.SogliaValore, .InCenter.SogliaMin & " - " & .InCenter.SogliaMax)
            If .InCenter.Valore < .InCenter.Min Or .InCenter.Valore > .InCenter.Max Then
                TaraturaFoto = "KO"
                tabella.Item(1, 0).Style.BackColor = ArcaColor.Arancio
            End If
            If Trasporto.tipoController = ENGICAM Then
                If .InCenter.SogliaValore < .InCenter.SogliaMin Or .InCenter.SogliaValore > .InCenter.SogliaMax Then
                    TaraturaFoto = "KO"
                    tabella.Item(3, 0).Style.BackColor = ArcaColor.Arancio
                End If
            End If
            xmlLog.AddTest("fInCenterAdj", Log.Result, TaraturaFoto, "value", .InCenter.Valore, "threshold", .InCenter.SogliaValore, "ambient", .InCenter.AmbienteValore)

            'tabella.Rows.Add("INLEFT", .InLeft.Valore, .InLeft.Min & " - " & .InLeft.Max, .InLeft.SogliaValore, .InLeft.SogliaMin & " - " & .InLeft.SogliaMax)
            If .InLeft.Valore < .InLeft.Min Or .InLeft.Valore > .InLeft.Max Then
                TaraturaFoto = "KO"
                tabella.Item(1, 1).Style.BackColor = ArcaColor.Arancio
            End If
            If Trasporto.tipoController = ENGICAM Then
                If .InLeft.SogliaValore < .InLeft.SogliaMin Or .InLeft.SogliaValore > .InLeft.SogliaMax Then
                    TaraturaFoto = "KO"
                    tabella.Item(3, 1).Style.BackColor = ArcaColor.Arancio
                End If
            End If
            xmlLog.AddTest("fInLeftAdj", Log.Result, TaraturaFoto, "value", .InLeft.Valore, "threshold", .InLeft.SogliaValore, "ambient", .InLeft.AmbienteValore)

            'tabella.Rows.Add("HMAX", .HMax.Valore, .HMax.Min & " - " & .HMax.Max, .HMax.SogliaValore, .HMax.SogliaMin & " - " & .HMax.SogliaMax)
            If .HMax.Valore < .HMax.Min Or .HMax.Valore > .HMax.Max Then
                TaraturaFoto = "KO"
                tabella.Item(1, 2).Style.BackColor = ArcaColor.Arancio
            End If
            If Trasporto.tipoController = ENGICAM Then
                If .HMax.SogliaValore < .HMax.SogliaMin Or .HMax.SogliaValore > .HMax.SogliaMax Then
                    TaraturaFoto = "KO"
                    tabella.Item(3, 2).Style.BackColor = ArcaColor.Arancio
                End If
            End If
            xmlLog.AddTest("fHMaxAdj", Log.Result, TaraturaFoto, "value", .HMax.Valore, "threshold", .HMax.SogliaValore, "ambient", .HMax.AmbienteValore)

            'tabella.Rows.Add("FEED", .Feed.Valore, .Feed.Min & " - " & .Feed.Max, .Feed.SogliaValore, .Feed.SogliaMin & " - " & .Feed.SogliaMax)
            If .Feed.Valore < .Feed.Min Or .Feed.Valore > .Feed.Max Then
                TaraturaFoto = "KO"
                tabella.Item(1, 3).Style.BackColor = ArcaColor.Arancio
            End If
            If Trasporto.tipoController = ENGICAM Then
                If .Feed.SogliaValore < .Feed.SogliaMin Or .Feed.SogliaValore > .Feed.SogliaMax Then
                    TaraturaFoto = "KO"
                    tabella.Item(3, 3).Style.BackColor = ArcaColor.Arancio
                End If
            End If
            xmlLog.AddTest("fFeedAdj", Log.Result, TaraturaFoto, "value", .Feed.Valore, "threshold", .Feed.SogliaValore, "ambient", .Feed.AmbienteValore)

            'tabella.Rows.Add("C1", .C1.Valore, .C1.Min & " - " & .C1.Max, .C1.SogliaValore, .C1.SogliaMin & " - " & .C1.SogliaMax)
            If .C1.Valore < .C1.Min Or .C1.Valore > .C1.Max Then
                TaraturaFoto = "KO"
                tabella.Item(1, 4).Style.BackColor = ArcaColor.Arancio
            End If
            If Trasporto.tipoController = ENGICAM Then
                If .C1.SogliaValore < .C1.SogliaMin Or .C1.SogliaValore > .C1.SogliaMax Then
                    TaraturaFoto = "KO"
                    tabella.Item(3, 4).Style.BackColor = ArcaColor.Arancio
                End If
            End If
            xmlLog.AddTest("fC1Adj", Log.Result, TaraturaFoto, "value", .C1.Valore, "threshold", .C1.SogliaValore, "ambient", .C1.AmbienteValore)

            'tabella.Rows.Add("SHIFT", .Shift.Valore, .Shift.Min & " - " & .Shift.Max, .Shift.SogliaValore, .Shift.SogliaMin & " - " & .Shift.SogliaMax)
            If .Shift.Valore < .Shift.Min Or .Shift.Valore > .Shift.Max Then
                TaraturaFoto = "KO"
                tabella.Item(1, 5).Style.BackColor = ArcaColor.Arancio
            End If
            If Trasporto.tipoController = ENGICAM Then
                If .Shift.SogliaValore < .Shift.SogliaMin Or .Shift.SogliaValore > .Shift.SogliaMax Then
                    TaraturaFoto = "KO"
                    tabella.Item(3, 5).Style.BackColor = ArcaColor.Arancio
                End If
            End If
            xmlLog.AddTest("fShiftAdj", Log.Result, TaraturaFoto, "value", .Shift.Valore, "threshold", .Shift.SogliaValore, "ambient", .Shift.AmbienteValore)

            'tabella.Rows.Add("INQ", .InQ.Valore, .InQ.Min & " - " & .InQ.Max, .InQ.SogliaValore, .InQ.SogliaMin & " - " & .InQ.SogliaMax)
            If .InQ.Valore < .InQ.Min Or .InQ.Valore > .InQ.Max Then
                TaraturaFoto = "KO"
                tabella.Item(1, 6).Style.BackColor = ArcaColor.Arancio
            End If
            If Trasporto.tipoController = ENGICAM Then
                If .InQ.SogliaValore < .InQ.SogliaMin Or .InQ.SogliaValore > .InQ.SogliaMax Then
                    TaraturaFoto = "KO"
                    tabella.Item(3, 6).Style.BackColor = ArcaColor.Arancio
                End If
            End If
            xmlLog.AddTest("fInqAdj", Log.Result, TaraturaFoto, "value", .InQ.Valore, "threshold", .InQ.SogliaValore, "ambient", .InQ.AmbienteValore)

            'tabella.Rows.Add("COUNT", .Count.Valore, .Count.Min & " - " & .Count.Max, .Count.SogliaValore, .Count.SogliaMin & " - " & .Count.SogliaMax)
            If .Count.Valore < .Count.Min Or .Count.Valore > .Count.Max Then
                TaraturaFoto = "KO"
                tabella.Item(1, 7).Style.BackColor = ArcaColor.Arancio
            End If
            If Trasporto.tipoController = ENGICAM Then
                If .Count.SogliaValore < .Count.SogliaMin Or .Count.SogliaValore > .Count.SogliaMax Then
                    TaraturaFoto = "KO"
                    tabella.Item(3, 7).Style.BackColor = ArcaColor.Arancio
                End If
            End If
            xmlLog.AddTest("fCountAdj", Log.Result, TaraturaFoto, "value", .Count.Valore, "threshold", .Count.SogliaValore, "ambient", .Count.AmbienteValore)

            'tabella.Rows.Add("OUT", .Out.Valore, .Out.Min & " - " & .Out.Max, .Out.SogliaValore, .Out.SogliaMin & " - " & .Out.SogliaMax)
            If .Out.Valore < .Out.Min Or .Out.Valore > .Out.Max Then
                TaraturaFoto = "KO"
                tabella.Item(1, 8).Style.BackColor = ArcaColor.Arancio
            End If
            If Trasporto.tipoController = ENGICAM Then
                If .Out.SogliaValore < .Out.SogliaMin Or .Out.SogliaValore > .Out.SogliaMax Then
                    TaraturaFoto = "KO"
                    tabella.Item(3, 8).Style.BackColor = ArcaColor.Arancio
                End If
            End If
            xmlLog.AddTest("fOutAdj", Log.Result, TaraturaFoto, "value", .Out.Valore, "threshold", .Out.SogliaValore, "ambient", .Out.AmbienteValore)

            'tabella.Rows.Add("C3", .C3.Valore, .C3.Min & " - " & .C3.Max, .C3.SogliaValore, .C3.SogliaMin & " - " & .C3.SogliaMax)
            If .C3.Valore < .C3.Min Or .C3.Valore > .C3.Max Then
                TaraturaFoto = "KO"
                tabella.Item(1, 9).Style.BackColor = ArcaColor.Arancio
            End If
            If Trasporto.tipoController = ENGICAM Then
                If .C3.SogliaValore < .C3.SogliaMin Or .C3.SogliaValore > .C3.SogliaMax Then
                    TaraturaFoto = "KO"
                    tabella.Item(3, 9).Style.BackColor = ArcaColor.Arancio
                End If
            End If
            xmlLog.AddTest("fC3Adj", Log.Result, TaraturaFoto, "value", .C3.Valore, "threshold", .C3.SogliaValore, "ambient", .C3.AmbienteValore)

            'tabella.Rows.Add("C4A", .C4A.Valore, .C4A.Min & " - " & .C4A.Max, .C4A.SogliaValore, .C4A.SogliaMin & " - " & .C4A.SogliaMax)
            If .C4A.Valore < .C4A.Min Or .C4A.Valore > .C4A.Max Then
                TaraturaFoto = "KO"
                tabella.Item(1, 10).Style.BackColor = ArcaColor.Arancio
            End If
            If Trasporto.tipoController = ENGICAM Then
                If .C4A.SogliaValore < .C4A.SogliaMin Or .C4A.SogliaValore > .C4A.SogliaMax Then
                    TaraturaFoto = "KO"
                    tabella.Item(3, 10).Style.BackColor = ArcaColor.Arancio
                End If
            End If
            xmlLog.AddTest("fC4aAdj", Log.Result, TaraturaFoto, "value", .C4A.Valore, "threshold", .C4A.SogliaValore, "ambient", .C4A.AmbienteValore)

            'tabella.Rows.Add("C4B", .C4B.Valore, .C4B.Min & " - " & .C4B.Max, .C4B.SogliaValore, .C4B.SogliaMin & " - " & .C4B.SogliaMax)
            If .C4B.Valore < .C4B.Min Or .C4B.Valore > .C4B.Max Then
                TaraturaFoto = "KO"
                tabella.Item(1, 11).Style.BackColor = ArcaColor.Arancio
            End If
            If Trasporto.tipoController = ENGICAM Then
                If .C4B.SogliaValore < .C4B.SogliaMin Or .C4B.SogliaValore > .C4B.SogliaMax Then
                    TaraturaFoto = "KO"
                    tabella.Item(3, 11).Style.BackColor = ArcaColor.Arancio
                End If
            End If
            xmlLog.AddTest("fC4bAdj", Log.Result, TaraturaFoto, "value", .C4B.Valore, "threshold", .C4B.SogliaValore, "ambient", .C4B.AmbienteValore)

            'tabella.Rows.Add("REJECT", .Rej.Valore, .Rej.Min & " - " & .Rej.Max, .Rej.SogliaValore, .Rej.SogliaMin & " - " & .Rej.SogliaMax)
            If .Rej.Valore < .Rej.Min Or .Rej.Valore > .Rej.Max Then
                TaraturaFoto = "KO"
                tabella.Item(1, 12).Style.BackColor = ArcaColor.Arancio
            End If
            If Trasporto.tipoController = ENGICAM Then
                If .Rej.SogliaValore < .Rej.SogliaMin Or .Rej.SogliaValore > .Rej.SogliaMax Then
                    TaraturaFoto = "KO"
                    tabella.Item(3, 12).Style.BackColor = ArcaColor.Arancio
                End If
            End If
            xmlLog.AddTest("fRejectAdj", Log.Result, TaraturaFoto, "value", .Rej.Valore, "threshold", .Rej.SogliaValore, "ambient", .Rej.AmbienteValore)

            If Trasporto.CRM = True Then
                'tabella.Rows.Add("INBOX", .InBox.Valore, .InBox.Min & " - " & .InBox.Max, .InBox.SogliaValore, .InBox.SogliaMin & " - " & .InBox.SogliaMax)
                If .InBox.Valore < .InBox.Min Or .InBox.Valore > .InBox.Max Then
                    TaraturaFoto = "KO"
                    tabella.Item(1, 13).Style.BackColor = ArcaColor.Arancio
                End If
                If Trasporto.tipoController = ENGICAM Then
                    If .InBox.SogliaValore < .InBox.SogliaMin Or .InBox.SogliaValore > .InBox.SogliaMax Then
                        TaraturaFoto = "KO"
                        tabella.Item(3, 13).Style.BackColor = ArcaColor.Arancio
                    End If
                End If
                xmlLog.AddTest("fInBoxAdj", Log.Result, TaraturaFoto, "value", .InBox.Valore, "threshold", .InBox.SogliaValore, "ambient", .InBox.AmbienteValore)
            End If


        End With
fineTaratura:
        tabella.AutoSize = True
        tabella.Width = tabella.Columns.GetColumnsWidth(0) + tabella.Columns.GetColumnsWidth(1) + tabella.Columns.GetColumnsWidth(2) + tabella.Columns.GetColumnsWidth(3) + tabella.Columns.GetColumnsWidth(4) + tabella.Columns.GetColumnsWidth(5) + tabella.Columns.GetColumnsWidth(6)

        If TaraturaFoto = "KO" Then
            Interfaccia("errore")
            ScriviMessaggio(Messaggio(212), lblMessaggio) 'Taratura foto non riuscita correttamente. Vedi tabella. Premi un tasto per ripetere, ESC per uscire
            Select Case AttendiTasto()
                Case "ESC"
                    tabella.Visible = False
                    Exit Function
                Case Else
                    GoTo taraFoto
            End Select
        End If
        ScriviMessaggio("Taratura eseguita correttamente. Premi un tasto per continuare, R per ripetere la taratura", lblMessaggio)
        Select Case AttendiTasto()
            Case "R"
                GoTo taraFoto
        End Select
        tabella.Rows.Clear()
        tabella.Visible = False
    End Function



    Function ImpostaMacAddress() As String
        If statoConnessione = ConnectionState.TransparentMode Then CmdTransparentOut()
macaddress:
        ScriviMessaggio(Messaggio(114), lblMessaggio) 'Richiesta MAC Address in corso
        ImpostaMacAddress = CmdGetMacAddress()
        If ImpostaMacAddress <> "OK" Then
            Interfaccia("errore")
            ScriviMessaggio(Messaggio(259), lblMessaggio, Color.Red) 'Non e' stato possibile leggere l'indirizzo MAC, premere un tasto per ripetere, ESC per uscire
            Select Case AttendiTasto()
                Case "ESC"
                    Exit Function
                Case Else
                    GoTo macaddress
            End Select
        End If
        Trasporto.MacAddress = comandoSingoloRisposta(5)
        xmlLog.AddTestAttribute("setMacAddress", "address", Trasporto.MacAddress)
        If Trasporto.MacAddress.Substring(Trasporto.MacAddress.Length - 4) <> Trasporto.MatricolaController.Substring(Trasporto.MatricolaController.Length - 4, 4) Then
            Dim fixedMacAddress As String = ""
            Select Case Trasporto.tipoController
                Case PXA
                    fixedMacAddress = "000E7281"
                Case ENGICAM
                    fixedMacAddress = "000E7282"
                Case Else
                    ScriviMessaggio(Messaggio(118), lblMessaggio) ' Non sono stato in grado di riconoscere il Controller. Premi 'P' se PXA, qualunque altro tasto se ENGICAM o ESC per uscire.
                    AttendiTasto()
                    Select Case tastoPremuto
                        Case 80, 112
                            fixedMacAddress = "000E7281"
                        Case 27
                            ImpostaMacAddress = "FAIL"
                            GoTo fine
                        Case Else
                            fixedMacAddress = "000E7282"
                    End Select
            End Select
            ScriviMessaggio(String.Format(Messaggio(68), fixedMacAddress & Trasporto.MatricolaController.Substring(Trasporto.MatricolaController.Length - 4, 4)), lblMessaggio) 'Impostazione MAC ADDRESS {0} in corso.
            ImpostaMacAddress = CmdSetMacAddress(fixedMacAddress & Trasporto.MatricolaController.Substring(Trasporto.MatricolaController.Length - 4, 4))
            GoTo macaddress
        End If
        'Trasporto.MacAddress = "000E7281" & Trasporto.MatricolaController.Substring(Trasporto.MatricolaController.Length - 4, 4)
        'ScriviMessaggio(String.Format(Messaggio(68), Trasporto.MacAddress), lblMessaggio) 'Impostazione MAC ADDRESS {0} in corso.
        'ImpostaMacAddress = CmdSetMacAddress(Trasporto.MacAddress)
fine:
        xmlLog.AddTest("setMacAddress", Log.Result, ImpostaMacAddress)
        'ScriviMessaggio(Messaggio(69), lblMessaggio)
        'cmdReset()

        'ImpostaMacAddress = cmdComandoSingolo("T,1,6,14", 5, 5000)
        'If TestAlimentatore() <> "OK" Then Exit Function
        'Trasporto.MacAddress = comandoSingoloRisposta(6)
    End Function

    Function TestAlimentatore() As String
test:
        Dim fwTest As Boolean = False
        Dim vAliOff As Single = testValuesIni.ReadValue("Alimentatore", "vAliOff")
        Dim vAliMin As Single = testValuesIni.ReadValue("Alimentatore", "vAliMin")
        Dim vAliMax As Single = testValuesIni.ReadValue("Alimentatore", "vAliMax")
        Dim attesa As Integer = testValuesIni.ReadValue("Alimentatore", "Attesa")
        ScriviMessaggio(Messaggio(70), lblMessaggio) 'Lettura versione del Controller
        cmdGetVersion(2)
        TestAlimentatore = LeggiRc(myGetVersionReply.rc,, "CM20")
        If TestAlimentatore = "Failed" Then
            TestAlimentatore = LeggiRc(myGetVersionReply.rc,, )
        End If
        If TestAlimentatore <> "OK" Then
            Exit Function
        End If
        If myGetVersionReply.Ver <> "T_00" And controllerType <> "NEW" Then
            TestAlimentatore = "OK"
            fwTest = False
            Trasporto.AliVOff = -1
            Trasporto.AliVonMotOff = -1
            Trasporto.AliVonMotOn = -1
            Exit Function
        End If
        If Val("&h" & myGetVersionReply.Rel) < 13 And controllerType <> "NEW" Then
            ScriviMessaggio(Messaggio(208), lblMessaggio) 'La release del firmware non è corretta, impossibile verificare la tensione. Premere un tasto per continuare.
            AttendiTasto()
            fwTest = False
            TestAlimentatore = "RelOld"
            Exit Function
        End If

        ScriviMessaggio(Messaggio(14), lblMessaggio)
        CmdTransparentIn()

        tabella.Visible = True
        tabella.ColumnCount = 3
        tabella.Rows.Clear()
        tabella.Columns(0).Name = "TEST"
        tabella.Columns(1).Name = "VALUE"
        tabella.Columns(2).Name = "RANGE"
        tabella.RowHeadersVisible = False
        tabella.Enabled = False

        'DISATTIVATO TEMPORANEAMENTE ALCUNI PC TORNANO 30303008  !!!
#If 0 Then
        'Lettura tensione senza potenza
        ScriviMessaggio(Messaggio(15), lblMessaggio)
        tcPowerOff()
        'Aspetta(1000)
        Aspetta(2000)  'DAVIDE
        tcGetPowerAdValue() 'Get Power AD value - risp: 3x3x3x3x
        'Try
        Trasporto.AliVOff = Format(CDbl((AsciCode2AsciChar(TransparentAnswer) / 3.15) / 10), "0.00")
        'TransparentAnswer = "30303008"
        'Trasporto.AliVOff = Format(CDbl((AsciCode2AsciChar(TransparentAnswer) / 3.15) / 10), "0.00")
        'Catch ex As Exception
        'Trasporto.AliVOff = -1
        'End Try
        tabella.Rows.Add(Messaggio(11), Trasporto.AliVOff, "<" & vAliOff)
        tabella.Width = tabella.Columns(0).Width + tabella.Columns(1).Width + tabella.Columns(2).Width
        tabella.Height = tabella.RowCount * 20 + 40
        'tabella.Height = tabella.PreferredSize.Height
        If Trasporto.AliVOff >= vAliOff Then
            tabella.Item(1, 0).Style.BackColor = ArcaColor.Arancio
            TestAlimentatore = "AliVoffKO"
            Exit Function
        Else
            tabella.Item(1, 0).Style.BackColor = Color.White
        End If
#End If

        'Attivazione potenza
        ScriviMessaggio(Messaggio(16), lblMessaggio) 'Attivazione POWER ON.
        tcPowerOn()
        'Aspetta(attesa)

        'Verifica tensione con potenza attiva e motore spento
        ScriviMessaggio(Messaggio(17), lblMessaggio) 'Lettura tensione ad alimentazione attiva e motore SPENTO.
        tcGetPowerAdValue()
        If (controllerType = "NEW") Then
            Trasporto.AliVonMotOff = Format(CDbl((AsciCode2AsciChar(TransparentAnswer) / 2.81) / 10), "0.00")
        Else
            Trasporto.AliVonMotOff = Format(CDbl((AsciCode2AsciChar(TransparentAnswer) / 3.15) / 10), "0.00")
        End If
        tabella.Rows.Add(Messaggio(12), Trasporto.AliVonMotOff, ">" & vAliMin & " - <" & vAliMax)
        tabella.Width = tabella.Columns(0).Width + tabella.Columns(1).Width + tabella.Columns(2).Width
        tabella.Height = tabella.RowCount * 20 + 40
        'tabella.Height = tabella.PreferredSize.Height
        If Trasporto.AliVonMotOff < vAliMin Or Trasporto.AliVonMotOff > vAliMax Then
            tabella.Item(1, 1).Style.BackColor = ArcaColor.Arancio
            TestAlimentatore = "AliVonMotOffKO"
            Exit Function
        Else
            tabella.Item(1, 1).Style.BackColor = Color.White
        End If

        'Far girare il trasporto orizzontale (comando D211)
        ScriviMessaggio(Messaggio(18), lblMessaggio) 'Attivazione motore trasporto orizzontale.
        tcMSOOutput()
        Aspetta(attesa)

        'Verifica tensione con motore acceso
        ScriviMessaggio(Messaggio(19), lblMessaggio) 'Lettura tensione ad alimentazione attiva e motore ACCESO
        tcGetPowerAdValue()
        If (controllerType = "NEW") Then
            Trasporto.AliVonMotOn = Format(CDbl((AsciCode2AsciChar(TransparentAnswer) / 2.81) / 10), "0.00")
        Else
            Trasporto.AliVonMotOn = Format(CDbl((AsciCode2AsciChar(TransparentAnswer) / 3.15) / 10), "0.00")
        End If
        tabella.Rows.Add(Messaggio(13), Trasporto.AliVonMotOn, ">" & vAliMin & " - <" & vAliMax)
        tabella.Width = tabella.Columns(0).Width + tabella.Columns(1).Width + tabella.Columns(2).Width
        tabella.Height = tabella.RowCount * 20 + 40
        'tabella.Height = tabella.PreferredSize.Height
        tcMSOOff()
        Aspetta(200)
        tcPowerOff()
        Aspetta(200)
        ScriviMessaggio(Messaggio(20), lblMessaggio) 'Disattivazione TRANSPARENT mode.
        CmdTransparentOut()
        tabella.Width = tabella.Columns(0).Width + tabella.Columns(1).Width + tabella.Columns(2).Width
        If Trasporto.AliVonMotOn < vAliMin Or Trasporto.AliVonMotOn > vAliMax Then
            tabella.Item(1, 2).Style.BackColor = ArcaColor.Arancio
            TestAlimentatore = "AliVonMotOnKO"
        Else
            tabella.Item(1, 2).Style.BackColor = Color.White
        End If

        If TestAlimentatore <> "OK" Then
            Interfaccia("errore ")
            ScriviMessaggio(Messaggio(227), lblMessaggio) 'Test ALIMENTATORE non andato a buon fine, premere un tasto per ripetere, ESC per uscire.
            Select Case AttendiTasto()
                Case "ESC"
                Case Else
                    GoTo test
            End Select
        End If
        tabella.ClearSelection()
        Aspetta(2000)
        tabella.Rows.Clear()
        tabella.Visible = False
    End Function

    Function EsaminaChecklist() As String
        EsaminaChecklist = ""
        lstChecklist.Items.Clear()
        lstChecklist.DrawMode = DrawMode.OwnerDrawFixed

        Dim checklistFile As String = localFolder & "\" & configIni.ReadValue("Checklist", "nomeFile")
        Dim checklistIni As New INIFile(checklistFile)
        Dim nCheck As Integer = 1
        Dim titolo As String = checklistIni.ReadValue("Check" & nCheck, "titolo")

        Do While titolo <> "Failed"
            Dim listItem As New myListItem
            listItem.Testo = titolo
            listItem.Colore = Color.LightGray
            lstChecklist.Items.Add(listItem)
            nCheck += 1
            titolo = checklistIni.ReadValue("Check" & nCheck, "titolo")
        Loop
        lstChecklist.Height = (lstChecklist.Items.Count + 1) * lstChecklist.ItemHeight
        lstChecklist.Visible = True

        Aspetta(100)
        CmdTransparentIn()
        If statoConnessione <> ConnectionState.TransparentMode Then
            EsaminaChecklist = "KO"
            Exit Function
        End If
        'Aspetta(100)
        'tcPowerOn()
        'Aspetta(100)

        nCheck = 1
        checkListStep = CaricaTestChecklist(nCheck, checklistIni)

        Do While checkListStep.Titolo <> "Failed"
            xmlLog.AddTest("CHK" & checkListStep.Titolo.Replace(" ", ""), Log.Result, "false")

            If checkListStep.comando_entrata <> "Failed" Then
                CmdTransparentCommand(checkListStep.comando_entrata, 1000)
            End If
            Do While Not chkValore > 0
                Application.DoEvents()
            Loop
            tcPowerOff()
            If checkListStep.comando_uscita <> "Failed" Then
                CmdTransparentCommand(checkListStep.comando_uscita, 1000)
            End If
            If chkValore = 2 Then
                CType(lstChecklist.Items(nCheck - 1), myListItem).Colore = Color.Red
                lstChecklist.Refresh()
                Interfaccia("errore")
                ScriviMessaggio(Messaggio(207), lblMessaggio)
                cmdComando1.Visible = False
                cmdComando2.Visible = False
                cmdComando3.Visible = False
                cmdComando4.Visible = False
                chkOk.Visible = False
                chkFault.Visible = False
                AttendiTasto()
                lstChecklist.Visible = False
                CmdTransparentOut()
                Exit Function
            End If

            xmlLog.AddTestAttribute("CHK" & checkListStep.Titolo.Replace(" ", ""), Log.Result, "OK")
            CType(lstChecklist.Items(nCheck - 1), myListItem).Colore = Color.Green
            lstChecklist.Refresh()
            Aspetta(100)

            nCheck += 1
            checkListStep = CaricaTestChecklist(nCheck, checklistIni)
        Loop

        Aspetta(100)
        'tcPowerOff()
        CmdTransparentOut()
        cmdComando1.Visible = False
        cmdComando2.Visible = False
        cmdComando3.Visible = False
        cmdComando4.Visible = False
        chkOk.Visible = False
        chkFault.Visible = False
        lstChecklist.Visible = False
        EsaminaChecklist = "OK"
        Trasporto.checklist = "OK"
    End Function

    Function CaricaTestChecklist(ByVal numeroTest As Integer, ByVal checklistIni As INIFile) As Schecklist
        CaricaTestChecklist = Nothing
        Dim comando As String = ""
        Dim buttonSplit() As String
        CaricaTestChecklist.Titolo = checklistIni.ReadValue("Check" & numeroTest, "titolo")
        If CaricaTestChecklist.Titolo <> "Failed" Then
            CaricaTestChecklist.Stringa = checklistIni.ReadValue("Check" & numeroTest, "Label")
            ScriviMessaggio(CaricaTestChecklist.Stringa, lblMessaggio)
            CaricaTestChecklist.potenza = checklistIni.ReadValue("Check" & numeroTest, "potenza")
            CaricaTestChecklist.comando_uscita = checklistIni.ReadValue("Check" & numeroTest, "uscita")
            CaricaTestChecklist.comando_entrata = checklistIni.ReadValue("Check" & numeroTest, "entrata")
            comando = checklistIni.ReadValue("Check" & numeroTest, "Comando1")
            buttonSplit = comando.Split(",")
            If buttonSplit.Length > 1 Then
                cmdComando1.Visible = True
                cmdComando1.ForeColor = ArcaColor.GrigioChiaro
                cmdComando1.Text = buttonSplit(1)
                CaricaTestChecklist.Button1.Comando = buttonSplit(0).Trim
                CaricaTestChecklist.Button1.Titolo = buttonSplit(1).Trim
            Else
                cmdComando1.Visible = False
            End If

            comando = checklistIni.ReadValue("Check" & numeroTest, "Comando2")
            buttonSplit = comando.Split(",")
            If buttonSplit.Length > 1 Then
                cmdComando2.Visible = True
                cmdComando2.ForeColor = ArcaColor.GrigioChiaro
                cmdComando2.Text = buttonSplit(1)
                CaricaTestChecklist.Button2.Comando = buttonSplit(0).Trim
                CaricaTestChecklist.Button2.Titolo = buttonSplit(1).Trim
            Else
                cmdComando2.Visible = False
            End If

            comando = checklistIni.ReadValue("Check" & numeroTest, "Comando3")
            buttonSplit = comando.Split(",")
            If buttonSplit.Length > 1 Then
                cmdComando3.Visible = True
                cmdComando3.ForeColor = ArcaColor.GrigioChiaro
                cmdComando3.Text = buttonSplit(1)
                CaricaTestChecklist.Button3.Comando = buttonSplit(0).Trim
                CaricaTestChecklist.Button3.Titolo = buttonSplit(1).Trim
            Else
                cmdComando3.Visible = False
            End If

            comando = checklistIni.ReadValue("Check" & numeroTest, "Comando4")
            buttonSplit = comando.Split(",")
            If buttonSplit.Length > 1 Then
                cmdComando4.Visible = True
                cmdComando4.ForeColor = ArcaColor.GrigioChiaro
                cmdComando4.Text = buttonSplit(1)
                CaricaTestChecklist.Button4.Comando = buttonSplit(0).Trim
                CaricaTestChecklist.Button4.Titolo = buttonSplit(1).Trim
            Else
                cmdComando4.Visible = False
            End If

            chkOk.Checked = False
            chkOk.Visible = False
            chkFault.Checked = False
            chkFault.Visible = False
            chkValore = 0
            cmdComando1.Select()
            Refresh()

            If cmdComando1.Visible = False And cmdComando2.Visible = False And cmdComando3.Visible = False And cmdComando4.Visible = False Then
                myTimer.Interval = configIni.ReadValue("Varie", "DurataVerifica")
                myTimer.Start()
            End If
        Else
            cmdComando1.Visible = False
            cmdComando2.Visible = False
            cmdComando3.Visible = False
            cmdComando4.Visible = False
            chkOk.Checked = False
            chkOk.Visible = False
        End If



    End Function

    Function ImpostaDataOra() As String
        Dim mese, giorno, ora, minuti As Integer
        mese = Format(Now, "MM")
        giorno = Format(Now, "dd")
        ora = Format(Now, "HH")
        minuti = Format(Now, "mm")

        ImpostaDataOra = cmdComandoSingolo("F,1,0," & mese & "," & giorno & "," & ora & "," & minuti, 3, 5000)

    End Function

    Function InserimentoMatricolaController() As String
        InserimentoMatricolaController = "OK"
inserimento:
        Interfaccia("Normale")
        txtMatricola.Enabled = True
        txtMatricola.Visible = True
        txtMatricola.MaxLength = 16
        txtMatricola.Text = ""
        txtMatricola.Focus()
        ScriviMessaggio(String.Format(Messaggio(10), txtMatricola.MaxLength), lblMessaggio)
        AttendiTasto("INVIO")
        txtMatricola.Enabled = False
        If txtMatricola.TextLength <> 16 Then
            Interfaccia("errore")
            ScriviMessaggio(Messaggio(206), lblMessaggio)
            Select Case AttendiTasto()
                Case "ESC"
                    InserimentoMatricolaController = Messaggio(302) 'MatricolaControllerKO
                    Exit Function
            End Select
            GoTo inserimento
        End If
        controllerType = "OLD"
        If txtMatricola.Text.Substring(1, 5) = "13353" Then controllerType = "NEW" '13396
        Trasporto.MatricolaController = UCase(txtMatricola.Text)
        txtMatricola.Visible = False
    End Function

    Function InserimentoMatricola() As String
        InserimentoMatricola = "OK"
inserimento:
        Interfaccia("Normale")
        txtMatricola.Enabled = True
        txtMatricola.Visible = True
        txtMatricola.MaxLength = 12
        txtMatricola.Text = ""
        txtMatricola.Focus()
        ScriviMessaggio(String.Format(Messaggio(9), txtMatricola.MaxLength), lblMessaggio)
        AttendiTasto("INVIO")
        txtMatricola.Enabled = False
        If txtMatricola.TextLength <> 12 Then
            Interfaccia("errore")
            ScriviMessaggio(Messaggio(205), lblMessaggio)
            Select Case AttendiTasto()
                Case "ESC"
                    InserimentoMatricola = Messaggio(301) 'MatricolaTrasportoKO
                    Exit Function
            End Select
            GoTo inserimento
        End If
        Trasporto.Matricola = UCase(txtMatricola.Text)
        txtMatricola.Visible = False
    End Function

    Function InserimentoCodiceTrasporto() As String
        VisualizzaControlli(False)
        InserimentoCodiceTrasporto = "OK"
inserimento:
        Interfaccia("Normale")
        txtMatricola.Enabled = True
        txtMatricola.Visible = True
        txtMatricola.MaxLength = 9
        txtMatricola.Text = ""
        txtMatricola.Focus()
        ScriviMessaggio(String.Format(Messaggio(8), txtMatricola.MaxLength), lblMessaggio)
        AttendiTasto("INVIO")
        txtMatricola.Enabled = False
        If txtMatricola.TextLength <> 9 And txtMatricola.TextLength <> 5 Then
            Interfaccia("errore")
            ScriviMessaggio(Messaggio(205), lblMessaggio)
            Select Case AttendiTasto()
                Case "ESC"
                    InserimentoCodiceTrasporto = Messaggio(300) 'CodiceTrasportoKO
                    Exit Function
            End Select
            GoTo inserimento
        End If

        For i = 0 To 4
            If Asc(txtMatricola.Text.Substring(i, 1)) < 48 Or Asc(txtMatricola.Text.Substring(i, 1)) > 57 Then
                Interfaccia("errore")
                ScriviMessaggio(Messaggio(204), lblMessaggio)
                Select Case AttendiTasto()
                    Case "ESC"
                        InserimentoCodiceTrasporto = Messaggio(300) 'CodiceTrasportoKO
                        Exit Function
                End Select
                GoTo inserimento
            End If
        Next
        Trasporto.Codice = UCase(txtMatricola.Text)
        txtMatricola.Visible = False
    End Function

    Sub CaricaImpostazioneConnessione(Optional connessione As String = "")
        If connessione = "" Then connessione = tipoConnessione
        Select Case connessione
            Case "RS232"
                With ConnectionParam
                    .ConnectionMode = DLINKMODESERIAL
                    .RsConf.device = comPort
                    .RsConf.baudrate = comBaudRate
                    .RsConf.parity = IIf(comParity = "NONE", 0, 1)
                    .RsConf.car = comDataBits
                    .RsConf.stopbit = comStopbits
                End With
            Case "RS232EASY"
                With ConnectionParam
                    .ConnectionMode = DLINKMODESERIALEASY
                    .RsConf.device = comPort
                    .RsConf.baudrate = comBaudRate
                    .RsConf.parity = IIf(comParity = "NONE", 0, 1)
                    .RsConf.car = comDataBits
                    .RsConf.stopbit = comStopbits
                End With
            Case "USB"
                ConnectionParam.ConnectionMode = DLINKMODEUSB
            Case "USBEASY"
                ConnectionParam.ConnectionMode = DLINKMODEUSBEASY
            Case "LAN"
                With ConnectionParam
                    .ConnectionMode = DLINKMODETCPIP
                    .TcpIpPar.clientIpAddr = lanIPAddress
                    .TcpIpPar.portNumber = lanPort
                End With
        End Select
        myToolTip.SetToolTip(Me.lstComScope, connessione)
    End Sub

    'Private Sub frmPrincipale_KeyUp(sender As Object, e As KeyEventArgs) Handles Me.KeyUp
    '    tastoPremuto = e.KeyCode
    'End Sub

    Private Sub frmPrincipale_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        Dim ip As String = String.Empty
        Dim myIps() As Net.IPAddress = Net.Dns.GetHostAddresses(Net.Dns.GetHostName)
        For Each myIp As Net.IPAddress In myIps
            If myIp.AddressFamily = AddressFamily.InterNetwork Then
                ip = myIp.ToString()
                Exit For
            End If
        Next
        SetThreadExecutionState(EXECUTION_STATE.ES_DISPLAY_REQUIRED Or EXECUTION_STATE.ES_CONTINUOUS)

        Text = Messaggio(1) & " - " & Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString & " TeamViewer ID: " & System.Net.Dns.GetHostName & " IpAddress: " & ip
        VisualizzaControlli(False)
        If Cerca7Zip() <> "OK" Then
            End
        End If
        If CopiaDB() <> "OK" Then
            'gestione errori
        End If
        cboIdProdotto.Visible = True
        cboCliente.Visible = True
        cboIdProdotto.Focus()
        ScriviMessaggio(Messaggio(7), lblMessaggio)
        If PopolaComboIdProdotto() <> "OK" Then

            'gestione errori
        End If

        CaricaImpostazioneConnessione()

    End Sub

    Private Sub frmPrincipale_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        'dimensioni orizzontali
        lblMessaggio.Width = Me.Width * 0.9
        picLogo.Width = Me.Width * 0.04
        picLogo.Height = picLogo.Width * 1.135
        cboIdProdotto.Width = Me.Width * 0.11
        cboCliente.Width = Me.Width * 0.39
        lstChecklist.Width = Me.Width * 0.2
        cmdComando1.Width = Me.Width * 0.16
        cmdComando2.Width = Me.Width * 0.16
        cmdComando3.Width = Me.Width * 0.16
        cmdComando4.Width = Me.Width * 0.16
        lstComScope.Width = Me.Width * 0.2
        txtMatricola.Width = Me.Width * 0.24
        trkShift.Width = Me.Width * 0.39
        cmdPrincipale.Width = Me.Width * 0.16
        cmdSx.Width = Me.Width * 0.11
        cmdDx.Width = Me.Width * 0.11
        lblShiftCenter.Width = Me.Width * 0.16
        cmdShiftKo.Width = Me.Width * 0.16
        cmdShiftOk.Width = Me.Width * 0.16
        pgbDownload.Width = Me.Width * 0.64
        tabella.Width = Me.Width * 0.38
        chkFault.Width = Me.Width * 0.1
        chkOk.Width = Me.Width * 0.1
        lblMacchina.Width = Me.Width * 0.6

        'posizioni orizzontali
        lblMessaggio.Left = 12
        picLogo.Left = Me.Width - (picLogo.Width + 40)
        cboIdProdotto.Left = Me.Width * 0.132
        cboCliente.Left = Me.Width * 0.248
        lstChecklist.Left = Me.Width * 0.756
        cmdComando1.Left = Me.Width * 0.033
        cmdComando2.Left = Me.Width * 0.033
        cmdComando3.Left = Me.Width * 0.033
        cmdComando4.Left = Me.Width * 0.033
        lstComScope.Left = Me.Width * 0.756
        txtMatricola.Left = Me.Width * 0.248
        trkShift.Left = Me.Width * 0.248
        cmdPrincipale.Left = Me.Width * 0.371
        cmdSx.Left = Me.Width * 0.252
        cmdDx.Left = Me.Width * 0.522
        lblShiftCenter.Left = Me.Width * 0.398
        cmdShiftKo.Left = Me.Width * 0.445
        cmdShiftOk.Left = Me.Width * 0.248
        pgbDownload.Left = Me.Width * 0.033
        tabella.Left = Me.Width * 0.252
        chkFault.Left = Me.Width * 0.445
        chkOk.Left = Me.Width * 0.248
        lblMacchina.Left = Me.Width * 0.25

        'dimensioni verticali
        lblMessaggio.Height = Me.Height * 0.154
        cboIdProdotto.Height = Me.Height * 0.06
        cboCliente.Height = Me.Height * 0.06
        lstChecklist.Height = Me.Height * 0.425
        cmdComando1.Height = Me.Height * 0.1
        cmdComando2.Height = Me.Height * 0.1
        cmdComando3.Height = Me.Height * 0.1
        cmdComando4.Height = Me.Height * 0.1
        lstComScope.Height = Me.Height * 0.2
        txtMatricola.Height = Me.Height * 0.06
        trkShift.Height = Me.Height * 0.069
        cmdPrincipale.Height = Me.Height * 0.307
        cmdSx.Height = Me.Height * 0.057
        cmdDx.Height = Me.Height * 0.057
        lblShiftCenter.Height = Me.Height * 0.057
        cmdShiftKo.Height = Me.Height * 0.1
        cmdShiftOk.Height = Me.Height * 0.1
        pgbDownload.Height = Me.Height * 0.049
        tabella.Height = Me.Height * 0.31
        chkFault.Height = Me.Height * 0.063
        chkOk.Height = Me.Height * 0.063

        'posizioni verticali
        lblMessaggio.Top = 34
        picLogo.Top = 34
        cboIdProdotto.Top = Me.Height * 0.215
        cboCliente.Top = Me.Height * 0.215
        lstChecklist.Top = Me.Height * 0.215
        cmdComando1.Top = Me.Height * 0.304
        cmdComando2.Top = Me.Height * 0.433
        cmdComando3.Top = Me.Height * 0.562
        cmdComando4.Top = Me.Height * 0.691
        lstComScope.Top = Me.Height * 0.215 + lstChecklist.Height
        txtMatricola.Top = Me.Height * 0.273
        trkShift.Top = Me.Height * 0.338
        cmdPrincipale.Top = Me.Height * 0.332
        cmdSx.Top = Me.Height * 0.389
        cmdDx.Top = Me.Height * 0.389
        lblShiftCenter.Top = Me.Height * 0.398
        cmdShiftKo.Top = Me.Height * 0.535
        cmdShiftOk.Top = Me.Height * 0.535
        pgbDownload.Top = Me.Height * 0.21
        tabella.Top = Me.Height * 0.273
        chkFault.Top = Me.Height * 0.473
        chkOk.Top = Me.Height * 0.473
        lblMacchina.Top = Me.Height - (lblMacchina.Height + 50)
    End Sub

    Private Sub frmPrincipale_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.WindowState = FormWindowState.Maximized
        Me.KeyPreview = True

        myToolTip.Active = True
        myToolTip.AutoPopDelay = 5000
        myToolTip.InitialDelay = 50
        myToolTip.ReshowDelay = 50
        myToolTip.ShowAlways = True
        myToolTip.SetToolTip(Me.lstComScope, tipoConnessione)
        Ripristina()


        If configIni.ReadValue("option", "DllTrace").ToUpper <> "N" Then
            trace = New STraceDirective
            ReDim trace.infoComp(3)
            trace.infoComp(0) = 0
            trace.infoComp(1) = 0
            trace.infoComp(2) = 0
            trace.infoComp(3) = 0
            trace.infoLayer = 1
            trace.lpComScope = 0
            Dim folder As String = "TraceTraspSup.txt"
            'trace.tracePathName = Encoding.ASCII.GetBytes(folder.PadRight(259, Chr(0)))
            ReDim trace.tracePathName(259)
            For i = 0 To folder.Length - 1
                trace.tracePathName(i) = Asc(folder.Substring(i, 1))
                trace.tracePathName(i + 1) = 0
            Next
            trace.trcNum = 30
            trace.trcSize = 10
            trace.trcType = 1

            Try
                Dim ret As Int16
                ret = CMCommandImport.SetTraceDirective(trace)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
                'End
            End Try
        End If
    End Sub

    Private Sub cboIdProdotto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboIdProdotto.SelectedIndexChanged
        cboCliente.Items.Clear()

        objConn.Open()
        sqlStringa = "SELECT DISTINCT nome_cliente FROM clienti, tblman WHERE tblman.id_prodotto LIKE '" & cboIdProdotto.Text & "' AND tblman.id_cliente = clienti.id_cliente"
        sqlCommand = New OleDbCommand(sqlStringa, objConn)
        dbReader = sqlCommand.ExecuteReader
        Do While Not dbReader.Read() = Nothing
            cboCliente.Items.Add(dbReader(0))
        Loop
        objConn.Close()

        cboCliente.Focus()
        If cboCliente.Items.Count = 1 Then
            cboCliente.SelectedIndex = 0
        End If

    End Sub

    Private Sub cboIdProdotto_TextChanged(sender As Object, e As EventArgs) Handles cboIdProdotto.TextChanged
        If Len(cboIdProdotto.Text) = 5 Then
            cboIdProdotto.SelectedIndex = cboIdProdotto.FindString(cboIdProdotto.Text)
        End If
    End Sub


    Private Sub cboCliente_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCliente.SelectedIndexChanged
        cboCliente.Enabled = False
        cboIdProdotto.Enabled = False

        Trasporto = New STraspSup

        ScriviMessaggio(Messaggio(6), lblMessaggio)
        Select Case AttendiTasto()
            Case "ESC"
                cboCliente.Enabled = True
                cboIdProdotto.Enabled = True
                cboIdProdotto.Text = ""
                cboCliente.Text = ""
                Exit Sub
        End Select

        Trasporto.MacchinaCodice = cboIdProdotto.Text
        Trasporto.ClienteNome = cboCliente.Text

        sqlStringa = "SELECT id_cliente FROM clienti where clienti.nome_cliente = '" & Trasporto.ClienteNome & "'"
        Trasporto.ClienteID = LeggiDB(sqlStringa)(0)

        sqlStringa = "SELECT DISTINCT prodotti.categoria_prodotto FROM prodotti where prodotti.id_prodotto = '" & Trasporto.MacchinaCodice & "'"
        Trasporto.Categoria = UCase(LeggiDB(sqlStringa)(0))
        lblMacchina.Text = Trasporto.MacchinaCodice & " - " & Trasporto.Categoria & "(" & Trasporto.ClienteNome & ")"
        Dim iCerca As Integer = InStr(Trasporto.Categoria, "CRM")
        If iCerca > 0 Then
            Trasporto.CRM = True
        End If

        iCerca = InStr(Trasporto.Categoria, "CD80")
        If iCerca > 0 Then
            Trasporto.CD80 = Mid(Trasporto.Categoria, iCerca - 1, 1) * 2
        End If

        iCerca = InStr(Trasporto.Categoria, " ")
        If iCerca > 0 Then
            Trasporto.Categoria = Mid(Trasporto.Categoria, 1, iCerca - 1)
        End If

        Trasporto.TestResult = TestTrasportoSuperiore()

salvataggio:
        Trasporto.fineData = Format(Now, "dd/MM/yy")
        Trasporto.fineOra = Format(Now, "HH:mm")
        If Trasporto.TestResult = "OK" Then Trasporto.TestResult = "PASS"
        SalvaLog()
        SalvaLogServer(localFolder & "\LogTrasportoSuperiore.txt")
        Ripristina()

    End Sub

    Private Sub Ripristina()
        Interfaccia("Normale")
        VisualizzaControlli(False)
        ScriviMessaggio(Messaggio(5), lblMessaggio)
        cboCliente.Visible = True
        cboIdProdotto.Visible = True
        picLogo.Visible = True
        lstComScope.Visible = False
        lblMacchina.Text = ""
        cboIdProdotto.Text = ""
        cboCliente.Text = ""
        cmdComando1.Text = ""
        cmdComando2.Text = ""
        cmdComando3.Text = ""
        cmdComando4.Text = ""
        lstComScope.Items.Clear()
        txtMatricola.Text = ""
        cmdPrincipale.Text = ""
        chkFault.Text = "FAULT"
        chkOk.Text = "OK"
        cboCliente.Enabled = True
        cboIdProdotto.Enabled = True
        tipoConnessione = configIni.ReadValue("Connection", "Connection")
    End Sub

    Private Sub chkOk_CheckedChanged(sender As Object, e As EventArgs) Handles chkOk.CheckedChanged
        chkValore = 1
    End Sub

    Private Sub chkFault_CheckedChanged(sender As Object, e As EventArgs) Handles chkFault.CheckedChanged
        chkValore = 2
    End Sub

    Private Sub myTimer_Tick(sender As Object, e As EventArgs) Handles myTimer.Tick
        VisualizzaCheckbox()
        myTimer.Stop()
    End Sub

    Sub VisualizzaCheckbox()
        chkFault.Visible = True
        chkOk.Visible = True
    End Sub

    Private Sub cmdComando1_Click(sender As Object, e As EventArgs) Handles cmdComando1.Click
        cmdComando1.Enabled = False
        If checkListStep.potenza.ToUpper() = "SI" Then
            tcPowerOff()
            Aspetta(100)
            tcPowerOn()
            Aspetta(100)
        End If
        CmdTransparentCommand(checkListStep.Button1.Comando, 1000)
        Aspetta(1000)
        VisualizzaCheckbox()
        Aspetta(500)
        cmdComando1.Enabled = True
    End Sub

    Private Sub cmdComando2_Click(sender As Object, e As EventArgs) Handles cmdComando2.Click
        cmdComando2.Enabled = False
        If checkListStep.potenza.ToUpper() = "SI" Then
            tcPowerOff()
            Aspetta(100)
            tcPowerOn()
            Aspetta(100)
        End If
        CmdTransparentCommand(checkListStep.Button2.Comando, 1000)
        Aspetta(1000)
        VisualizzaCheckbox()
        Aspetta(500)
        cmdComando2.Enabled = True
    End Sub

    Private Sub cmdComando3_Click(sender As Object, e As EventArgs) Handles cmdComando3.Click
        cmdComando3.Enabled = False
        If checkListStep.potenza.ToUpper() = "SI" Then
            tcPowerOff()
            Aspetta(100)
            tcPowerOn()
            Aspetta(100)
        End If
        CmdTransparentCommand(checkListStep.Button3.Comando, 1000)
        Aspetta(1000)
        VisualizzaCheckbox()
        Aspetta(500)
        cmdComando3.Enabled = True
    End Sub

    Private Sub cmdComando4_Click(sender As Object, e As EventArgs) Handles cmdComando4.Click
        cmdComando4.Enabled = False
        If checkListStep.potenza.ToUpper() = "SI" Then
            tcPowerOn()
            Aspetta(100)
        End If
        CmdTransparentCommand(checkListStep.Button4.Comando, 1000)
        If checkListStep.potenza.ToUpper() = "SI" Then
            tcPowerOff()
            Aspetta(100)
        End If
        Aspetta(1000)
        VisualizzaCheckbox()
        Aspetta(500)
        cmdComando4.Enabled = True
    End Sub

    Private Sub trkShift_Scroll(sender As Object, e As EventArgs) Handles trkShift.Scroll
        lblShiftCenter.Text = trkShift.Value
    End Sub

    Private Function PopolaComboIdProdotto() As String
        PopolaComboIdProdotto = ""
        objConn.Open()
        sqlStringa = "SELECT DISTINCT tblman.id_prodotto FROM tblman, prodotti where tblman.id_prodotto = prodotti.id_prodotto and (prodotti.categoria_prodotto like 'CM18%' or prodotti.categoria_prodotto like 'CM20%' Or prodotti.categoria_prodotto like 'OM61%')"
        sqlCommand = New OleDbCommand(sqlStringa, objConn)
        dbReader = sqlCommand.ExecuteReader
        cboIdProdotto.Items.Clear()
        Do While Not dbReader.Read() = Nothing
            cboIdProdotto.Items.Add(dbReader(0))
        Loop
        objConn.Close()
    End Function


    Private Function CopiaDB() As String
        CopiaDB = "OK"
        Dim serverFile As String
        Dim localFile As String
        Dim md5Source As String
        Dim md5Destination As String
        Dim verificaMD5 As Boolean = True

        If configIni.ReadValue("varie", "DataVerificaFile") = DateTime.Now.ToShortDateString Then verificaMD5 = False

copiaDBFW:
        ScriviMessaggio(Messaggio(2), lblMessaggio) 'Copia del database in corso...
        Try
            ScriviMessaggio(Messaggio(2) & " - " & databaseName, lblMessaggio) 'Copia del database in corso...
            If Not File.Exists(serverDBFolder & databaseName) Then
                ScriviMessaggio("Il DBFW nel server non è raggiungibile, premi un tasto per utilizzare il DBFW locale Object ESC per chiudere il sw", lblMessaggio)
                If AttendiTasto() = "ESC" Then End
                GoTo connessione
            End If
            If File.GetLastWriteTime(serverDBFolder & databaseName) > File.GetLastWriteTime(Application.StartupPath & "\DB\" & databaseName) Then
                My.Computer.FileSystem.CopyFile(serverDBFolder & databaseName, localDBFolder & databaseName, overwrite:=True)
                If verificaMD5 = True Then
                    md5Source = Md5Calculate(serverDBFolder & databaseName)
                    md5Destination = Md5Calculate(localDBFolder & databaseName)
                    If md5Source <> md5Destination Then
                        ScriviMessaggio("Copia in locale del DBFW non riuscita correttamente, premi un tasto per riprovare, ESC per uscire", lblMessaggio, ArcaColor.Arancio)

                        If AttendiTasto() = "ESC" Then
                            CopiaDB = "KO"
                            Exit Function
                        End If
                        Dim folder As DirectoryInfo = New DirectoryInfo(localDBFolder)
                        Try
                            For Each Cartella As IO.DirectoryInfo In folder.GetDirectories
                                Cartella.Delete(True)
                            Next
                            For Each File As IO.FileInfo In folder.GetFiles
                                File.Delete()
                            Next
                        Catch ex As Exception
                        End Try
                        GoTo copiaDBFW
                    End If
                End If
            End If

        Catch ex As System.IO.FileNotFoundException
            If Dir(localDBFolder & databaseName) = "" Then
                Interfaccia("errore")
                ScriviMessaggio(String.Format(Messaggio(201), databaseName, serverDBFolder, localDBFolder), lblMessaggio) 'Non è stato possibile trovare il database {0} all'indirizzo di rete {1}, e neppure in quello locale {2}. Premere un tasto per uscire.
                AttendiTasto()
                End
            Else
                Interfaccia("attesa")
                ScriviMessaggio(String.Format(Messaggio(202), databaseName, serverDBFolder, localDBFolder), lblMessaggio) 'Non è stato possibile trovare il database {0} all'indirizzo di rete {1}, verrà utilizzato quello in locale {2}. Premere un tasto per continuare, ESC per uscire.
                Select Case AttendiTasto()
                    Case "ESC"
                        End
                End Select
            End If
        End Try

connessione:
        Try
            objConn.Open()
            sqlStringa = "Select COUNT (*) As 'Count' FROM (SELECT Distinct cod_fw from tblman)"
            sqlCommand = New OleDbCommand(sqlStringa, objConn)
            Dim nRecord As Integer = sqlCommand.ExecuteScalar
            pgbDownload.Visible = True
            pgbDownload.Maximum = nRecord

            sqlStringa = "SELECT DISTINCT tblman.cod_fw FROM tblman, prodotti WHERE tblman.id_prodotto = prodotti.id_prodotto AND (prodotti.categoria_prodotto LIKE '%CM%' OR prodotti.categoria_prodotto LIKE '%OM61%') AND (tblman.tipo_fw = 2 OR tblman.tipo_fw = 4 OR tblman.tipo_fw = 5) ORDER BY cod_fw" ' "SELECT DISTINCT tblman.cod_fw FROM tblman ORDER BY cod_fw"
            sqlCommand = New OleDbCommand(sqlStringa, objConn)
            Dim recordAttuale As Integer = 0
            dbReader = sqlCommand.ExecuteReader
            Dim fwPath As String
            md5Destination = "Destination"
            md5Source = "Source"
            Do While Not dbReader.Read() = Nothing
                fwPath = dbReader(0)
                ScriviMessaggio(Messaggio(4) & " - " & fwPath, lblMessaggio) 'Copia dei firmware in corso
                If Not Directory.Exists(serverFWFolder & fwPath) = True Then
                    ' file fw non trovato, segnalare in qualche maniera e continuare
                End If
                Dim myFoundFile() As String = Directory.GetFiles(serverFWFolder & fwPath)
                For Each myFile As String In myFoundFile
                    If Path.GetExtension(myFile).ToUpper = ".ZIP" Then
                        If verificaMD5 = True Then
                            md5Source = Md5Calculate(myFile)
                        End If
                        Exit For
                        End If
                Next
                If Not Directory.Exists(localFWFolder & fwPath) = True Then
                    My.Computer.FileSystem.CreateDirectory(localFWFolder & fwPath)
                End If
searchZipFile:
                myFoundFile = Directory.GetFiles(localFWFolder & fwPath)
                For Each myFile As String In myFoundFile
                    If Path.GetExtension(myFile).ToUpper = ".ZIP" Then
                        If verificaMD5 = True Then
                            md5Destination = Md5Calculate(myFile)
                        End If
                        GoTo jumpFwCopy
                    End If
                Next
                My.Computer.FileSystem.CopyDirectory(serverFWFolder & fwPath, localFWFolder & fwPath, overwrite:=True)
                GoTo searchZipFile
jumpFwCopy:
                If verificaMD5 = True Then

                    If md5Source <> md5Destination Then
                        myFoundFile = Directory.GetFiles(localFWFolder & fwPath)
                        For Each myFile As String In myFoundFile
                            File.Delete(myFile)
                        Next
                        GoTo searchZipFile
                    End If
                End If

doNotCopy:
                recordAttuale += 1
                pgbDownload.Value = recordAttuale
                Application.DoEvents()
                md5Source = "Source"
                md5Destination = "Destination"
            Loop
        Catch ex As Exception
            CopiaDB = ex.Message
            Interfaccia("errore")
            ScriviMessaggio(ex.Message + " copia fallita, premi un tasto per continuare", lblMessaggio)
            AttendiTasto()
            Interfaccia()
            If ex.Message.Substring(0, 19) = "Impossibile trovare" Then GoTo doNotCopy
        End Try
        objConn.Close()
        pgbDownload.Visible = False
        configIni.WriteValue("varie", "DataVerificaFile", DateTime.Now.ToShortDateString)
        ScriviMessaggio(Messaggio(3), lblMessaggio) 'Copia database terminata.
    End Function

    Private Function Cerca7Zip() As String
        If Dir(path7Zip) = "" Then
            Interfaccia("errore")
            ScriviMessaggio(String.Format(Messaggio(200), path7Zip), lblMessaggio)
            Cerca7Zip = "7-Zip NOT FOUND"
            AttendiTasto()
        Else
            Cerca7Zip = "OK"
        End If
    End Function

    Private Sub VisualizzaControlli(Optional ByVal mostra As Boolean = True)
        cboIdProdotto.Visible = mostra
        cboCliente.Visible = mostra
        cmdComando1.Visible = mostra
        cmdComando2.Visible = mostra
        cmdComando3.Visible = mostra
        cmdComando4.Visible = mostra
        cmdShiftKo.Visible = mostra
        cmdShiftOk.Visible = mostra
        txtMatricola.Visible = mostra
        cmdPrincipale.Visible = mostra
        lblShiftCenter.Visible = mostra
        trkShift.Visible = mostra
        chkFault.Visible = mostra
        cmdSx.Visible = mostra
        cmdDx.Visible = mostra
        chkOk.Visible = mostra
        pgbDownload.Visible = mostra
        lstChecklist.Visible = mostra
    End Sub

    Private Sub frmPrincipale_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        End
    End Sub

    Private Sub AbilitaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AbilitaToolStripMenuItem.Click
        lstComScope.Visible = True
        lstComScope.Enabled = True
    End Sub

    Private Sub DisabilitaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DisabilitaToolStripMenuItem.Click
        lstComScope.Visible = False
        lstComScope.Enabled = False
    End Sub

    Private Sub lstChecklist_DrawItem(sender As Object, e As DrawItemEventArgs) Handles lstChecklist.DrawItem
        e.DrawBackground()
        Dim b As New SolidBrush(CType(lstChecklist.Items(e.Index), myListItem).Colore)
        e.Graphics.DrawString(CType(lstChecklist.Items(e.Index), myListItem).Testo, e.Font, b, e.Bounds)
    End Sub

    Private Sub cmdShiftKo_Click(sender As Object, e As EventArgs) Handles cmdShiftKo.Click
        pulsantePremuto = "KO"
    End Sub

    Private Sub RS232ToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RS232ToolStripMenuItem.Click
        tipoConnessione = DLINKMODESERIAL
        ApriConnessione()
        If statoConnessione = ConnectionState.Connect Then
            mnuMainCommands.Enabled = True
            MsgBox("CONNECTION OK")
        End If
    End Sub

    Private Sub USBToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles USBToolStripMenuItem.Click
        tipoConnessione = DLINKMODEUSB
        ApriConnessione()
        If statoConnessione = ConnectionState.Connect Then
            mnuMainCommands.Enabled = True
            MsgBox("CONNECTION OK")
        End If
    End Sub

    Private Sub mnuDisconnect_Click(sender As Object, e As EventArgs) Handles mnuDisconnect.Click
        ChiudiConnessione()
        mnuMainCommands.Enabled = False
    End Sub

    Private Sub MenuStrip1_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles MenuStrip1.ItemClicked

    End Sub

    Private Sub SingleCommandToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SingleCommandToolStripMenuItem.Click
        Dim comando As String = InputBox(Messaggio(96), Messaggio(97))
        If comando.Length = 0 Then Exit Sub
        cmdComandoSingolo(comando)
        MsgBox(SingleAnswer)
    End Sub

    Private Sub TransparentCommandToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TransparentCommandToolStripMenuItem.Click
        CmdTransparentIn()
        Dim comando As String = InputBox(Messaggio(96), Messaggio(98))
        If comando.Length = 0 Then Exit Sub
        CmdTransparentCommand(comando)
        CmdTransparentOut()
        MsgBox(TransparentAnswer)
    End Sub

    Private Sub OPENLToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OPENLToolStripMenuItem.Click
        cmdComandoSingolo("O,1,L,123456")
    End Sub

    Private Sub OPENRToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OPENRToolStripMenuItem.Click
        cmdComandoSingolo("O,1,R,123456")
    End Sub

    Private Sub CLOSELToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CLOSELToolStripMenuItem.Click
        cmdComandoSingolo("C,1,L")
    End Sub

    Private Sub lblMessaggio_Click(sender As Object, e As EventArgs) Handles lblMessaggio.Click

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)
        ApriConnessione()
        DLLExtendedStatus(hCon, lpEXTENDEDSTATUS, StrutturaNulla, myExtendedStatus, 5000)
        ChiudiConnessione()
        End
    End Sub

    Private Sub cmdPrincipale_Click(sender As Object, e As EventArgs) Handles cmdPrincipale.Click

    End Sub

    Private Sub CLOSERToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CLOSERToolStripMenuItem.Click
        cmdComandoSingolo("C,1,R")
    End Sub



    Private Sub lstComScope_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstComScope.SelectedIndexChanged

    End Sub

    Private Sub cmdDx_Click(sender As Object, e As EventArgs) Handles cmdDx.Click
        If trkShift.Maximum > trkShift.Value Then
            trkShift.Value += 1
        End If
        lblShiftCenter.Text = trkShift.Value
    End Sub

    Private Sub cmdSx_Click(sender As Object, e As EventArgs) Handles cmdSx.Click
        If trkShift.Value > 1 Then
            trkShift.Value -= 1
        End If
        lblShiftCenter.Text = trkShift.Value
    End Sub

    Private Sub cmdShiftOk_Click(sender As Object, e As EventArgs) Handles cmdShiftOk.Click
        pulsantePremuto = "OK"
    End Sub



    Private Sub PressioneTasto(sender As Object, e As KeyEventArgs) Handles lblMessaggio.KeyUp, lstChecklist.KeyUp, lstComScope.KeyUp,
        tabella.KeyUp, trkShift.KeyUp, txtMatricola.KeyUp, Me.KeyUp, lstChecklist.KeyUp, lstComScope.KeyUp, lblMacchina.KeyUp, lblShiftCenter.KeyUp,
        pgbDownload.KeyUp, cmdSx.KeyUp, cmdDx.KeyUp, cmdShiftOk.KeyUp, cmdShiftKo.KeyUp

        tastoPremuto = e.KeyCode
        Select Case sender.name.ToString
            Case cmdDx.Name.ToString
                lblShiftCenter.Text -= 1
            Case cmdSx.Name.ToString
                lblShiftCenter.Text += 1
        End Select
    End Sub

    Private Sub lstComScope_TextChanged(sender As Object, e As EventArgs) Handles lstComScope.TextChanged

    End Sub

    Private Sub frmPrincipale_HelpButtonClicked(sender As Object, e As CancelEventArgs) Handles Me.HelpButtonClicked

    End Sub
End Class
