﻿Imports System.Data.OleDb
Imports System.IO

Public Class CMClass
    Public Shared tastoPremuto As Integer = 0
    ' Public Shared messaggio As List(Of String) = New List(Of String)
    Public Shared sequentialNumber As Integer = 1
    Public Shared localFolder As String = Application.StartupPath
    Public Shared localDBFolder As String = localFolder & "\DB\"
    Public Shared localFWFolder As String = localFolder & "\FW\"
    Public Shared localDLFolder As String = localFolder & "\Download\"
    Public Shared configIni As New INIFile(Application.StartupPath & "\setup.ini")
    Public Shared lingua As String = configIni.ReadValue("option", "language")
    Public Shared connessioneLog As String = "RS232"
    Public Shared serverDBFolder As String = configIni.ReadValue("option", "DatabaseFolder")
    Public Shared serverFWFolder As String = configIni.ReadValue("option", "FirmwareFolder")
    Public Shared serverLogFolder As String = configIni.ReadValue("option", "LogFolder")
    Public Shared databaseName As String = configIni.ReadValue("option", "DatabaseName")
    Public Shared stringaConnessione As String = "provider=Microsoft.Jet.OLEDB.4.0;Mode=Read;Data Source =" & localDBFolder & databaseName
    Public Shared objConn As OleDbConnection = New OleDbConnection(stringaConnessione)
    Public Shared Tasto As Integer = 0

    ''' <summary>
    ''' Legge il database con la stringa di connessione standard e restituisce una lista indicizzata che sono il risultato della stringa SQL inserita
    ''' </summary>
    ''' <param name="sql">Stringa SQL necessaria per l'esecuzione della ricerca</param>
    ''' <returns></returns>
    Public Shared Function LeggiDB(ByVal sql As String) As List(Of String)
        LeggiDB = New List(Of String)
        objConn.Open()
        Dim cmdOle As New OleDbCommand(sql, objConn)
        Dim dRead As OleDbDataReader = cmdOle.ExecuteReader
        Do While dRead.Read() = Not Nothing
            LeggiDB.Add(dRead(0))
        Loop
        objConn.Close()
    End Function

    Public Shared Function Messaggio(ByVal indice As Integer) As String
        Dim msg As New INIFile(localFolder & "\" & lingua & ".ini")
        Messaggio = msg.ReadValue("messages", "msg" & indice)
        Messaggio = Replace(Messaggio, "Ã©", "è")
        Messaggio = Replace(Messaggio, "Ã²", "ò")
        Messaggio = Replace(Messaggio, "Ã¨", "é")
        Messaggio = Replace(Messaggio, "Ã ", "à")
        Messaggio = Replace(Messaggio, "Ã¹", "ù")
    End Function

    ''' <summary>
    ''' Definizione colori da utilizzare nell'applicazione
    ''' </summary>
    Structure ArcaColor
        Public Shared Blu As Color = Color.FromArgb(&HFF1A328A) 'Blu - 26.50.129 
        Public Shared Arancio As Color = Color.FromArgb(&HFFE45620) 'Arancio - 228.86.32
        Public Shared Bianco As Color = Color.FromArgb(&HFFFFFFFF) 'Bianco - 255.255.255
        Public Shared Verde As Color = Color.FromArgb(&HFFAEBF37) 'Verde - 174.191.55
        Public Shared Viola As Color = Color.FromArgb(&HFF55166E) 'Viola - 85.22.110
        Public Shared Nero As Color = Color.FromArgb(&HFF000000) 'Nero - 0.0.0
        Public Shared Grigio As Color = Color.FromArgb(&HFF58595B) 'Grigio - 88.89.91
        Public Shared GrigioChiaro As Color = Color.FromArgb(&HFF969696) 'Grigio Chiaro - 150.150.150

        'Public Shared sfondo As Color = Color.FromArgb(&HFF1A328A) 'Blu - 26.50.129 
        'Public Shared errore As Color = Color.FromArgb(&HFFE45620) 'Arancio - 228.86.32
        'Public Shared istruzione As Color = Color.FromArgb(&HFF999999) '
        'Public Shared messaggio As Color = Color.FromArgb(&HFFFFFFFF) 'Bianco - 255.255.255
        'Public Shared attesa As Color = Color.FromArgb(&HFFAEBF37) 'Verde - 174.191.55
        'Public Shared OK As Color = Color.FromArgb(&HFFAEBF37) '
        'Public Shared KO As Color = Color.FromArgb(&HFFE45620) '
        'Public Shared Warning As Color = Color.FromArgb(&HFFAAAA00) '

        'Public Shared sfondo As Color = Color.FromArgb(&HFFCBCBCB)
        'Public Shared errore As Color = Color.FromArgb(&HFFE45620)
        'Public Shared istruzione As Color = Color.FromArgb(&HFF1A3281)
        'Public Shared messaggio As Color = Color.FromArgb(&HFF000000)
        'Public Shared attesa As Color = Color.FromArgb(&HFF30E410)
        'Public Shared OK As Color = Color.FromArgb(&HFF7EAF47)
        'Public Shared KO As Color = Color.FromArgb(&HFF800000)
        'Public Shared Warning As Color = Color.FromArgb(&HFFBBBB00)
    End Structure


    ''' <summary>
    ''' Scrittura del messaggio sull'oggetto indicato
    ''' </summary>
    ''' <param name="testo">Testo da mostrare</param>
    ''' <param name="nomeObj">Nome dell'oggetto sulla quale scrivere il testo</param>
    ''' <param name="colore">Colore del testo da mostrare (default = messaggio)</param>
    Public Shared Sub ScriviMessaggio(ByVal testo As String, ByVal nomeObj As Object, Optional ByVal colore As Color = Nothing)
        'If colore = Nothing Then
        '    colore = ArcaColor.messaggio
        'End If
        nomeObj.Text = testo
        'nomeObj.ForeColor = colore
    End Sub

    ''' <summary>
    ''' Struttura contenente le parti della macchina
    ''' </summary>
    Public Structure SMacchina
        Dim Codice As String
        Dim IDcliente As Integer
        Dim NomeCliente As String
        Dim Categoria As String
        Dim CategoriaCompleta As String
        Dim PaeseDestinazione As String
        Dim SerialNumber As String
        Dim SerialNumberCompleto As String
        Dim ControllerSerialNumber As String
        Dim ControllerCodice As String
        Dim CRM As String
        Dim CD80 As String
        Dim nCD80 As Integer
        Dim CassetteNumber As Integer
        Dim LettoreModello As String
        Dim LettoreCodice As String
        Dim LettoreMatricola As String
        Dim CD80AMatricola As String
        Dim CD80BMatricola As String
        Dim CD80CMatricola As String
        Dim CD80DMatricola As String
        Dim MacAddress As String
        Dim MacAddressNew As String
        Dim MacAddressOld As String
        Dim HostName As String
        Dim LettoreFW As String
        Dim LettoreCDF As String
        Dim LettoreCurrency As String
        Dim SuiteCodice As String
        Dim SuiteNome As String
        Dim Skin As String
        Dim Language As String
        Dim DisplayMessage As String
        Dim DisplayVolume As String
        Dim DisplayLingua As String
        Dim DisplaySfondo As String
        Dim IDunitConfiguration As String
        Dim CustomizationCodice As String
        Dim ShiftCenterValue As String
        Dim BagFW As String
        Dim ScFW As String
        Dim EmfuFW As String
        'Dim ExtendedStatus As SExtendedStatus
        Dim Test As STest
    End Structure
    'Public Shared Macchina As SMacchina = New SMacchina


    ''' <summary>
    ''' Struttura dei FW standard
    ''' </summary>
    Public Structure SFirmware
        Dim Ver As String
        Dim Rel As String
    End Structure

    ''' <summary>
    ''' Struttura FW Bag
    ''' </summary>
    Public Structure SFwBag
        Dim MBAG As SFirmware
        Dim EMFU As SFirmware
    End Structure

    ''' <summary>
    ''' Struttura dei singoli fotosensori standand
    ''' </summary>
    Public Structure SPhoto
        Dim Valore As Integer
        Dim Min As Integer
        Dim Max As Integer
        Dim SogliaValore As Integer
        Dim SogliaMin As Integer
        Dim SogliaMax As Integer
        Dim AmbienteValore As Integer
        Dim AmbienteMin As Integer
        Dim AmbienteMax As Integer
    End Structure

    ''' <summary>
    ''' Struttura dei fotosensori Bag
    ''' </summary>
    Public Structure SPhotoBag
        Dim Transport As SPhoto
        Dim NotesInLift As SPhoto
        Dim Ir1Empty As SPhoto
        Dim Ir2Empty As SPhoto
        Dim Ir1NotEmpty As SPhoto
        Dim Ir2NotEmpty As SPhoto
    End Structure

    ''' <summary>
    ''' Struttura dei fotosensori del trasporto superiore
    ''' </summary>
    Public Structure SPhotoAll
        Dim InCenter As SPhoto
        Dim InLeft As SPhoto
        Dim HMax As SPhoto
        Dim Feed As SPhoto
        Dim C1 As SPhoto
        Dim Shift As SPhoto
        Dim InQ As SPhoto
        Dim Count As SPhoto
        Dim Out As SPhoto
        Dim C3 As SPhoto
        Dim C4A As SPhoto
        Dim C4B As SPhoto
        Dim Rej As SPhoto
        Dim Fpbksus As SPhoto
        Dim InBox As SPhoto
    End Structure

    ''' <summary>
    ''' Struttura dei fotosensori del Safe
    ''' </summary>
    Public Structure SPhotoSafe
        Dim CashLeft As SPhoto
        Dim CashRight As SPhoto
        Dim Thick As SPhoto
        Dim FinRcyc As SPhoto
        Dim EndV As SPhoto
        Dim InCD80A As SPhoto
        Dim InCD80B As SPhoto
        Dim InCD80C As SPhoto
        Dim InCD80D As SPhoto
    End Structure

    ''' <summary>
    ''' Struttura della taratura di tutti i fotosensori
    ''' </summary>
    Public Structure STaratura
        Dim FotoBag As SPhotoBag
        Dim Foto As SPhotoAll
        Dim FotoSafe As SPhotoSafe
    End Structure
    Public Shared Taratura As STaratura = New STaratura

    ''' <summary>
    ''' Struttura del singolo cassetto
    ''' </summary>
    Public Structure SCassette
        Dim noteId As String
        Dim bnNumber As Integer
        Dim freeCap As Integer
        Dim name As String
        Dim enable As String
        Dim firmware As String
    End Structure


    ''' <summary>
    ''' Struttura standard dei test
    ''' </summary>
    Public Structure STest
        Dim DataInizio As String
        Dim DataFine As String
        Dim OraInizio As String
        Dim OraFine As String
        Dim ToolVersion As String
        Dim TestLocation As String
        Dim TestUser As String
        Dim Categoria As String
        Dim EsitoFinale As Boolean
        Dim Fase As String
        Dim DateTime As Boolean
        Dim Assign As Boolean
        Dim UnitConfiguration As Boolean
        Dim CassetteSetup As Boolean
        Dim CassetteNumber As Boolean
        Dim Lan As Boolean
        Dim CancellazioneBanchi As Boolean
        Dim ClientCDFDownload As Boolean
        Dim LettoreCDF As String
        Dim ReaderFWDownload As Boolean
        Dim PhotoAdjust As Boolean
        Dim CustomDownload As Boolean
        Dim SuiteDownload As Boolean
        Dim CRM As Boolean
        Dim Bag As Boolean
        Dim MatricolaMacchina As String
        Dim MatricolaController As String
        Dim MatricolaLettore As Boolean
        Dim TestConnessione As Boolean
        Dim TipoConnessione As String
        Dim TaraturaFotoBag As Boolean
        Dim TaraturaFoto As Boolean
        Dim MiniDeposito As Boolean
        Dim Seriali As Boolean
        Dim USB As Boolean
        Dim ImpostazioneTipo As Boolean
        Dim Alarm As Boolean
        Dim UMA As Boolean
        Dim Suite As String
        Dim Custom As String
        Dim SerialNumberCompleto As String
        Dim ControllerSerialNumber As String
        Dim LettoreCodice As String
        Dim LettoreMatricola As String
        Dim MacAddressNew As String
        Dim MacAddressOld As String
        Dim LettoreCurrency As String
        Dim SuiteCodice As String
        Dim Skin As String
        Dim Language As String
        Dim DisplayMessage As String
        Dim DisplayVolume As String
        Dim DisplayLingua As String
        Dim DisplaySfondo As String
        Dim CustomizationCodice As String
        'Dim ShiftCenterValue As Integer
        'Dim HostName As String
        'Dim MacAddressNew As String
        'Dim MacAddressOld As String
        'Dim ReaderFW As String
        'Dim CustomCode As String
        'Dim SuiteCode As String
        'Dim CDFLettore As String
    End Structure
    Public Shared LogTest As STest = New STest

    Public Structure SLog
        Dim Anagrafica As SMacchina
    End Structure

    Structure STraspSup
        Dim Codice As String
        Dim Matricola As String
        Dim MacchinaCodice As String
        Dim MatricolaController As String
        Dim tipoController As String
        Dim filePxaRTC As String
        Dim filePxaFPGA As String
        Dim filePxaOSC As String
        Dim fileEngicamRTC As String
        Dim fileEngicamFPGA As String
        Dim fileEngicamOSC As String
        Dim fileEngicamUSB As String
        Dim fileEngicamBoot As String
        Dim fwRTC As String
        Dim fwFPGA As String
        Dim fwOSC As String
        Dim MacAddress As String
        Dim ClienteID As String
        Dim ClienteNome As String
        Dim Categoria As String
        Dim CodiceFWSuite As String
        Dim CRM As Boolean
        Dim CD80 As Integer
        Dim TestResult As String
        Dim checklist As String
        Dim AliVOff As Single
        Dim AliVonMotOff As Single
        Dim AliVonMotOn As Single
        Dim fotoRealTime As SPhotoAll
        Dim fotoSafe As SPhotoSafe
        Dim fotoForchetta As String
        Dim SuiteCodice As String
        Dim SuiteCodiceCompleto As String
        Dim CustomizationCodice As String
        Dim BnRiconosciute As Integer
        Dim BnRifiutate As Integer
        Dim VelTrasp As SVelTrasp
        Dim ShiftCenterValue As Integer
        Dim DeviceName As String
        Dim Deposito As SDeposito
        Dim inizioData As String
        Dim inizioOra As String
        Dim fineData As String
        Dim fineOra As String
        Dim customDisplayMessageCode As String
        Dim customSkinCode As String
        Dim customCdfCode As String
        Dim customMenuTree As String
    End Structure

    Structure SDeposito
        Dim Riconosciute As Integer
        Dim Unfit As Integer
        Dim Reject As Integer
        Dim Totale As Integer
    End Structure
    Structure SLimitiVelocità
        Dim Massima As Integer
        Dim MediaMassima As Integer
        Dim MediaMinima As Integer
        Dim Minima As Integer
    End Structure

    Structure SVelTrasp
        Dim Massima As Integer
        Dim Media As Integer
        Dim Minima As Integer
        Dim Limiti As SLimitiVelocità
    End Structure

    Public Structure SCashData
        Dim cassetto As SCassette
    End Structure
    Public CashData As SCashData

    ''' <summary>
    ''' Ciclo per attendere un determinato tempo
    ''' </summary>
    ''' <param name="mSec">durata della pausa espressa in mSec</param>
    Public Shared Sub Aspetta(Optional ByVal mSec As Integer = 1000)

        Application.DoEvents()
        Threading.Thread.Sleep(mSec)

    End Sub

    ''' <summary>
    ''' Ciclo di attesa pressione tasto(RISPOSTA : INVIO,R,ESC,OK=tastoSpecifico)
    ''' </summary>
    ''' <param name="tastoSpecifico">Codice asci decimale del tasto da attendere</param>
    ''' <returns></returns>
    Public Shared Function AttendiTasto(Optional ByVal tastoSpecifico As String = "", Optional ByVal oggetto As Object = Nothing) As String
        'Dim tasto As Integer
        If oggetto IsNot Nothing Then
            oggetto.focus()
        End If
        AttendiTasto = ""
        tastoPremuto = 0
        Do Until Len(AttendiTasto) > 0
            If Len(tastoSpecifico) > 0 Then
                Select Case UCase(tastoSpecifico)
                    Case "INVIO", "ENTER"
                        Tasto = 13
                    Case "ESC", "esc"
                        Tasto = 27
                    Case Else
                        Tasto = Asc(UCase(tastoSpecifico))
                End Select
                If tastoPremuto = Tasto Then
                    AttendiTasto = "OK"
                End If
            Else
                Select Case tastoPremuto
                    Case 0
                    Case 13
                        AttendiTasto = "INVIO"
                    Case 82, 114
                        AttendiTasto = "R"
                    Case 27
                        AttendiTasto = "ESC"
                    Case 48, 96
                        AttendiTasto = "0"
                    Case 49, 97
                        AttendiTasto = "1"
                    Case 50, 98
                        AttendiTasto = "2"
                    Case 51, 99
                        AttendiTasto = "3"
                    Case 52, 100
                        AttendiTasto = "4"
                    Case 53, 101
                        AttendiTasto = "5"
                    Case 54, 102
                        AttendiTasto = "6"
                    Case 55, 103
                        AttendiTasto = "7"
                    Case 56, 104
                        AttendiTasto = "8"
                    Case 57, 105
                        AttendiTasto = "9"
                    Case Else
                        AttendiTasto = "OK"
                End Select
                'End If
            End If

            Application.DoEvents()
        Loop
    End Function

    ''' <summary>
    ''' Scrive il singolo dato del log
    ''' </summary>
    ''' <param name="nomeFile"></param>
    ''' <param name="sezione"></param>
    ''' <param name="valore"></param>
    Public Shared Sub ScriviLog(ByVal nomeFile As String, ByVal sezione As String, ByVal valore As String)
        Using fileLog As StreamWriter = File.AppendText(localFolder & "\" & nomeFile)

        End Using
    End Sub

    Public Class myListItem
        Dim _testo As String
        Dim _colore As Color

        Property Testo As String
            Get
                Return _testo
            End Get
            Set(ByVal value As String)
                _testo = value
            End Set
        End Property

        Property Colore As Color
            Get
                Return _colore
            End Get
            Set(ByVal value As Color)
                _colore = value
            End Set
        End Property
    End Class

    Structure Sbottone
        Dim Titolo As String
        Dim Comando As String
    End Structure

    Structure Schecklist
        Dim Titolo As String
        Dim Stringa As String
        Dim Button1 As Sbottone
        Dim Button2 As Sbottone
        Dim Button3 As Sbottone
        Dim Button4 As Sbottone
        Dim comando_uscita As String
        Dim comando_entrata As String
        Dim potenza As String
    End Structure

    Structure SPhotoStatus
        Dim coverOpen As Boolean
        Dim feederOpen As Boolean
        Dim reject As Boolean
        Dim feed As Boolean
        Dim pressor As Boolean
        Dim inLeft As Boolean
        Dim inCenter As Boolean
        Dim curve1 As Boolean
        Dim curve3 As Boolean
        Dim curve4A As Boolean
        Dim curve4B As Boolean
        Dim pusher As Boolean
        Dim align As Boolean
        Dim Hpres As Boolean
        Dim shift As Boolean
        Dim inq As Boolean
        Dim count As Boolean
        Dim out As Boolean
        Dim Hmax As Boolean
    End Structure
    Public Shared photoStatus As SPhotoStatus

    Public Shared Function Bin2Hex(ByVal valore As String) As String
        Bin2Hex = ""
        Try
            Dim dec As Integer = Bin2Dec(valore)
            Bin2Hex = Dec2Hex(dec)
        Catch ex As Exception
        End Try
    End Function

    Public Shared Function Hex2Dec(ByVal valore As String) As Double
        Hex2Dec = 0
        Try
            For i = valore.Length To 1 Step -1
                Select Case Mid(valore, i, 1)
                    Case "A"
                        Hex2Dec += 10 * 16 ^ (valore.Length - i)
                    Case "B"
                        Hex2Dec += 11 * 16 ^ (valore.Length - i)
                    Case "C"
                        Hex2Dec += 12 * 16 ^ (valore.Length - i)
                    Case "D"
                        Hex2Dec += 13 * 16 ^ (valore.Length - i)
                    Case "E"
                        Hex2Dec += 14 * 16 ^ (valore.Length - i)
                    Case "F"
                        Hex2Dec += 15 * 16 ^ (valore.Length - i)
                    Case Else
                        Hex2Dec += CInt(Mid(valore, i, 1)) * 16 ^ (valore.Length - i)
                End Select
            Next
        Catch ex As Exception
        End Try

    End Function
    Public Shared Function Hex2Bin(ByVal valore As String) As String
        Hex2Bin = ""
        Try
            Dim iValore As Double = Hex2Dec(valore)
            Hex2Bin = Dec2Bin(iValore)
        Catch ex As Exception
        End Try
    End Function

    Public Shared Function Dec2Hex(ByVal valore As Integer, Optional ByVal base As Integer = 0) As String
        Dec2Hex = ""
        Try
            Dec2Hex = Hex(valore)
            If base > 0 Then
                For i = Dec2Hex.Length + 1 To base
                    Dec2Hex = "0" + Dec2Hex
                Next
            End If
        Catch ex As Exception
        End Try
    End Function

    Public Shared Function Bin2Dec(ByVal valore As String) As Integer
        Bin2Dec = 0
        Try
            For i = 0 To valore.Length - 1
                Bin2Dec += CInt(Mid(valore, Len(valore) - i, 1)) * 2 ^ i
            Next
        Catch ex As Exception
        End Try
    End Function

    Public Shared Function Dec2Bin(ByVal valore As Integer, Optional ByVal base As Integer = 8) As String
        Dim i As Integer
        Dec2Bin = ""
        Try
            Do While Not valore = 0
                i = valore / 2
                If i * 2 = valore Then
                    Dec2Bin = "0" + Dec2Bin
                Else
                    Dec2Bin = "1" + Dec2Bin
                End If
                valore = Int(valore / 2)
            Loop

            Dim formato As String = ""
            For i = 1 To base - Dec2Bin.Length
                Dec2Bin = "0" + Dec2Bin
            Next
        Catch ex As Exception
        End Try
        'valore = String.Format(formato, Dec2Bin)
    End Function

    Public Shared Function LeggiSecondeCifre(ByVal valore As String) As String
        LeggiSecondeCifre = ""
        Try
            For i = 2 To valore.Length Step 2
                LeggiSecondeCifre += Mid(valore, i, 1)
            Next
        Catch ex As Exception
        End Try
    End Function

    Public Shared Function AsciCode2AsciChar(ByVal stringa As String) As String
        AsciCode2AsciChar = ""
        Try
            For i = 0 To stringa.Length - 1 Step 2

                AsciCode2AsciChar &= Chr("&h" & CInt(stringa.Substring(i, 2)))
            Next
        Catch ex As Exception
        End Try
    End Function

    Public Shared Function Dec2Char(ByVal stringa As String, Optional ByVal lunghezza As Integer = 4) As String
        Dec2Char = ""
        For i = 0 To lunghezza - stringa.Length - 1
            Dec2Char &= "30"
        Next
        Try
            For i = 0 To lunghezza - stringa.Length - 1
                Dec2Char &= Dec2Hex(Asc(stringa.Substring(i, 1)))
            Next
        Catch ex As Exception
        End Try
    End Function
End Class
