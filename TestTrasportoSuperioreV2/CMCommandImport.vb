﻿'cmdOpen() 'OK
'cmdUnitCoverTest()
''cmdDeposit() 'OK
''cmdUndo() 'KO KO KO KO KO KO KO KO KO KO KO 
''CmdGetCashData() 'OK
''cmdGetVersion(0) 'OK
''CmdWithdrawal() 'KO KO KO KO KO KO KO KO KO 
'CmdExtendedStatus() 'OK
'cmdClose("R") 'OK
'cmdAssign() 'OK

Imports System
Imports System.Runtime.InteropServices
Imports System.Text
Imports TestTrasportoSuperioreV2.CMClass

Module CMCommandImport

    Public hCon As Integer = 0
    Public hConAttuale As Integer = 0
    Public statoConnessione As String
    Public TransparentAnswer As String = ""
    Public comandoSingoloRisposta(120) As String
    Public SingleAnswer As String = ""

    'Costanti parametri per le strutture delle funzioni
    Public Const LENPASSWORD As Integer = 6
    Public Const MAXSLOTACTIVE As Integer = 4
    Public Const MAXFLASHREF As Integer = 16
    Public Const MAXCASSETTES As Integer = 24
    Public Const MAXDELAYCLASS As Integer = 10
    Public Const MAXUSERID As Integer = 10
    Public Const MAXMODULE As Integer = 30
    Public Const MAXCHARDENOM As Integer = 4
    Public Const MAXCHANNEL As Integer = 32
    Public Const MAXDEPOSIT As Integer = 200
    Public Const MAXDENOMINATION As Integer = 32
    Public Const MAXBOOKING As Integer = 29
    Public Const SERIALNUMBER As Integer = 12
    Public Const MAXLOG As Integer = 8192
    Public Const MAXHOOPERCOIN As Integer = 8
    Public Const NUMPROTDM As Integer = 6

    'Costanti per la struttura lpCmd delle chiamate alla DLL
    Public Const lpAUTOASSIGNCASSETTE As Byte = 65
    Public Const lpBELL As Byte = 66
    Public Const lpCLOSE As Byte = 67
    Public Const lpDEPOSITCASH As Byte = 68
    Public Const lpEXTENDEDSTATUS As Byte = 69
    Public Const lpFILL As Byte = 70
    Public Const lpGETCASHDATA As Byte = 71
    Public Const lpERRORLOG As Byte = 72
    Public Const lpINIT As Byte = 73
    Public Const lpJOURNAL As Byte = 74
    Public Const lpKEYCHANGE As Byte = 75
    Public Const lpDOWNLOAD As Byte = 76
    Public Const lpOPEN As Byte = 79
    Public Const lpSINGLECOMMAND As Byte = 91
    Public Const lpFILETRANSF As Byte = 94
    Public Const lpWITHDRAWAL As Byte = 87
    Public Const lpSETSEVLEVEL As Byte = 80
    Public Const lpSETOIDPAR As Byte = 78
    Public Const lpGETSEVLEVEL As Byte = 81
    Public Const lpGETCONFIG As Byte = 82
    Public Const lpSETCONFIG As Byte = 83
    Public Const lpTEST As Byte = 84
    Public Const lpUNDO As Byte = 85
    Public Const lpVERSION As Byte = 86
    Public Const lpTRANSPARENTSTART As Byte = 88
    Public Const lpTRANSPARENTEND As Byte = 89
    Public Const lpMODFILL As Byte = 90
    Public Const lpCTSTWS As Byte = 121
    Public Const lpGETCASHMONSTER As Byte = 103
    Public Const lpEXTSTATUSMONSTER As Byte = 101
    Public Const lpMOVENOTES As Byte = 119
    Public Const lpGETCNFMONSTER As Byte = 114
    Public Const lpSWAPNOTES As Byte = 115
    Public Const lpCOINDSP As Byte = 100
    Public Const lpUTILITY As Byte = 117
    Public Const lpROBBERY As Byte = 98
    Public Const lpMODDEP As Byte = 77
    Public Const lpEMULSEND As Byte = 93
    Public Const lpEMULRECEIVE As Byte = 64
    Public Const lpTRANSPARENT As Byte = 35
    Public Const lpTWSCTS As Byte = 123
    Public Const lpLEFTSIDE As Byte = 76
    Public Const lpRIGHTSIDE As Byte = 82
    'Public Const lpSINGLECONNECTION As Byte = 

    'Costanti di configurazione protocollo di connessione
    Public Const PROTTRANSPARENTOFF As Byte = 0
    Public Const PROTTRANSPARENTON As Byte = 1
    Public Const DLINKMODESERIAL As Byte = 83
    Public Const DLINKMODESERIALEASY As Byte = 115
    Public Const DLINKMODETCPIP As Byte = 76
    Public Const DLINKMODESSL As Byte = 108
    Public Const DLINKMODETLS As Byte = 116
    Public Const DLINKMODEUSB As Byte = 85
    Public Const DLINKMODEUSBEASY As Byte = 117
    Public Const DLINKMODEEMULATION As Byte = 69
    Public Const DLINKMODESERVERLAN As Byte = 101

    <DllImport("CMTrace.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMTrace_Init")>
    Public Function TraceInit(ByRef layer As Int32) As Short
    End Function

    <DllImport("CMTrace.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMTrace_SetTraceDirectiveExt")>
    Public Function SetTraceDirective(traceDirective As STraceDirective) As Short
    End Function

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
    Public Structure STraceDirective
        Public infoLayer As Int32
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=259)>
        Dim tracePathName() As Byte
        Public lpComScope As Int32
        Public trcType As Int32
        Public trcSize As Short
        Public trcNum As Short
        Public infoComp() As Int16
    End Structure
    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
    Public Structure SComponent
        Public level As Int32
    End Structure
    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
    Public Structure SComScope
        Public hWin As Short
    End Structure



    Public Function LeggiRc(ByVal rc As Integer, Optional modulo As String = "RC", Optional ByVal categoria As String = "") As String
        Dim replyFile As String = ""
        If categoria.Length = 0 Then
            Select Case myMacchina.Info.Categoria
                Case "CM18", "CM18SOLO", "CM20S", "CM18EVO"
                    replyFile = localFolder & "\TheRCsCM18.ini"
                Case "CM18B", "OM61"
                    replyFile = localFolder & "\TheRCsCM18B.ini"
                Case "CM18T", "CM18SOLOT", "CM20T", "CM18EVOT"
                    replyFile = localFolder & "\TheRCsCM18T.ini"
                Case "CM20"
                    replyFile = localFolder & "\TheRCsCM20.ini"
            End Select
        Else
            replyFile = localFolder & "\TheRCsCM20.ini"
        End If
        Dim replyCode As New INIFile(replyFile)
        LeggiRc = replyCode.ReadValue(modulo, rc)
    End Function

    Structure SScope
        Dim Send As String
        Dim Receive As String
    End Structure
    Public myScope As New SScope


    Structure SMacchina
        Dim Status As MExtendedStatus
        Dim Open As SOpenStatus
        Dim Info As SInfo
        Dim Serials As SSerials
        Dim Firmware As SModuleFirmware
        Dim Test As STest
    End Structure
    Public myMacchina As SMacchina = New SMacchina

    Structure SCliente
        Dim ID As Integer
        Dim Nome As String
    End Structure
    Structure SModuleFirmware
        Dim Suite As SFirmware
        Dim Reader As SFirmware
        Dim Cassette As SFirmware
    End Structure

    Structure SFirmware
        Dim Code As String
        Dim Name As String
        Dim Ver As String
        Dim Rel As String
    End Structure

    Structure SSerials
        Dim Reader As String
        Dim System As String
        Dim Controller As String
    End Structure
    Structure SInfo
        Dim MACAddress As String
        Dim CassetteNumber As String
        Dim Cliente As SCliente
        Dim Codice As String
        Dim CategoriaCompleta As String
        Dim Categoria As String
        Dim CRM As Integer
        Dim CD80 As Integer
    End Structure

    Structure SOpenStatus
        Dim Side As String
        Dim rc As String
    End Structure

    Structure MExtendedStatus
        Dim Feeder As String
        Dim Controller As String
        Dim Reader As String
        Dim Safe As String
        Dim CassetteA As String
        Dim CassetteB As String
        Dim CassetteC As String
        Dim CassetteD As String
        Dim CassetteE As String
        Dim CassetteF As String
        Dim CassetteG As String
        Dim CassetteH As String
        Dim CassetteI As String
        Dim CassetteJ As String
        Dim CassetteK As String
        Dim CassetteL As String
        Dim ProtocolCasseteNum As String
        Dim EscrowA As String
        Dim EscrowB As String
        Dim DepositA As String
        Dim DepositB As String
        Dim ProtocoloDepositNum As String
        Dim MsgFeeder As String
        Dim MsgController As String
        Dim MsgReader As String
        Dim MsgSafe As String
        Dim MsgCassetteA As String
        Dim MsgCassetteB As String
        Dim MsgCassetteC As String
        Dim MsgCassetteD As String
        Dim MsgCassetteE As String
        Dim MsgCassetteF As String
        Dim MsgCassetteG As String
        Dim MsgCassetteH As String
        Dim MsgCassetteI As String
        Dim MsgCassetteJ As String
        Dim MsgCassetteK As String
        Dim MsgCassetteL As String
        Dim MsgEscrowA As String
        Dim MsgEscrowB As String
        Dim MsgDepositA As String
        Dim MsgDepositB As String
    End Structure

    Public Structure ConnectionState
        Const Disconnect As String = "DISCONNESSO"
        Const Connect As String = "CONNESSO"
        Const Open As String = "APERTA"
        Const TransparentMode As String = "TRANSPARENTON"
        Const Errore As String = "ERRORE"
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
    Structure SNull
    End Structure
    Public StrutturaNulla As SNull
    Dim dllError As New INIFile(localFolder & "\dllError.ini")

#Region "Vari Comandi singoli"
    Function LimitaCD80(Optional ByVal cassetteName As String = "A", Optional ByVal limite As Integer = 750) As String
        Dim comando As String = "03"
        Dim limiteStringa As String = ""
        Dim stringa As String = ""
        LimitaCD80 = ""

        stringa = Hex(limite)
        For i = 2 To Len(limiteStringa) Step 2
            limiteStringa = limiteStringa & "0" & Mid(stringa, i, 1)
        Next
        For i = Len(limiteStringa) To 7
            limiteStringa = "0" & limiteStringa
        Next
        Select Case cassetteName
            Case "A"
                comando = "94" & comando
            Case "B"
                comando = "95" & comando
        End Select
        comando = comando & Hex(750)

    End Function

    Function LeggiSUITE() As String
        LeggiSUITE = cmdComandoSingolo("T,1,0,44", 5, 5000)
        Select Case LeggiSUITE
            Case "OK"
                myMacchina.Firmware.Suite.Name = comandoSingoloRisposta(6)
        End Select
    End Function

    Function LeggiSuiteFileName() As String
        LeggiSuiteFileName = cmdComandoSingolo("T,1,0,84", 5, 5000)
        Select Case LeggiSUITE()
            Case "OK"
                myMacchina.Firmware.Suite.Code = comandoSingoloRisposta(6)
        End Select
    End Function

    Function LeggiCassetteNumber() As String
        LeggiCassetteNumber = cmdComandoSingolo("T,1,0,88", 5, 5000)
        Select Case LeggiCassetteNumber
            Case "OK"
                myMacchina.Info.CassetteNumber = comandoSingoloRisposta(10)
        End Select
    End Function

    Function LeggiMACAddress() As String
        LeggiMACAddress = cmdComandoSingolo("T,1,6,14", 5, 5000)
        Select Case LeggiMACAddress
            Case "OK"
                myMacchina.Info.MACAddress = comandoSingoloRisposta(6)
        End Select
    End Function

    Function LeggiSerialeLettore() As String
        LeggiSerialeLettore = cmdComandoSingolo("T,1,3,13", 5, 5000)
        Select Case LeggiSerialeLettore
            Case "OK"
                myMacchina.Serials.Reader = comandoSingoloRisposta(9)
        End Select
    End Function
#End Region

#Region "Connection and Disconnection"
    ''' <summary>
    ''' Connetti
    ''' </summary>
    ''' <param name="hCon"></param>
    ''' <param name="connectionparam"></param>
    ''' <returns></returns>
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Connect")>
    Public Function Connetti(ByRef hCon As Int16, ByRef connectionparam As SConnectionParam) As Int16
    End Function

    ''' <summary>
    ''' Disconnetti
    ''' </summary>
    ''' <param name="hCon"></param>
    ''' <returns></returns>
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Disconnect")>
    Public Function Disconnetti(ByVal hCon As Int16) As Integer
    End Function

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
    Structure SRSConf
        ''' <summary>
        ''' COM port name
        ''' </summary>
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=10)>
        Dim device As String
        ''' <summary>
        ''' Port speed
        ''' </summary>
        Dim baudrate As Int32
        ''' <summary>
        ''' Parity check type (0=no parity - 1=odd - 2=even)
        ''' </summary>
        Dim parity As Int32
        ''' <summary>
        ''' Stop bit number
        ''' </summary>
        Dim stopbit As Int32
        ''' <summary>
        ''' Data bits number
        ''' </summary>
        Dim car As Int32
        ''' <summary>
        ''' DTR managed by the CMDevice
        ''' </summary>
        Dim dtr As Boolean
    End Structure
    ''' <summary>
    ''' Parametri di connessione RS232
    ''' </summary>
    ''' <remarks></remarks>
    Public RsConf As SRSConf

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
    Structure STcpIpPar
        ''' <summary>
        ''' CM IP address number or CM Netbios name
        ''' </summary>
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=20)>
        Dim clientIpAddr As String
        ''' <summary>
        ''' TCP IP comunication port number (8000-8009 / 8101 service port / 8100 used for file transfert)
        ''' </summary>
        Dim portNumber As Integer
    End Structure
    ''' <summary>
    ''' Parametri di connessione TCPIP
    ''' </summary>
    Public TcpIpPar As STcpIpPar

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
    Structure SConnectionParam
        ''' <summary>
        ''' Connection type: S=RS232, s=RS232 simplified, L=LAN, 
        ''' l=LAN SSL, t=LAN TLS, U=USB, u=USB simplified, E=Emulation
        ''' </summary>
        Dim ConnectionMode As Byte
        ''' <summary>
        ''' Parametri di connessione RS232
        ''' </summary>
        Dim RsConf As SRSConf
        ''' <summary>
        ''' Parametri di connessione TCPIP
        ''' </summary>
        Dim TcpIpPar As STcpIpPar
    End Structure
    ''' <summary>
    ''' Parametri di connessione
    ''' </summary>
    Public ConnectionParam As SConnectionParam = New SConnectionParam
#End Region

#Region "Single Command"
    ''' <summary>
    ''' Single Command Import
    ''' </summary>
    ''' <param name="Hcon"></param>
    ''' <param name="idCommand"></param>
    ''' <param name="lpCmd"></param>
    ''' <param name="lpReply"></param>
    ''' <param name="timeOute"></param>
    ''' <returns></returns>
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function CmdSingleCommand(ByVal Hcon As Int16, ByVal idCommand As Byte, ByRef lpCmd As STransparent, ByRef lpReply As STransparent, ByVal timeOute As Int32) As Integer
    End Function

    ''' <summary>
    ''' Single Command Structure
    ''' </summary>
    Structure STransparent
        Dim buff As String
        Dim size As Integer
    End Structure
    Public myTransparent As STransparent = New STransparent
#End Region

    Public Function CmdTransparentIn() As String
        CmdTransparentIn = cmdComandoSingolo("X,1", 2, 5000)
        If CmdTransparentIn = "OK" Then
            statoConnessione = ConnectionState.TransparentMode
        End If
    End Function

    Public Function CmdTransparentOut() As String
        CmdTransparentOut = cmdComandoSingolo("Y,1", 2, 5000)
        If CmdTransparentOut = "OK" Then
            statoConnessione = ConnectionState.Connect
        End If
        Aspetta(3000)
    End Function

    Public Function CmdGetMacAddress() As String
        CmdGetMacAddress = cmdComandoSingolo("T,1,6,14", 4, 5000)
        'If CmdGetMacAddress = "OK" Then
        '    Return comandoSingoloRisposta(5)
        'End If
    End Function
    Public Function CmdSetMacAddress(ByVal macAddress As String) As String
        CmdSetMacAddress = cmdComandoSingolo("Z,1,6,14," & macAddress, 4, 5000)

    End Function

    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function DLLReset(ByVal Hcon As Int16, ByVal idCommand As Byte, ByRef lpCmd As SInpOpTest, ByRef lpReply As Int16, ByVal timeOute As Int32) As Integer
    End Function

    Public Function cmdReset(Optional ByVal cat As String = "") As String
        Dim replyFile As String = ""
        cmdReset = ""
        Dim cicle As Integer = 0

        Try
            If cat.Length = 0 Then
                cat = myMacchina.Info.Categoria
            End If

            Select Case cat
                Case "CM18", "CM18SOLO", "CM20S", "CM18EVO"
                    replyFile = localFolder & "\TheRCsCM18.ini"
                Case "CM18B", "OM61"
                    replyFile = localFolder & "\TheRCsCM18B.ini"
                Case "CM18T", "CM18SOLOT", "CM20T", "CM18EVOT"
                    replyFile = localFolder & "\TheRCsCM18T.ini"
                Case "CM20"
                    replyFile = localFolder & "\TheRCsCM20.ini"
                Case Else
                    replyFile = localFolder & "\TheRCsCM18T.ini"
            End Select
            Dim replyCode As New INIFile(replyFile)
            'InpOpTest.Modulo = Convert.ToByte("0"c)
            'InpOpTest.Type = 7 'Convert.ToByte("7"c)
            'Dim rcReset As Int16 = 0

            cmdReset = cmdComandoSingolo("T,1,0,7", 4, 5000) ' DLLReset(hCon, lpTEST, InpOpTest, rcReset, 5000)
            'Select Case cmdReset
            '    Case 0
            '        cmdReset = replyCode.ReadValue("RC", rcReset)
            '    Case Else
            '        cmdReset = dllError.ReadValue("dllAnswer", cmdReset)
            'End Select
            'Aspetta(25000)
            Aspetta(30000)
            'cmdOpen()

            'Do Until myOpenReply.Rc <> "102" And myOpenReply.Rc <> "4" And Len(myOpenReply.Rc) > 0  '4 BUSY DAVIDE
            '    Aspetta(500)
            '    cmdOpen()
            'Loop
waiting:
            cicle += 1
            cmdComandoSingolo("O,1,L,123456", 3, 5000)
            Do Until comandoSingoloRisposta(3) <> "102" And comandoSingoloRisposta(3) <> "4" And comandoSingoloRisposta(3).Length > 0
                Aspetta(500)
                cmdComandoSingolo("O,1,L,123456", 3, 5000)
            Loop
        Catch ex As System.IndexOutOfRangeException
            If cicle > 20 Then
                Return "Failed"
            End If
            Aspetta(3000)
            GoTo waiting
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        'cmdClose()
    End Function


#Region "Transparent Command"
    ''' <summary>
    ''' Invio comando in Transparent. E' necessario che la macchina sia in Transparent mode
    ''' </summary>
    ''' <param name="comando">Comando da inviare</param>
    ''' <param name="esadecimale">true se l'invio e la risposta sono in esadecimale. es. D2.03</param>
    ''' <returns></returns>
    Public Function CmdTransparentCommand(ByVal comando As String, Optional timeOut As Integer = 1000, Optional ByVal esadecimale As Boolean = False) As String
        Dim reply As Integer
        Dim risposta As String = ""
        Dim myTransparent As STransparent = New STransparent
        Dim myTranspRisp As STransparent = New STransparent
        TransparentAnswer = ""
        If esadecimale = True Then
            'For i = 1 To Len(comando) Step 2
            '    Transparent.buff &= Chr(Hex("&H" & Mid(comando, i, 2)))
            'Next
            myTransparent.buff = Chr(comando)
        Else
            myTransparent.buff = comando
        End If

        myTransparent.size = Len(myTransparent.buff)
        myTranspRisp.buff = New String("&H00", 1700)
        myTranspRisp.size = Len(myTranspRisp.buff)
        frmPrincipale.lstComScope.Items.Add("SND: " & myTransparent.buff)
        frmPrincipale.lstComScope.SelectedIndex = frmPrincipale.lstComScope.Items.Count - 1
        reply = CmdSingleCommand(hCon, lpTRANSPARENT, myTransparent, myTranspRisp, timeOut)
        If Len(myTranspRisp.buff) > 1500 Then
            myTranspRisp.buff = ""
        End If
        frmPrincipale.lstComScope.Items.Add("RCV: " & myTranspRisp.buff)
        frmPrincipale.lstComScope.SelectedIndex = frmPrincipale.lstComScope.Items.Count - 1
        If esadecimale = True Then
            For i = 1 To Len(myTranspRisp.buff)
                risposta &= Hex(Asc(Mid(myTranspRisp.buff, i, 2)))
            Next
        Else
            risposta = myTranspRisp.buff
        End If
        CmdTransparentCommand = risposta
        TransparentAnswer = risposta
    End Function
#End Region

    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function DLLAdvanceUnitIdentification(ByVal Hcon As Int16, ByVal idCommand As Byte, ByVal lpCmd As SAdvanceUnitId, ByRef lpReply As SadvanceunitidReply, ByVal timeOute As Int16) As Integer
    End Function

    Structure SAdvanceUnitId
        Dim Modulo As Byte
        Dim Type As Byte
        'Dim ExInfo As SNull
    End Structure
    Public myAdvancedUnitId As SAdvanceUnitId

    Structure SadvanceunitidReply
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=6)>
        Dim CMxx As String
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=5)>
        Dim Name As String
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=11)>
        Dim Type As String
        Dim numProtCas As Int16
        Dim numCas As Int16
        Dim numProBag As Int16
        Dim numBag As Int16
        Dim numCd80 As Int16
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=11)>
        Dim Mode As String
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=11)>
        Dim Led As String
        Dim Rc As Int16
    End Structure
    Public myAdvanceUnitIdReply As SadvanceunitidReply

    Public Function cmdAdvanceUnitIdentification() As String
        cmdAdvanceUnitIdentification = ""
        MsgBox("La funzione Advance Unit Identification NON FUNZIONA!!!")
        Exit Function

        myAdvancedUnitId.Modulo = Convert.ToByte("0"c)
        myAdvancedUnitId.Type = CByte("88")

        cmdAdvanceUnitIdentification = DLLAdvanceUnitIdentification(hCon, lpTEST, myAdvancedUnitId, myAdvanceUnitIdReply, 5000)

        cmdUnitCoverTest()

    End Function


#Region "Assign cassette"
    ''' <summary>
    ''' Assign
    ''' </summary>
    ''' <param name="Hcon"></param>
    ''' <param name="idCommand"></param>
    ''' <param name="lpCmd"></param>
    ''' <param name="lpReply"></param>
    ''' <param name="timeOute"></param>
    ''' <returns></returns>
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function DLLAssign(ByVal Hcon As Int16, ByVal idCommand As Byte, ByVal lpCmd As SNull, ByRef lpReply As SReplyAssign, ByVal timeOute As Int16) As Integer
    End Function

    ''' <summary>
    ''' Struttura per il reply code del comando Assign
    ''' </summary>
    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
    Structure SReplyAssign
        'Dim Part As Integer
        'Dim Delay_Id As Integer
        'Dim Act_slt As Integer
        'Dim CassNumber As Integer
        'Dim bn_in_low As Integer
        Dim Msg As Int16
        'Dim heat_st As Integer
        'Dim NumNotes As Integer
        'Dim Amount As Integer
        Dim rc As Long
    End Structure
    Public myAssignReply As SReplyAssign

    ''' <summary>
    ''' Comando Assign Cassette
    ''' </summary>
    ''' <returns></returns>
    Public Function cmdAssign() As String
        Dim replyFile As String = ""
        Select Case myMacchina.Info.Categoria
            Case "CM18", "CM18SOLO", "CM20S", "CM18EVO"
                replyFile = localFolder & "\TheRCsCM18.ini"
            Case "CM18B", "OM61"
                replyFile = localFolder & "\TheRCsCM18B.ini"
            Case "CM18T", "CM18SOLOT", "CM20T", "CM18EVOT"
                replyFile = localFolder & "\TheRCsCM18T.ini"
            Case "CM20"
                replyFile = localFolder & "\TheRCsCM20.ini"
        End Select
        Dim replyCode As New INIFile(replyFile)
        cmdAssign = ""
        cmdAssign = DLLAssign(hCon, lpAUTOASSIGNCASSETTE, StrutturaNulla, myAssignReply, 5000)
        Select Case cmdAssign
            Case 0
                Select Case myAssignReply.Msg
                    Case 0
                        cmdAssign = replyCode.ReadValue("RC", myAssignReply.rc)
                    Case 1 To 10
                        cmdAssign = "Cassette " & Chr(myAssignReply.Msg + 55) & " NOT EMPTY"
                    Case 11 To 20
                        cmdAssign = "Cassette " & Chr(myAssignReply.Msg + 45) & " FAILED"
                End Select
            Case Else
                cmdAssign = dllError.ReadValue("dllAnswer", cmdAssign)
        End Select

    End Function
#End Region

#Region "Open"
    ''' <summary>
    ''' Open
    ''' </summary>
    ''' <param name="Hcon"></param>
    ''' <param name="idCommand"></param>
    ''' <param name="lpCmd"></param>
    ''' <param name="lpReply"></param>
    ''' <param name="timeOute"></param>
    ''' <returns></returns>
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function DLLOpen(ByVal Hcon As Int16, ByVal idCommand As Byte, ByRef lpCmd As SOpen, ByRef lpReply As SOpenReply, ByVal timeOute As Int32) As Integer
    End Function

    ''' <summary>
    ''' Invio comando di OPEN
    ''' </summary>
    ''' <param name="side">byte rappresentante la lettera del lato su cui effettuare la OPEN</param>
    ''' <param name="password">Password da utilizzare per la OPEN</param>
    ''' <returns></returns>
    Public Function cmdOpen(Optional ByRef side As String = "L", Optional ByRef password As String = "123456") As String
        Dim replyFile As String = ""
        Select Case myMacchina.Info.Categoria
            Case "CM18", "CM18SOLO", "CM20S", "CM18EVO"
                replyFile = localFolder & "\TheRCsCM18.ini"
            Case "CM18B", "OM61"
                replyFile = localFolder & "\TheRCsCM18B.ini"
            Case "CM18T", "CM18SOLOT", "CM20T", "CM18EVOT"
                replyFile = localFolder & "\TheRCsCM18T.ini"
            Case "CM20"
                replyFile = localFolder & "\TheRCsCM20.ini"
        End Select
        Dim replyCode As New INIFile(replyFile)
        Dim bSide As Byte = Asc(side)
        cmdOpen = ""
        myOpen.Side = bSide
        myOpen.Password = password
        'cmdOpen = DLLOpen(hCon, lpOPEN, myOpen, myOpenReply, 5000)
        cmdOpen = cmdComandoSingolo("O,1," & side & "," & password, 3)

        'Select Case cmdOpen
        '    Case 0
        '        cmdOpen = replyCode.ReadValue("RC", myOpenReply.Rc)
        '        If cmdOpen = "OK" Then
        '            myMacchina.Open.Side = Chr(bSide)
        '        End If
        '    Case Else
        '        cmdOpen = dllError.ReadValue("dllAnswer", cmdOpen)
        '        myMacchina.Open.Side = "-"
        'End Select
        myMacchina.Open.rc = cmdOpen

    End Function

    '<StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
    ''' <summary>
    ''' Struttura di invio comando OPEN
    ''' </summary>
    Structure SOpen
        ''' <summary>
        ''' Lato su cui eseguire la Open
        ''' </summary>
        Dim Side As Byte
        ''' <summary>
        ''' Password per eseguire la open
        ''' </summary>
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=7)> 'PASSWORD LEN + 1
        Dim Password As String
        ''' <summary>
        ''' Option: null=no parameter, 1=Get version of Boot loader FW, 2=Book side for 1 minute, 3=Cancel side booking
        ''' </summary>
        Dim Opt As IntPtr
    End Structure
    Public myOpen As SOpen = New SOpen

    ' <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
    ''' <summary>
    ''' Struttura per il reply code del comando Open
    ''' </summary>
    Structure SOpenReply
        ''' <summary>
        ''' Lato su cui si è tentata la Open
        ''' </summary>
        Dim Side As Byte
        ''' <summary>
        ''' Fw Version
        ''' </summary>
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=5)>
        Dim FwVersion As String
        ''' <summary>
        ''' Reply code
        ''' </summary>
        Dim Rc As Int16
    End Structure
    Public myOpenReply As SOpenReply = New SOpenReply
#End Region

#Region "Close"
    ''' <summary>
    ''' Close
    ''' </summary>
    ''' <param name="Hcon"></param>
    ''' <param name="idCommand"></param>
    ''' <param name="lpCmd"></param>
    ''' <param name="lpReply"></param>
    ''' <param name="timeOute"></param>
    ''' <returns></returns>
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function DLLClose(ByVal Hcon As Int16, ByVal idCommand As Byte, ByRef lpCmd As SClose, ByRef lpReply As SCloseReply, ByVal timeOute As Int32) As Integer
    End Function

    ''' <summary>
    ''' Struttura di invio Close
    ''' </summary>
    Structure SClose
        ''' <summary>
        ''' Lato su cui eseguire la Close
        ''' </summary>
        Dim side As Byte
    End Structure
    Public myClose As SClose = New SClose

    ''' <summary>
    ''' Struttura per il reply del Close
    ''' </summary>
    Structure SCloseReply
        ''' <summary>
        ''' Lato su cui si è tentata la Close
        ''' </summary>
        Dim side As Byte
        ''' <summary>
        ''' Reply Code
        ''' </summary>
        Dim Rc As Int16
    End Structure
    Public myCloseReply As SCloseReply = New SCloseReply

    Public Function cmdClose(Optional ByVal side As String = "L") As String
        Dim bSide As Byte = Asc(side)
        Dim replyFile As String = ""
        Select Case myMacchina.Info.Categoria
            Case "CM18", "CM18SOLO", "CM20S", "CM18EVO"
                replyFile = localFolder & "\TheRCsCM18.ini"
            Case "CM18B", "OM61"
                replyFile = localFolder & "\TheRCsCM18B.ini"
            Case "CM18T", "CM18SOLOT", "CM20T", "CM18EVOT"
                replyFile = localFolder & "\TheRCsCM18T.ini"
            Case "CM20"
                replyFile = localFolder & "\TheRCsCM20.ini"
        End Select
        Dim replyCode As New INIFile(replyFile)
        myClose.side = bSide
        cmdClose = cmdComandoSingolo("C,1," & side, 3)
        'cmdClose = DLLClose(hCon, lpCLOSE, myClose, myCloseReply, 5000)
        'Select Case cmdClose
        '    Case 0
        '        cmdClose = replyCode.ReadValue("RC", myCloseReply.Rc)
        '    Case Else
        '        cmdClose = dllError.ReadValue("dllAnswer", cmdClose)
        'End Select

    End Function
#End Region

#Region "Deposit"
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function DLLDeposit(ByVal Hcon As Int16, ByVal idCommand As Byte, ByRef lpCmd As SDeposit, ByRef lpReply As SDepositReply, ByVal timeOute As Int32) As Integer
    End Function

    Structure SDeposit
        Dim Side As Byte
        Dim Mode As Int16
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=5)>
        Dim Denom As String
        Dim exinfo As SNull
    End Structure
    Public myDeposit As SDeposit = New SDeposit

    Structure SDepositReply
        Dim Side As Byte
        Dim NotesToSafe As Int16
        Dim NotesToOut As Int16
        Dim NotesToRej As Int16
        Dim fff As Int16 'Non utilizzato
        Dim NumItem As Int16
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=768)>
        Dim DepNote() As SInfoNotes
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=24)>
        Dim NoteInSafe() As Int16
        Dim rc As Int16
    End Structure
    Public myDepositReply As SDepositReply = New SDepositReply

    Structure SInfoNotes
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=5)>
        Dim Denom As String
        Dim Num As Int16
        'Dim mmm As Int16
    End Structure
    Public InfoNote As SInfoNotes = New SInfoNotes

    ''' <summary>
    ''' Esegue un deposito di banconote
    ''' </summary>
    ''' <param name="side">Lato su cui eseguire il comando</param>
    ''' <returns></returns>
    Public Function cmdDeposit(Optional ByVal side As String = "L") As String
        Dim replyFile As String = ""
        Select Case myMacchina.Info.Categoria
            Case "CM18", "CM18SOLO", "CM20S", "CM18EVO"
                replyFile = localFolder & "\TheRCsCM18.ini"
            Case "CM18B", "OM61"
                replyFile = localFolder & "\TheRCsCM18B.ini"
            Case "CM18T", "CM18SOLOT", "CM20T", "CM18EVOT"
                replyFile = localFolder & "\TheRCsCM18T.ini"
            Case "CM20"
                replyFile = localFolder & "\TheRCsCM20.ini"
        End Select
        Dim replyCode As New INIFile(replyFile)
        cmdDeposit = ""
        myDeposit.Side = Asc(side)
        myDeposit.Mode = 0
        myDeposit.Denom = "0000"
        cmdDeposit = DLLDeposit(hCon, lpDEPOSITCASH, myDeposit, myDepositReply, 60000)
        Select Case cmdDeposit
            Case 0
                cmdDeposit = replyCode.ReadValue("RC", myDepositReply.rc)
            Case Else
                cmdDeposit = dllError.ReadValue("dllAnswer", cmdDeposit)
        End Select
    End Function

    Public Function cmdCounting(Optional ByVal side As String = "L") As String
        Dim replyFile As String = ""
        Select Case myMacchina.Info.Categoria
            Case "CM18", "CM18SOLO", "CM20S", "CM18EVO"
                replyFile = localFolder & "\TheRCsCM18.ini"
            Case "CM18B", "OM61"
                replyFile = localFolder & "\TheRCsCM18B.ini"
            Case "CM18T", "CM18SOLOT", "CM20T", "CM18EVOT", "CM18HC"
                replyFile = localFolder & "\TheRCsCM18T.ini"
            Case "CM20"
                replyFile = localFolder & "\TheRCsCM20.ini"
        End Select
        Dim replyCode As New INIFile(replyFile)
        cmdCounting = ""
        myDeposit.Side = Asc(side)
        myDeposit.Mode = 3
        myDeposit.Denom = "0000"
        'cmdCounting = cmdComandoSingolo("D,1," & side & ",3,0000", 4, 60000)
        cmdCounting = DLLDeposit(hCon, lpDEPOSITCASH, myDeposit, myDepositReply, 60000)
        Select Case cmdCounting
            Case 0
                cmdCounting = replyCode.ReadValue("RC", myDepositReply.rc)
            Case Else
                cmdCounting = dllError.ReadValue("dllAnswer", cmdCounting)
        End Select
    End Function
#End Region

#Region "Unit Cover Test"

    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function DLLUnitCoverTest(ByVal Hcon As Int16, ByVal idCommand As Byte, ByRef lpCmd As SInpOpTest, ByRef lpReply As SInpOpTestReply, ByVal timeOute As Int32) As Integer
    End Function

    Structure SInpOpTest
        Dim Modulo As Byte
        Dim Type As Int16
    End Structure
    Public InpOpTest As SInpOpTest = New SInpOpTest

    Structure SInpOpTestReply
        Dim SafeDoor As Int16 '0=closed - 1=open
        Dim TrayCassette As Int16 '0=closed - 1=open
        Dim Cover As Int16 '0=closed - 1=open
        Dim Feeder As Int16 '0=closed - 1=open
        Dim InputSlot As Int16 '0=empty - 1=notes detected
        Dim RejectSlot As Int16 '0=empty - 1=notes detected
        Dim LeftSlot As Int16 '0=empty - 1=notes detected
        Dim RightSlot As Int16 '0=empty - 1=notes detected
        Dim LeftExtButton As Int16
        Dim RightExtButton As Int16
        Dim CageOpen As Int16
        Dim GateOpen As Int16
        Dim BagOpen As Int16
        Dim Fkb As Int16
        Dim Fks As Int16
        Dim Rc As Int16
    End Structure
    Public lpInpOpTestReply As SInpOpTestReply = New SInpOpTestReply

    Public Function cmdUnitCoverTest(Optional ByVal denomination As String = "0000") As String
        Dim replyFile As String = ""
        Select Case myMacchina.Info.Categoria
            Case "CM18", "CM18SOLO", "CM20S", "CM18EVO"
                replyFile = localFolder & "\TheRCsCM18.ini"
            Case "CM18B", "OM61"
                replyFile = localFolder & "\TheRCsCM18B.ini"
            Case "CM18T", "CM18SOLOT", "CM20T", "CM18EVOT"
                replyFile = localFolder & "\TheRCsCM18T.ini"
            Case "CM20"
                replyFile = localFolder & "\TheRCsCM20.ini"
        End Select
        Dim replyCode As New INIFile(replyFile)
        InpOpTest.Modulo = Convert.ToByte("0"c)
        InpOpTest.Type = Convert.ToByte("0"c)

        cmdUnitCoverTest = DLLUnitCoverTest(hCon, lpTEST, InpOpTest, lpInpOpTestReply, 5000)
        Select Case cmdUnitCoverTest
            Case 0
                cmdUnitCoverTest = replyCode.ReadValue("RC", lpInpOpTestReply.Rc)
            Case Else
                cmdUnitCoverTest = dllError.ReadValue("dllAnswer", cmdUnitCoverTest)
        End Select
    End Function
#End Region

#Region "Withdrawal"
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function DLLWithdrawal(ByVal Hcon As Int16, ByVal idCommand As Byte, ByRef lpCmd As SWithdrawal, ByRef lpReply As SWithdrawalReply, ByVal timeOute As Int32) As Integer
    End Function

    Structure SWithdrawal
        Dim Side As Byte
        'Dim Target As Int16
        Dim NumItem As Int16
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=24)>
        Dim Info() As SInfoNotes
    End Structure
    Public myWithdrawal As SWithdrawal = New SWithdrawal

    Structure SWithdrawalReply
        Dim Side As Byte
        Dim NumItem As Int16
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=24)>
        Dim Info() As SInfoNotes
        Dim rc As Int16
    End Structure
    Public myWithdrawalReply As SWithdrawalReply = New SWithdrawalReply

    Public Function CmdWithdrawal() As String
        Dim replyFile As String = ""
        Select Case myMacchina.Info.Categoria
            Case "CM18", "CM18SOLO", "CM20S", "CM18EVO"
                replyFile = localFolder & "\TheRCsCM18.ini"
            Case "CM18B", "OM61"
                replyFile = localFolder & "\TheRCsCM18B.ini"
            Case "CM18T", "CM18SOLOT", "CM20T", "CM18EVOT"
                replyFile = localFolder & "\TheRCsCM18T.ini"
            Case "CM20"
                replyFile = localFolder & "\TheRCsCM20.ini"
        End Select
        Dim replyCode As New INIFile(replyFile)
        CmdWithdrawal = ""
        myWithdrawal.Side = lpRIGHTSIDE
        myWithdrawal.NumItem = 1
        ReDim myWithdrawal.Info(myWithdrawal.NumItem - 1)
        myWithdrawal.Info(0).Denom = "AAAA"
        myWithdrawal.Info(0).Num = 8
        CmdWithdrawal = DLLWithdrawal(hCon, lpWITHDRAWAL, myWithdrawal, myWithdrawalReply, 112000)

        Select Case CmdWithdrawal
            Case 0
                CmdWithdrawal = replyCode.ReadValue("RC", myWithdrawalReply.rc)
            Case Else
                CmdWithdrawal = dllError.ReadValue("dllAnswer", CmdWithdrawal)
        End Select
    End Function

#End Region ' NON FUNZIONA!!!

#Region "Undo Deposit"
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function DLLUndo(ByVal Hcon As Int16, ByVal idCommand As Byte, ByRef lpCmd As SUndo, ByRef lpReply As SWithdrawalReply, ByVal timeOute As Int32) As Integer
    End Function

    Structure SUndo
        Dim Side As Byte
        Dim Mode As Int16
    End Structure
    Public myUndo As SUndo = New SUndo

    Public Function cmdUndo(Optional ByVal mode As String = "") As String
        Dim replyFile As String = ""
        Select Case myMacchina.Info.Categoria
            Case "CM18", "CM18SOLO", "CM20S", "CM18EVO"
                replyFile = localFolder & "\TheRCsCM18.ini"
            Case "CM18B", "OM61"
                replyFile = localFolder & "\TheRCsCM18B.ini"
            Case "CM18T", "CM18SOLOT", "CM20T", "CM18EVOT"
                replyFile = localFolder & "\TheRCsCM18T.ini"
            Case "CM20"
                replyFile = localFolder & "\TheRCsCM20.ini"
        End Select
        Dim replyCode As New INIFile(replyFile)

        myUndo.Side = lpRIGHTSIDE
        myUndo.Mode = 0
        cmdUndo = DLLUndo(hCon, lpUNDO, myUndo, myWithdrawalReply, 112000)
        Select Case cmdUndo
            Case 0
                cmdUndo = replyCode.ReadValue("RC", myWithdrawalReply.rc)
            Case Else
                cmdUndo = dllError.ReadValue("dllAnswer", cmdUndo)
        End Select
    End Function

#End Region 'FUNZIONA MA NON LEGGE IL REPLY CODE

#Region "Get Version"
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function DLLGetVersion(ByVal Hcon As Int16, ByVal idCommand As Byte, ByRef lpCmd As SGetVersion, ByRef lpReply As SGetVersionReply, ByVal timeOute As Int32) As Integer
    End Function

    Structure SGetVersion
        Dim target As Byte
    End Structure
    Public myGetVersion As SGetVersion = New SGetVersion

    Structure SGetVersionReply
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=5)>
        Dim Ver As String
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=5)>
        Dim Rel As String
        Dim rc As Int16
    End Structure
    Public myGetVersionReply As SGetVersionReply = New SGetVersionReply

    ''' <summary>
    ''' Comando per leggere la versione e la release del firmware dei singoli moduli
    ''' </summary>
    ''' <param name="target">Modulo da interrogare(0:MainControllerCompatibility-1:Feeder - 2:MainController - 3:Reader - 4:SafeController - 5:FPGA - 6:OSC - A..L:Cassetti</param>
    ''' <returns></returns>
    Public Function cmdGetVersion(ByVal target As String)
        Dim replyFile As String = ""
        Select Case myMacchina.Info.Categoria
            Case "CM18", "CM18SOLO", "CM20S", "CM18EVO"
                replyFile = localFolder & "\TheRCsCM18.ini"
            Case "CM18B", "OM61"
                replyFile = localFolder & "\TheRCsCM18B.ini"
            Case "CM18T", "CM18SOLOT", "CM20T", "CM18EVOT"
                replyFile = localFolder & "\TheRCsCM18T.ini"
            Case "CM20"
                replyFile = localFolder & "\TheRCsCM20.ini"
        End Select
        Dim replyCode As New INIFile(replyFile)
        cmdGetVersion = ""
        'myGetVersion.target = Asc(target)
        'cmdGetVersion = DLLGetVersion(hCon, lpVERSION, myGetVersion, myGetVersionReply, 5000)


        'Select Case cmdGetVersion
        '    Case 0
        '        cmdGetVersion = replyCode.ReadValue("RC", myGetVersionReply.rc)
        '    Case Else
        '        cmdGetVersion = dllError.ReadValue("dllAnswer", cmdGetVersion)
        'End Select

        cmdGetVersion = cmdComandoSingolo("V,1," & target, 2)
        myGetVersionReply.rc = comandoSingoloRisposta(2)
        myGetVersionReply.Ver = comandoSingoloRisposta(3)
        myGetVersionReply.Rel = comandoSingoloRisposta(4)

    End Function
#End Region

#Region "Extended Status"
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function DLLExtendedStatus(ByVal Hcon As Int16, ByVal idCommand As Byte, ByRef lpCmd As SNull, ByRef lpReply As SExtendedStatusReply, ByVal timeOute As Int32) As Integer
    End Function

    Structure SExtendedStatusReply
        Dim ErrorTable As SErrorTable
        Dim rc As Int16
    End Structure
    Public myExtendedStatus As SExtendedStatusReply = New SExtendedStatusReply

    Structure SErrorTable
        Dim NumItem As Int16
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=54)> '30
        Dim Info() As SInfoErr
    End Structure

    Structure SInfoErr
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=5)>
        Dim err As String
    End Structure

    Public Function CmdExtendedStatus() As String
        Dim replyFile As String = ""
        Select Case myMacchina.Info.Categoria
            Case "CM18", "CM18SOLO", "CM20S", "CM18EVO"
                replyFile = localFolder & "\TheRCsCM18.ini"
            Case "CM18B", "OM61"
                replyFile = localFolder & "\TheRCsCM18B.ini"
            Case "CM18T", "CM18SOLOT", "CM20T", "CM18EVOT"
                replyFile = localFolder & "\TheRCsCM18T.ini"
            Case "CM20"
                replyFile = localFolder & "\TheRCsCM20.ini"
        End Select
        Dim replyCode As New INIFile(replyFile)
        CmdExtendedStatus = ""

        CmdExtendedStatus = DLLExtendedStatus(hCon, lpEXTENDEDSTATUS, StrutturaNulla, myExtendedStatus, 5000)

        Select Case CmdExtendedStatus
            Case 0
                CmdExtendedStatus = replyCode.ReadValue("RC", myExtendedStatus.rc)
            Case Else
                CmdExtendedStatus = dllError.ReadValue("dllAnswer", CmdExtendedStatus)
        End Select
        If CmdExtendedStatus = "OK" Then
            For i = 0 To myExtendedStatus.ErrorTable.NumItem
                If myExtendedStatus.ErrorTable.Info(i).err.ToString = "" Then Exit For
                Select Case Mid(myExtendedStatus.ErrorTable.Info(i).err, 1, 1)
                    Case "1"
                        myMacchina.Status.Feeder = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgFeeder = replyCode.ReadValue("Feeder", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "2"
                        myMacchina.Status.Controller = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgController = replyCode.ReadValue("Controller", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "3"
                        myMacchina.Status.Safe = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgSafe = replyCode.ReadValue("Safe", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "4"
                        myMacchina.Status.Reader = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgReader = replyCode.ReadValue("Reader", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "A"
                        myMacchina.Status.CassetteA = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteA = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "B"
                        myMacchina.Status.CassetteB = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteB = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "C"
                        myMacchina.Status.CassetteC = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteC = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "D"
                        myMacchina.Status.CassetteD = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteD = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "E"
                        myMacchina.Status.CassetteE = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteE = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "F"
                        myMacchina.Status.CassetteF = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteF = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "G"
                        myMacchina.Status.CassetteG = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteG = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "H"
                        myMacchina.Status.CassetteH = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteH = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "I"
                        myMacchina.Status.CassetteI = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteI = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "J"
                        myMacchina.Status.CassetteJ = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteJ = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "K"
                        myMacchina.Status.CassetteK = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteK = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "L"
                        myMacchina.Status.CassetteL = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteL = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "q"
                        myMacchina.Status.EscrowA = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgEscrowA = replyCode.ReadValue("Bags", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "r"
                        myMacchina.Status.EscrowB = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgEscrowB = replyCode.ReadValue("Bags", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "a"
                        myMacchina.Status.DepositA = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgDepositA = replyCode.ReadValue("Bags", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "b"
                        myMacchina.Status.DepositB = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgDepositB = replyCode.ReadValue("Bags", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                End Select
            Next
        End If
    End Function

#End Region

#Region "Get Cash Data"
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function DLLGetCashData(ByVal Hcon As Int16, ByVal idCommand As Byte, ByRef lpCmd As SGetCashData, ByRef lpReply As SGetCashDataReply, ByVal timeOute As Int32) As Integer
    End Function

    Structure SGetCashData
        Dim side As Byte
    End Structure
    Public myGetCashData As SGetCashData = New SGetCashData

    Structure SGetCashDataReply
        Dim Side As Byte
        Dim NumItem As Int16
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=768)>
        Dim Info() As SInfoCassettes
        Dim rc As Int16
    End Structure
    Public myGetCashDataReply As SGetCashDataReply = New SGetCashDataReply

    Structure SInfoCassettes
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=5)>
        Dim Denom As String
        Dim NotesNum As Integer
        Dim NumFree As Int16
        Dim Cassette As Byte
        Dim Enable As Byte
    End Structure

    Public Function CmdGetCashData() As String
        Dim replyFile As String = ""
        Select Case myMacchina.Info.Categoria
            Case "CM18", "CM18SOLO", "CM20S", "CM18EVO"
                replyFile = localFolder & "\TheRCsCM18.ini"
            Case "CM18B", "OM61"
                replyFile = localFolder & "\TheRCsCM18B.ini"
            Case "CM18T", "CM18SOLOT", "CM20T", "CM18EVOT"
                replyFile = localFolder & "\TheRCsCM18T.ini"
            Case "CM20"
                replyFile = localFolder & "\TheRCsCM20.ini"
        End Select
        Dim replyCode As New INIFile(replyFile)
        CmdGetCashData = ""
        myGetCashData.side = lpRIGHTSIDE
        CmdGetCashData = DLLGetCashData(hCon, lpGETCASHDATA, myGetCashData, myGetCashDataReply, 5000)

        Select Case CmdGetCashData
            Case 0
                CmdGetCashData = replyCode.ReadValue("RC", myGetCashDataReply.rc)
            Case Else
                CmdGetCashData = dllError.ReadValue("dllAnswer", CmdGetCashData)
        End Select

    End Function

#End Region

    ''' <summary>
    ''' Chiude la connessione attiva
    ''' </summary>
    ''' <returns></returns>
    Public Function ChiudiConnessione() As Boolean
        ChiudiConnessione = False
        Dim replyDisconnect As Integer
        For i = 1 To hCon
            replyDisconnect = Disconnetti(hCon)
            Select Case replyDisconnect
                Case 0
                    statoConnessione = ConnectionState.Disconnect
                    ChiudiConnessione = True
                    hCon = 0
                    Exit For
            End Select
        Next

    End Function

    ''' <summary>
    ''' Esegue un Single Command, la risposta completa viene memorizzata nella matrice comandoSingoloRisposta
    ''' </summary>
    ''' <param name="comando">Comando completo. es X,1</param>
    ''' <param name="RCindex">Indice della posizione del replyCode(base1)</param>
    ''' <returns></returns>
    Public Function cmdComandoSingolo(ByVal comando As String, Optional RCindex As Integer = 4, Optional ByVal timeOut As Integer = 5000, Optional ByVal categoria As String = "") As String
        cmdComandoSingolo = ""
        Dim reply As Integer
        Dim mySingleCommand As STransparent = New STransparent
        Dim ReplySingleCommand As STransparent = New STransparent
        Dim inizio, lunghezza, posizione As Integer
        ReDim comandoSingoloRisposta(120)
        Dim replyFile As String = ""
        If categoria.Length = 0 Then
            Select Case myMacchina.Info.Categoria
                Case "CM18", "CM18SOLO", "CM20S", "CM18EVO"
                    replyFile = localFolder & "\TheRCsCM18.ini"
                Case "CM18B", "OM61"
                    replyFile = localFolder & "\TheRCsCM18B.ini"
                Case "CM18T", "CM18SOLOT", "CM20T", "CM18EVOT"
                    replyFile = localFolder & "\TheRCsCM18T.ini"
                Case "CM20"
                    replyFile = localFolder & "\TheRCsCM20.ini"
            End Select
        Else
            replyFile = localFolder & "\TheRCsCM20.ini"
        End If
        Dim replyCode As New INIFile(replyFile)

        mySingleCommand.buff = comando
        mySingleCommand.size = Len(mySingleCommand.buff)
        ReplySingleCommand.buff = New String("&H00", 1700)
        ReplySingleCommand.size = Len(ReplySingleCommand.buff)

        frmPrincipale.lstComScope.Items.Add("SND: " & comando)
        frmPrincipale.lstComScope.SelectedIndex = frmPrincipale.lstComScope.Items.Count - 1
        reply = CmdSingleCommand(hCon, lpSINGLECOMMAND, mySingleCommand, ReplySingleCommand, timeOut)
        If reply <> 0 Then
            Select Case reply
                Case 1059
                Case Else

            End Select
        End If
        'If reply <> 0 Then Return "Failed"
        SingleAnswer = ReplySingleCommand.buff
        frmPrincipale.lstComScope.Items.Add("RCV: " & SingleAnswer)
        frmPrincipale.lstComScope.SelectedIndex = frmPrincipale.lstComScope.Items.Count - 1
        comandoSingoloRisposta = SingleAnswer.Split(",")
        'posizione = 1
        'inizio = 1
        'lunghezza = 1
        'For i = 0 To comandoSingoloRisposta.GetUpperBound(0) - 1
        '    comandoSingoloRisposta(i) = ""
        'Next
        'For i = 1 To Len(ReplySingleCommand.buff)
        '    If Mid(ReplySingleCommand.buff, i, 1) = "," Then
        '        lunghezza = i - inizio
        '        comandoSingoloRisposta(posizione) = Mid(ReplySingleCommand.buff, inizio, lunghezza)
        '        inizio = inizio + lunghezza + 1
        '        posizione += 1
        '    End If
        'Next
        'comandoSingoloRisposta(posizione) = Mid(ReplySingleCommand.buff, inizio, Len(ReplySingleCommand.buff) - inizio + 1)
        Try

            cmdComandoSingolo = replyCode.ReadValue("RC", comandoSingoloRisposta(RCindex))
        Catch
        End Try

        'If InStr(SingleAnswer, "&") > 0 Then
        '    SingleAnswer = ""
        'End If
        If cmdComandoSingolo = "Failed" Then
            If comandoSingoloRisposta(RCindex) = "1" Or comandoSingoloRisposta(RCindex) = "101" Then
                cmdComandoSingolo = "OK"
            Else
                cmdComandoSingolo = comandoSingoloRisposta(RCindex)
            End If
       End If
    End Function

    ''' <summary>
    ''' Prova la connessione impostata
    ''' </summary>
    ''' <returns></returns>
    Public Function ProvaConnessione() As Boolean
        Dim replyConnection As Integer
connessione:
        If statoConnessione = ConnectionState.Connect Then
            ChiudiConnessione()
        End If
        ProvaConnessione = False
        replyConnection = Connetti(hCon, ConnectionParam)
        Select Case replyConnection
            Case 0, 1052 '1052=Connessione già attiva
                If hCon > hConAttuale Then hConAttuale = hCon
                If cmdComandoSingolo("T,1,0,88", 5) = "OK" Then
                    ProvaConnessione = True
                    statoConnessione = ConnectionState.Connect
                    myMacchina.Test.CassetteNumber = comandoSingoloRisposta(10)
                End If
            Case 1050

            Case Else
                statoConnessione = ConnectionState.Errore
                'ScriviMessaggio(String.Format(messaggio(201), replyConnection), lblMessage, ArcaColor.errore) 'Problemi di connessione. Errore numero: xx. Premere un tasto per ripetere, ESC per uscire
                Select Case AttendiTasto()
                    Case "ESC"
                        Exit Function
                    Case Else
                        GoTo connessione
                End Select
        End Select

    End Function



    ''' <summary>
    ''' Chiude tutte le connessioni aperte
    ''' </summary>
    ''' <returns></returns>
    Public Function ChiudiTutteConnessioni() As Boolean
        ChiudiTutteConnessioni = False
    End Function



    ''' <summary>
    ''' Apre la connessione impostata
    ''' </summary>
    ''' <returns></returns>
    Public Function ApriConnessione() As Boolean
        Dim replyConnection As Integer
        Dim reply As String
connessione:
        'If statoConnessione = ConnectionState.Connect Then
        ChiudiConnessione()
        'End If
        ApriConnessione = False
        replyConnection = Connetti(hCon, ConnectionParam)
        Select Case replyConnection
            Case 0, 1052 ' Connessione già attiva
                If hCon > hConAttuale Then hConAttuale = hCon
                'reply = cmdComandoSingolo("T,1,0,77", 4)
                'Select Case reply
                '    Case ""
                '        Select Case ConnectionParam.ConnectionMode
                '            Case 83
                '                'la connessione seriale standard non ha funzionato, provare la semplificata?
                '                If MsgBox("la connessione RS232 STANDAND non ha funzionato, provare la SEMPLIFICATA?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                '                    ConnectionParam.ConnectionMode = DLINKMODESERIALEASY
                '                    GoTo connessione
                '                End If
                '            Case 115
                '                'la connessione seriale semplificata non ha funzionato, provare la standard?
                '                If MsgBox("la connessione RS232 SEMPLIFICATA non ha funzionato, provare la STANDARD?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                '                    ConnectionParam.ConnectionMode = DLINKMODESERIAL
                '                    GoTo connessione
                '                End If
                '            Case 85
                '                If MsgBox("la connessione USB STANDARD non ha funzionato, provare la SEMPLIFICATA?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                '                    ConnectionParam.ConnectionMode = DLINKMODEUSBEASY
                '                    GoTo connessione
                '                End If
                '                'la connessione seriale standard non ha funzionato, provare la semplificata?

                '            Case 117
                '                If MsgBox("la connessione USB SEMPLIFICATA non ha funzionato, provare la STAMDARD?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                '                    ConnectionParam.ConnectionMode = DLINKMODEUSB
                '                    GoTo connessione
                '                End If
                '                'la connessione seriale semplificata non ha funzionato, provare la standard?

                '        End Select
                'End Select
                ApriConnessione = True
                statoConnessione = ConnectionState.Connect
            Case 1052 'Connessione già attiva

                'Case 1056 'Error on TCP/IP socket
                'Case 1066 '??

            Case Else
                statoConnessione = ConnectionState.Errore
                If ConnectionParam.ConnectionMode = DLINKMODETCPIP Then Exit Function
                If MsgBox("Connection ERROR, do you want retry?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                    GoTo connessione
                End If
                Exit Function
        End Select

    End Function

#Region "TRANSPARENT COMMANDS"
    ''' <summary>
    ''' D201 - Set operator and init counter for deposit. Power on(D133), set input mode MSO and MSV, send to safe input mode (D101). MDE in home position
    ''' </summary>
    Public Sub tcSetDepositMode()
        CmdTransparentCommand("D201", 1500)
    End Sub

    ''' <summary>
    ''' D202 - Set operator and init counter for withdrawal. Power on(D133). MDE in output position. Send to safe output mode (D102)
    ''' </summary>
    Public Sub tcSetWithdrawalMode()
        CmdTransparentCommand("D202", 1500)
    End Sub

    ''' <summary>
    ''' D203 - Power off(D133). MSO and MSV OFF. MDE in home position.
    ''' </summary>
    Public Sub tcSetStopMode()
        CmdTransparentCommand("D203", 1500)
    End Sub

    ''' <summary>
    ''' D204 - Set operator RIGHT
    ''' </summary>
    Public Sub tcSetSideR()
        CmdTransparentCommand("D204", 1500)
    End Sub

    ''' <summary>
    ''' D205 - Set operator LEFT
    ''' </summary>
    Public Sub tcSetSideL()
        CmdTransparentCommand("D205", 1500)
    End Sub

    ''' <summary>
    ''' </summary>
    ''' D206 - Set electromagnetic SHIFT ON
    Public Sub tcShiftOn()
        CmdTransparentCommand("D206", 1500)
    End Sub

    ''' <summary>
    ''' D207 - Set electromagnetic SHIFT OFF
    ''' </summary>
    Public Sub tcShoftOff()
        CmdTransparentCommand("D207", 1500)
    End Sub

    ''' <summary>
    ''' D208 - Set MDE in OUTPUT mode
    ''' </summary>
    Public Sub tcMDEOutput()
        CmdTransparentCommand("D208", 1500)
    End Sub

    ''' <summary>
    ''' D209 - Set MDE in INPUT mode
    ''' </summary>
    Public Sub tcMDEInput()
        CmdTransparentCommand("D209", 1500)
    End Sub

    ''' <summary>
    ''' D20A - Set MDE in HOME position
    ''' </summary>
    Public Sub tcMDEHome()
        CmdTransparentCommand("D20A", 1500)
    End Sub

    ''' <summary>
    ''' D20B - Set PRESSOR in HOME position
    ''' </summary>
    Public Sub tcPressorHome()
        CmdTransparentCommand("D20B", 1500)
    End Sub

    ''' <summary>
    ''' D20C - Set PRESSOR in OPENED position
    ''' </summary>
    Public Sub tcPressorOpen()
        CmdTransparentCommand("D20C", 1500)
    End Sub

    ''' <summary>
    ''' D20D - Set PRESSOR in CLOSED position
    ''' </summary>
    Public Sub tcPressorClose()
        CmdTransparentCommand("D20D", 1500)
    End Sub

    ''' <summary>
    ''' D20E - Turn ON Pre-feeding motor (MOTB)
    ''' </summary>
    Public Sub tcPreFeedOn()
        CmdTransparentCommand("D20E", 1500)
    End Sub

    ''' <summary>
    ''' D20F - Turn OFF Pre-feeding motor (MOTB)
    ''' </summary>
    Public Sub tcPreFeedOff()
        CmdTransparentCommand("D20F", 1500)
    End Sub

    ''' <summary>
    ''' D210 - Set MSO in INPUT mode
    ''' </summary>
    Public Sub tcMSOInput()
        CmdTransparentCommand("D210", 1500)
    End Sub

    ''' <summary>
    ''' D211 - Set MSO in OUTPUT mode
    ''' </summary>
    Public Sub tcMSOOutput()
        CmdTransparentCommand("D211", 1500)
    End Sub

    ''' <summary>
    ''' D212 - Set MSO OFF
    ''' </summary>
    Public Sub tcMSOOff()
        CmdTransparentCommand("D212", 1500)
    End Sub

    ''' <summary>
    ''' D213 - Set MSV in INPUT mode
    ''' </summary>
    Public Sub tcMSVInput()
        CmdTransparentCommand("D213", 1500)
    End Sub

    ''' <summary>
    ''' D214 - Set MSV in OUTPUT mode
    ''' </summary>
    Public Sub tcMSVOutput()
        CmdTransparentCommand("D214", 1500)
    End Sub

    ''' <summary>
    ''' D215 - Set MSV OFF mode
    ''' </summary>
    Public Sub tcMSVOff()
        CmdTransparentCommand("D215", 1500)
    End Sub

    ''' <summary>
    ''' D216 - Set ON use of PUSHER automatic in REJECT slot
    ''' </summary>
    Public Sub tcPusherRejectOn()
        CmdTransparentCommand("D216", 1500)
    End Sub

    ''' <summary>
    ''' D217 - Set OFF use of PUSHER automatic in REJECT slot
    ''' </summary>
    Public Sub tcPusherRejectOff()
        CmdTransparentCommand("D217", 1500)
    End Sub

    ''' <summary>
    ''' D218 - Turn ON FEEDING motor (MOTA)
    ''' </summary>
    Public Sub tcFeedMotorOn()
        CmdTransparentCommand("D218", 1500)
    End Sub

    ''' <summary>
    ''' D219 - Turn OFF FEEDING motor (MOTA)
    ''' </summary>
    Public Sub tcFeedMotorOff()
        CmdTransparentCommand("D219", 1500)
    End Sub

    ''' <summary>
    ''' D21A - MDE,Pushe,Pressor in HOME position. MSO,MSV and MIV in INPUT mode. LOOP shift ON / OFF, align home, mde output...
    ''' </summary>
    Public Sub tcTestGlobal()
        CmdTransparentCommand("D21A", 1500)
    End Sub

    ''' <summary>
    ''' D21B - Pressor TEST
    ''' </summary>
    Public Sub tcTestPressor()
        CmdTransparentCommand("D21B", 1500)
    End Sub

    ''' <summary>
    ''' D21C - Set ALING in HOME position
    ''' </summary>
    Public Sub tcAlignHome()
        CmdTransparentCommand("D21C", 1500)
    End Sub

    ''' <summary>
    ''' D21D - Set PUSHER in HOME position
    ''' </summary>
    Public Sub tcPusherHome()
        CmdTransparentCommand("D21D", 1500)
    End Sub

    ''' <summary>
    ''' D21E - Set PUSHER in OPENED position
    ''' </summary>
    Public Sub tcPusherOpen()
        CmdTransparentCommand("D21E", 1500)
    End Sub

    ''' <summary>
    ''' D21F - Set PUSHER in CLOSED position
    ''' </summary>
    Public Sub tcPusherClose()
        CmdTransparentCommand("D21F", 1500)
    End Sub

    ''' <summary>
    ''' D22E - Reset to 0 photo FEEDER COUNTER
    ''' </summary>
    Public Sub tcFeedCountClear()
        CmdTransparentCommand("D22E", 1500)
    End Sub

    ''' <summary>
    ''' D22F - Reset to 0 photo SHIFT COUNTER
    ''' </summary>
    Public Sub tcShiftCountClear()
        CmdTransparentCommand("D22F", 1500)
    End Sub

    ''' <summary>
    ''' D230 - Reset to 0 photo 3th CURVE COUNTER
    ''' </summary>
    Public Sub tcCurve3CountClear()
        CmdTransparentCommand("D230", 1500)
    End Sub

    ''' <summary>
    ''' D231 - Reset to 0 photo 4th CURVE COUNTER
    ''' </summary>
    Public Sub tcCurve4CountClear()
        CmdTransparentCommand("D231", 1500)
    End Sub

    ''' <summary>
    ''' D232 - Reset to 0 photo COUNT COUNTER
    ''' </summary>
    Public Sub tcFCountClear()
        CmdTransparentCommand("D232", 1500)
    End Sub

    ''' <summary>
    ''' D233 - Power ON (+36V enable) on high level and low leveel 
    ''' </summary>
    Public Sub tcPowerOn()
        CmdTransparentCommand("D233", 1500)
    End Sub

    ''' <summary>
    ''' D234 - Power OFF (+36 disable) on high level and low level
    ''' </summary>
    Public Sub tcPowerOff()
        CmdTransparentCommand("D234", 1500)
    End Sub

    ''' <summary>
    ''' E27D - Get Power AD Value read from pin PWRSNS
    ''' risp: 3x3x3x3x
    ''' </summary>
    Public Sub tcGetPowerAdValue()
        CmdTransparentCommand("E27D", 1500)
    End Sub

    ''' <summary>
    ''' D235 - Activate 1 impulse to ON relè for upper cover
    ''' </summary>
    Public Sub tcUpperRelèOn()
        CmdTransparentCommand("D235", 1500)
    End Sub

    ''' <summary>
    ''' D236 - Activate one impulse to OFF relè for upper cover
    ''' </summary>
    Public Sub tcUpperRelèOff()
        CmdTransparentCommand("D236", 1500)
    End Sub

    ''' <summary>
    ''' D239 - Set to 0 FEEDER COUNTER. Set status to BUSY. Start feeding and answer 3byte for any note transit in reader
    ''' </summary>
    Public Sub tcFeedStart()
        CmdTransparentCommand("D239", 1500)
    End Sub

    ''' <summary>
    ''' D23A - Stop feeder. Feeder in ready status and turn pressor in home position
    ''' </summary>
    Public Sub tcFeedStop()
        CmdTransparentCommand("D23A", 1500)
    End Sub

    ''' <summary>
    ''' D23C - Adjust value of all potentiometer and write any value in EEPROM
    ''' </summary>
    Public Sub tcPhotoAdjust()
        CmdTransparentCommand("D23C", 1500)
    End Sub

    ''' <summary>
    ''' D23D - Adjust value of potentiometer of FSHIFT and write the value in EEPROM
    ''' </summary>
    Public Sub tcPhotoShiftAdjust()
        CmdTransparentCommand("D23D", 1500)
    End Sub

    ''' <summary>
    ''' D23E - Adjust value of potentiometer of FINQ and write the value in EEPROM
    ''' </summary>
    Public Sub tcPhotoFinqAdj()
        CmdTransparentCommand("D23E", 1500)
    End Sub

    ''' <summary>
    ''' D23F - Adjust value of potentiometer of FCOUNT and write the value in EEPROM
    ''' </summary>
    Public Sub tcPhotoFCountAdj()
        CmdTransparentCommand("D23F", 1500)
    End Sub

    ''' <summary>
    ''' D240 - Adjust value of potentiometer of FC4A and write the value in EEPROM
    ''' </summary>
    Public Sub tcPhotoFC4AAdj()
        CmdTransparentCommand("D240", 1500)
    End Sub

    ''' <summary>
    ''' D241 - Adjust value of potentiometer of FC3 and write the value in EEPROM
    ''' </summary>
    Public Sub tcPhotoFC3Adj()
        CmdTransparentCommand("D241", 1500)
    End Sub

    ''' <summary>
    ''' D242 - Adjust value of potentiometer of FREJ and write the value in EEPROM
    ''' </summary>
    Public Sub tcPhotoFREJAdj()
        CmdTransparentCommand("D242", 1500)
    End Sub

    ''' <summary>
    ''' D243 - Adjust value of potentiometer of FINC and write the value in EEPROM
    ''' </summary>
    Public Sub tcPhotoFINCAdj()
        CmdTransparentCommand("D243", 1500)
    End Sub

    ''' <summary>
    ''' D244 - Adjust value of potentiometer of FC1 and write the value in EEPROM
    ''' </summary>
    Public Sub tcPhotoFC1Adj()
        CmdTransparentCommand("D244", 1500)
    End Sub

    ''' <summary>
    ''' D245 - Adjust value of potentiometer of FFEED and write the value in EEPROM
    ''' </summary>
    Public Sub tcPhotoFFEEDAdj()
        CmdTransparentCommand("D245", 1500)
    End Sub

    ''' <summary>
    ''' D246 - Adjust value of potentiometer of FHMAX and write the value in EEPROM
    ''' </summary>
    Public Sub tcPhotoFHMAXAdj()
        CmdTransparentCommand("D246", 1500)
    End Sub

    ''' <summary>
    ''' D247 - Adjust value of potentiometer of FOUT and write the value in EEPROM
    ''' </summary>
    Public Sub tcPhotoFOUTAdj()
        CmdTransparentCommand("D247", 1500)
    End Sub

    ''' <summary>
    ''' D248 - Adjust value of potentiometer of FINL and write the value in EEPROM
    ''' </summary>
    Public Sub tcPhotoFINLAdj()
        CmdTransparentCommand("D248", 1500)
    End Sub

    ''' <summary>
    ''' D249 - Adjust value of potentiometer of FC4B and write the value in EEPROM
    ''' </summary>
    Public Sub tcPhotoFC4BAdj()
        CmdTransparentCommand("D249", 1500)
    End Sub

    ''' <summary>
    ''' D24A - Adjust value of potentiometer of FPBKSUS and write the value in EEPROM
    ''' </summary>
    Public Sub tcPhotoFPBKSUSAdj()
        CmdTransparentCommand("D24A", 1500)
    End Sub

    ''' <summary>
    ''' D24B - Set ELM POCKET (if present on 4th curve) to permit banknote to enter in pocket slot
    ''' </summary>
    Public Sub tcPocketElmOn()
        'tcPocketHome
        CmdTransparentCommand("D24B", 1500)
    End Sub

    ''' <summary>
    ''' D24C - Set ELM POCKET (if present on 4th curve) to permit banknote to go directly in reject slot
    ''' </summary>
    Public Sub tcPocketElmOff()
        'tcPocketHome()
        CmdTransparentCommand("D24C", 1500)
    End Sub

    ''' <summary>
    ''' D24D - Set ELM POCKET (if present on 4th curve) in HOME position and only if HOME positio is OK then is possible to send On or Off command
    ''' </summary>
    Public Sub tcPocketHome()
        CmdTransparentCommand("D24D", 1500)
    End Sub

    ''' <summary>
    ''' D2 - 
    ''' </summary>
    Public Sub tc()
        CmdTransparentCommand("D2", 1500)
    End Sub

#End Region

    '********************   CM TRACE    ****************

    'Structure STraceDirective
    '    Dim InfoLayer As slayer
    '    Dim TracePathName As Byte()
    '    Dim lpComScope As Scomscope
    '    Dim TrcType As Strctype
    '    Dim TrcSize As Int16
    '    Dim TrcNum As Int16
    '    Dim sinfocomp As STRComponent
    'End Structure
    'Public TraceDirective As New STraceDirective

    'Structure STRComponent
    '    Dim Level As Int16
    'End Structure

    'Structure SComScope
    '    Dim hWin As hwnd
    'End Structure
End Module
